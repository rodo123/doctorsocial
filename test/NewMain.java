
import edoctor.com.beans.SpecialityBean;
import edoctor.com.dao.SpecialityDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author winbugs
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {
        SpecialityDAO s = new SpecialityDAO();
        List<SpecialityBean> list = new ArrayList<>();
        list = s.selectSpeciality();
        for (int i = 0 ; i < list.size() ; i++){
            System.out.println(list.get(i).getSp_name());
        }
    }
    
}
