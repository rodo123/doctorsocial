<%@page import="edoctor.com.beans.EmpleoBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<!DOCTYPE html>

	<html class="no-js"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RED MEDICA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="EDoctor.com" />
        <meta name="keywords" content="edoctor, medicos, doctores, html5, css3, mobile first, responsive" />
        <meta name="author" content="NyR" />


        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">

        <!-- Animate.css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="css/icomoon.css">
        <!-- Simple Line Icons -->
        <link rel="stylesheet" href="css/simple-line-icons.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <!-- Style -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/hover.css">


        <!-- Modernizr JS -->
        <script src="js/modernizr-2.6.2.min.js"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>
    
	<body>
        
        <section id="fh5co-explore" class="search-background" data-section="services">
            <div class="container ">
                <h3>
				</h3>
                <p>
				</p>
                <form class="contact-form contact-form-search backgroud-transparent-black">
					<div class="row"> 
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<label for="nombres" class="sr-only">Cargo</label>
							<input name="cargo"  type="name" class="form-control" id="cargo" placeholder="Cargo" size="20">
						</div>

						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<label for="apellidos" class="sr-only">Ubicaci�n</label>
							<input id="ubicacion" name="ubicacion" type="name" class="form-control"  placeholder="Ubicaci�n" size="20">
						</div>
						
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<label for="trabajo" class="sr-only">Funci�n Laboral</label>
							<input id="funcion_laboral" name="funcion_laboral" type="name" class="form-control"  placeholder="Funci�n Laboral" size="20">
						</div>
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">          
							<select id ="tipo_empleo" name = "tipo_empleo" class="form-control">
								<option value="">Tipo de Empleo</option>
								<option value="Jornada completa">Jornada completa
								</option>
								<option value="Media jornada">Media jornada
								</option>
								<option value="Contrato por obra">Contrato por obra
								</option>
								<option value="Temporal">Temporal
								</option>
								<option value="Voluntario">Voluntario
								</option>
								<option value="Pr�cticas">Pr�cticas
								</option>
						   </select>
						</div>
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<select id ="nivel_antiguedad" name = "nivel_antiguedad" class="form-control">
								<option value="">Nivel de Antiguedad</option>
								<option value="Pr�cticas">Pr�cticas
								</option>
								<option value="Sin experiencia">Sin experiencia
								</option>
								<option value="Algo de responsabilidad">Algo de responsabilidad
								</option>
								<option value="Mando intermedio" selected="">Mando intermedio
								</option>
								<option value="Director">Director
								</option>
								<option value="Ejecutivo">Ejecutivo
								</option>
							</select>
						</div>
						<div class="form-group col-md-12 col-sm-12 col-xs-12">
							<input type="submit" id="btn-submit" class="btn btn-primary btn-send-message btn-md col-md-12 col-sm-12 col-xs-12 " value="Buscar">
						</div>
					</div>
				</form>
			</div>
        </section>
        
		
		<section id="fh5co-cards">
            <div class="container ">
                <% int counter = 1;
                List<EmpleoBean> listEmpleo = (ArrayList<EmpleoBean>) request.getAttribute("listEmpleo");
                if(listEmpleo.size()==0){
                %>
					<div style=" text-align:center; ">
                        <img src="images/pediatrician.png" style="width:100px; text-align:center; height:100px;" />
                        <br><br>
                        <h4>Encuentre el mejor lugar para trabajar</h4>
                    </div>
                <% }                       
                for (EmpleoBean empleoBean : listEmpleo)
				{%>
                <div class="  col-md-6 col-sm-6">     
                    <div class="card backgroud-card">
                        <row>
                            <div class="col-md-4 col-sm-12 col-xs-4 col-xxs-12" style="padding-left: 0px; padding-right: 0px;">
								<img src="images/default_doctor.png" style="background:center no-repeat;  background-size: cover;  height: 180px;  padding-left: 0px;  padding-right: 0px;"/>
						    </div>
							<form name="perfil" method="post" action="EmpleoController/postularEmpleo.do">
							<div class="col-md-8 col-sm-12 col-xs-8 col-xxs-12">
								<div class="car-top">
									<h4 class="title-card-doctor "><% out.print(empleoBean.getCargo());%></h4>
								</div>
								<div class="">
									<ul class="profile-list">
												<li class="clearfix">
                                                    <strong class="title"><i class="fa fa-university" style="color:#730053" aria-hidden="true"></i> Empresa </strong>
                                                    <span class="cont"><% out.print(empleoBean.getEmpresa());%></span>
                                                </li>
												<li class="clearfix">
                                                    <strong class="title"><i class="fa fa-id-card-o" aria-hidden="true" style="color:#730053"></i> Ubicaci�n </strong>
                                                    <span class="cont"><% out.print(empleoBean.getUbicacion());%></span>
													<input type="hidden" name="cmp" value="<% out.print(empleoBean.getUbicacion());%>" style="visibility:hidden">
                                                </li>
                                                
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-id-card-o" aria-hidden="true" style="color:#730053"></i> Funci�n Laboral </strong>
                                                    <span class="cont">
														<%out.print(empleoBean.getFuncion_laboral());%>
													</span>
                                                </li>
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-stethoscope fa-lg" aria-hidden="true" style="color:#730053"></i> Tipo de Empleo </strong>
                                                    <span class="cont">
													
													<% out.print(empleoBean.getTipo_empleo());%></span>
                                                </li>
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-stethoscope fa-lg" aria-hidden="true" style="color:#730053"></i> Sector Empresa </strong>
                                                    <span class="cont"><% out.print(empleoBean.getSector_empresa());%></span></span>
                                                </li>
												<li class="clearfix">
                                                    <strong class="title"><i class="fa fa-stethoscope fa-lg" aria-hidden="true" style="color:#730053"></i> Descripci�n del Empleo </strong>
                                                    <span class="cont"><% out.print(empleoBean.getDescripcion());%></span></span>
                                                </li>
												<li class="clearfix">
                                                    <strong class="title"><i class="fa fa-stethoscope fa-lg" aria-hidden="true" style="color:#730053"></i> Descripci�n de la Empresa </strong>
                                                    <span class="cont"><% out.print(empleoBean.getDescripcion_empresa());%></span></span>
                                                </li>
												<li class="clearfix">
                                                    <strong class="title"><i class="fa fa-envelope" aria-hidden="true" style="color:#730053" ></i> Correo </strong>
                                                    <span class="cont"><a href="<% out.print(empleoBean.getEmail());%>"><% out.print(empleoBean.getEmail());%></a></span>
                                                </li>
                                                <!--<li class="clearfix">
                                                    <strong class="title"><i class="fa fa-comments  fa-lg" aria-hidden="true" style="color:#730053"></i> Redes Sociales </strong>
                                                    <span class="cont"> 
                                                   <a href=""><i class="fa fa-twitter fa-2x" aria-hidden="true" style="color:#730053"></i></a>
                                                   <a href=""><i class="fa fa-facebook-official fa-2x" aria-hidden="true" style="color:#730053"></i></a>
												   </span>

                                                </li>-->
												<input id="trabajo_id" name="trabajo_id" type="hidden" value="<% out.print(empleoBean.getTrabajo_id());%>"/>
												<input id="cargo_postulacion" name="cargo_postulacion" type="hidden" value="<% out.print(empleoBean.getCargo());%>"/>
												<input id="email_postulacion" name="email_postulacion" type="hidden" value="<% out.print(empleoBean.getEmail());%>"/>
                                                <li class="clearfix">
                  									<div class="form-group col-md-6 col-sm-6 col-xs-6">
														<input type="submit" id="btn-submit" class="btn btn-primary btn-send-message btn-md col-md-12 col-sm-12 col-xs-12 " value="POSTULAR">
													</div>
                                                </li>
												
                                    </ul>
                                </div>
							</div>
							</form>
						</row>
                    </div>
                </div>
                <%  }  %>
            </div>
             
            
        </section>

        
        
        
      

        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Stellar Parallax -->
        <script src="js/jquery.stellar.min.js"></script>
        <!-- Owl Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- Counters -->
        <script src="js/jquery.countTo.js"></script>
        <!-- Google Map -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
        <script src="js/google_map.js"></script>
        <!-- Main JS (Do not remove) -->
        <script src="js/main.js"></script>

    </body>
</html>

