<!DOCTYPE html>
<html lang="en" >

	<head>
	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RED MEDICA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="EDoctor.com" />
        <meta name="keywords" content="edoctor, mÃ©dicos, doctores, html5, css3, mobile first, responsive" />
        <meta name="author" content="NyR" />

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

		<!-- Bootstrap CSS -->    
		<link href="css/social/bootstrap.min.css" rel="stylesheet">
		<!-- bootstrap theme -->
		<link href="css/social/bootstrap-theme.css" rel="stylesheet">
		<!--external css-->
		<!-- font icon -->
		<link href="css/social/elegant-icons-style.css" rel="stylesheet" />
		<link href="css/social/font-awesome.min.css" rel="stylesheet" />    
		<!-- full calendar css-->
		<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
		<link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
		<!-- easy pie chart-->
		<link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
		<!-- owl carousel -->
		<link rel="stylesheet" href="css/social/owl.carousel.css" type="text/css">
		<link href="css/social/jquery-jvectormap-1.2.2.css" rel="stylesheet">
		<!-- Custom styles -->
		<link rel="stylesheet" href="css/fullcalendar.css">
		<link href="css/social/widgets.css" rel="stylesheet">
		<link href="css/social/style1.css" rel="stylesheet">
		<link href="css/social/style-responsive.css" rel="stylesheet" />
		<link href="css/social/xcharts.min.css" rel=" stylesheet"> 
		<link href="css/social/jquery-ui-1.10.4.min.css" rel="stylesheet">

  </head>

	<body>
  
		<!-- container section start -->
		<section id="container" class="">
			<header class="header dark-bg">
				
				<!--logo start-->
                <a href="index.jsp" class="logo"> <img src="images/logo2.png" alt="logo" style="width: 150px; margin-top: -8px;">
				</a>
                <!--logo end-->
				  <div class="nav search-row" id="top_menu">
                    <!-- Inicio de busqueda -->
                    <ul class="nav top-menu">                    
                        <li>
                            <form class="navbar-form">
                                <a href="BusquedaDoctorControlle" class="buscar" placeholder="Busqueda" type="button" >Buscar
                                </a>
                            </form>
                        </li>                    
                    </ul>
                    <!-- Fin de busqueda -->                
                </div>
              

                <div class="top-nav notification-row">                
                    <!-- notificatoin dropdown start-->
                    <ul class="nav pull-right top-menu">
                         <!-- task notificatoin start -->
                        <li id="task_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-home fa-lg" aria-hidden="true">
								</i>
                                <span class="badge bg-important">6
								</span>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Inicio
							</span>
                            <ul class="dropdown-menu extended tasks-bar">
                                <div class="notify-arrow notify-arrow-blue">
								</div>
                                <li>
                                    <p class="blue">You have 6 pending letter
									</p>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="task-info">
                                            <div class="desc">Design PSD 
											</div>
                                            <div class="percent">90%
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="external">
                                    <a href="#">See All Tasks
									</a>
                                </li>
                            </ul>
                        </li>
						
                        <li id="task_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-users fa-lg" aria-hidden="true">
								</i>
                            </a>
                                <span style="font-size: 12px; line-height: 16px;">Mi Red</span>
                        </li>
                        
						<li id="task_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i  class="fa fa-briefcase fa-lg" aria-hidden="true">
								</i>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Trabajos
							</span>
                        </li>
                        <!-- task notificatoin end -->
						
                        <!-- inbox notificatoin start-->
                        <li id="mail_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-comments-o fa-lg" aria-hidden="true">
								</i>
                                <span class="badge bg-important">6
								</span>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Mensajes
							</span>
                            <ul class="dropdown-menu extended inbox">
                                <div class="notify-arrow notify-arrow-blue">
								</div>
                                <li>
                                    <p class="blue">You have 5 new messages
									</p>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
											<img alt="avatar" src="./img/avatar-mini.jpg">
											</img>
										</span>
                                        <span class="subject">
										</span>
                                        <span class="from">Greg  Martin
										</span>
                                    </a>
                                </li>
								
                                <li>
                                    <a href="#">See all messages
									</a>
                                </li>
                            </ul>
                        </li>
                        <!-- inbox notificatoin end -->
                        <!-- alert notification start-->
                        <li id="alert_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-bell-o fa-lg" aria-hidden="true">
								</i>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Notificaciones
							</span>
                            <ul class="dropdown-menu extended notification">
                                <div class="notify-arrow notify-arrow-blue">
								</div>
                                <li>
                                    <p class="blue">You have 4 new notifications
									</p>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="label label-primary">
											<i class="icon_profile">
											</i>
										</span> 
                                        Friend Request
                                    </a>
                                </li>     
                            </ul>
                        </li>
                            <!-- alert notification end-->
                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="profile-ava">
                                    <img alt="" src="../img/avatar1_small.jpg">
                                </span>
                            </a>
                            <span style="font-size: 11px;">Yo</span>
                                <b class="caret"></b>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li class="eborder-top">
                                    <a href="#"><i class="icon_profile"></i> My Profile</a>
                                </li>
                                </li>
                                <li>
                                    <a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!-- notificatoin dropdown end-->
                    <!-- notificatoin dropdown end-->
                </div>
			</header>      
			<!--header end-->
			<br>
			<br>
			<br>
			<form method="POST" action="EmpleoController/insertEmpleo.do" class="simple" id="job-posting-form-elem" novalidate="">
				<div id="layout-view">
					<div id="header-region">
						<div>
							<div class="jobs-header-wrapper   show-edit-mode" aria-describedby="page-info">
								<div class="header-image-container">
									<img class="header-logo " src="" alt="" width="50" height="45">
									<i class="company-ghost header-company-icon hidden">
										<span class="ghost-description">
											Company placeholder image
										</span>
									</i>
								</div>
								<ul class="header-params-wrapper">
									<li>
										<div class="title-typeahead typeaheads" id="title-region">
											<div class="typeahead-input-wrapper">
												<label for="title-input" class="hide-a11y">
													Cargo
												</label>
												<div class="header-input-wrapper">
													<span class="twitter-typeahead" style="position: relative; display: inline-block;">
														<input id="cargo" name="cargo" type="text" class="title-input js-typeahead tt-input" value="" aria-describedby="error-title error-title-length" placeholder="Cargo" autocomplete="off" spellcheck="false" dir="auto" role="combobox" aria-owns="tt-behavior29" aria-autocomplete="list" aria-expanded="false" style="position: relative; vertical-align: top;">
													</span>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="company-typeahead typeaheads" id="company-region">
											<div class="typeahead-input-wrapper">
												<label for="company-input" class="hide-a11y">
													Empresa
												</label>
												<div class="header-input-wrapper">
													<span class="twitter-typeahead" style="position: relative; display: inline-block;">
														<input id="empresa" name="empresa" type="text" class="company-input js-typeahead tt-input" value="" aria-describedby="error-company-name error-company-name-length" placeholder="Empresa" autocomplete="off" spellcheck="false" dir="auto" role="combobox" aria-owns="tt-behavior30" aria-autocomplete="list" aria-expanded="false" style="position: relative; vertical-align: top;">
													</span>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="location-typeahead typeaheads " id="location-region">
											<div class="typeahead-input-wrapper">
												<label for="location-input" class="hide-a11y">
													Ubicaci�n
												</label>
												<div class="header-input-wrapper">
													<span class="twitter-typeahead" style="position: relative; display: inline-block;">
														<input id="ubicaci�n" name="ubicaci�n" type="text" class="location-input js-typeahead tt-input" value="" aria-describedby="error-location-id location-cn-error-message" placeholder="Ubicaci�n" autocomplete="off" spellcheck="false" dir="auto" role="combobox" aria-owns="tt-behavior31" aria-autocomplete="list" aria-expanded="false" style="position: relative; vertical-align: top;">
													</span>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div id="main-region">
							<div class="">
								<article id="job-post-form" class="view-wrapper">
									<fieldset class="categorization-fields">
										<div class="job-function-type category-row">
											<div class="job-function-field field">
												<span class="job-functions-typeahead typeaheads" id="job-function-region">
													<div class="typeahead-input-wrapper">
														<label for="multiselect-job-functions-typeahead" class="required categorization-field-label">
															Funci�n laboral
														</label>
														<div class="categorization-multiselect" tabindex="0">
															<ul class="multiselect-typeahead-wrapper" id="job-functions">
																<li class="multiselect-input-wrapper">
																	<span class="twitter-typeahead" style="position: relative; display: inline-block;">
																		<input id="funcion_laboral" name="funcion_laboral" type="text" class="multiselect-input js-typeahead tt-input" aria-describedby="error-job-functions" placeholder="Escoge una funci�n" autocomplete="off" spellcheck="false" dir="auto" role="combobox" aria-owns="tt-behavior39" aria-autocomplete="list" aria-expanded="false" style="position: relative; vertical-align: top; background-color: transparent;">
																	</span>
																</li>
															</ul>
														</div>
													</div>
												</span>
											</div>
											<div id="employment-type-region" class="employment-type-field field">
												<div>
													<label for="job-employment-type-input" class="required categorization-field-label">
														Tipo de empleo
													</label>
													<span class="select-overflow-wrapper">
														<select id="tipo_empleo" name="tipo_empleo" data-error-class="show-error-jobs-employment-type" class="job-posting-form-field"  data-model-param="employmentType" aria-describedby="error-jobs-employment-type" required="">
															<option value="">
																Selecciona uno
															</option>
															<option value="Jornada completa" selected="">
																Jornada completa
															</option>
															<option value="Media jornada">
																Media jornada
															</option>
															<option value="Contrato por obra">
																Contrato por obra
															</option>
															<option value="Temporal">
																Temporal
															</option>
															<option value="Voluntario">
																Voluntario
															</option>
															<option value="Pr�cticas">
																Pr�cticas
															</option>
														</select>
													</span>
												</div>
											</div>
										</div>
										<div class="job-level-industry category-row">
											<div class="industry-field field">
												<span class="job-industries-typeahead typeaheads" id="jobs-industries-region">
													<div class="typeahead-input-wrapper">
														<label for="multiselect-job-industries-typeahead" class="required categorization-field-label">
															Sector de la empresa
														</label>
														<div class="categorization-multiselect" tabindex="0">
															<ul class="multiselect-typeahead-wrapper" id="job-industries">
																<li class="multiselect-input-wrapper">
																	<span class="twitter-typeahead" style="position: relative; display: inline-block;">
																		<input id="sector_empresa" name="sector_empresa" type="text" class="multiselect-input js-typeahead tt-hint" aria-describedby="error-job-industries" readonly="" autocomplete="off" spellcheck="false" tabindex="-1" dir="ltr" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; opacity: 1; background: none 0% 0% / auto repeat scroll padding-box border-box rgba(0, 0, 0, 0);"><input type="text" class="multiselect-input js-typeahead tt-input" id="multiselect-job-industries-typeahead" aria-describedby="error-job-industries" placeholder="Escoge un sector" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;" role="combobox" aria-owns="tt-behavior47" aria-autocomplete="list" aria-expanded="false">
																	</span>
																</li>
															</ul>
														</div>
													</div>
												</span>
											</div>
											<div id="seniority-level-region" class="seniority-level-field field">
												<div>
													<label for="job-seniority-level-input" class="required categorization-field-label">
														Nivel de antiguedad
													</label>
													<span class="select-overflow-wrapper">
														<select id="nivel_antiguedad" name="nivel_antiguedad" class="job-posting-form-field" data-error-class="show-error-jobs-seniority-level" data-model-param="experienceLevel" aria-describedby="error-jobs-seniority-level" required="">
															<option value="">
																Selecciona..
															</option>
															<option value="Pr�cticas">
																Pr�cticas
															</option>
															<option value="Sin experiencia">
																Sin experiencia
															</option>
															<option value="Algo de responsabilidad">
																Algo de responsabilidad
															</option>
															<option value="Mando intermedio" selected="">
																Mando intermedio
															</option>
															<option value="Director">
																Director
															</option>
															<option value="Ejecutivo">
																Ejecutivo
															</option>
														</select>
													</span>
												</div>
											</div>
										</div>
									</fieldset>
									<fieldset class="job-description-fields editor-field">
										<legend>
											Descripci�n del empleo
										</legend>
										<div class="job-description-field field">
											<div id="job-description-edit">
												<div>
													<label for="topcard-summary" class="pe-form-field__label label-text">
														Descripci�n del empleo
													</label>             
													<textarea id="descripcion" name="descripcion" value="" class="ember-text-area pe-form-field__textarea ember-view"></textarea>
												</div>
											</div>
										</div>
									</fieldset>
									<fieldset class="company-description-fields editor-field">
										<legend>
											Descripci�n de la empresa
										</legend>
										<div class="company-description-field">
											<div id="company-description-edit">
												<div>
													<label for="topcard-summary" class="pe-form-field__label label-text">
														Descripci�n de la empresa
													</label>              
													<textarea id="descripcion_empleo" name="descripcion_empleo" value="" class="ember-text-area pe-form-field__textarea ember-view"></textarea>
												</div>
											</div>
										</div>
									</fieldset>
									<div id="apply-method-region">
										<div>
											<fieldset class="apply-options-fields">
												<legend>
													�C�mo te gustar�a que la gente solicite el empleo?
												</legend>
												<div class="job-internal-apply-field field">
													<input type="radio" id="job-internal-apply-input" data-model-param="jobApplyPreference" name="job-application-method" value="" checked="">
													<label for="job-internal-apply-input">
														<strong>
															Recomendado:
														</strong> 
															Permitir a los candidatos solicitar el empleo con sus perfiles y notificarme por correo electr�nico
													</label>
													<p class="job-internal-apply-email-field field">
														<label for="job-internal-apply-email-input">
															Direcci�n de correo electr�nico
														</label>
														<input type="text" id="email" name="email" value="" data-model-param="contactEmail" placeholder="ejemplo@ejemplo.com">
													</p>
												</div>
												<div class="job-external-apply-field field disabled">
													<input type="radio" id="job-external-apply-input" data-model-param="jobApplyPreference" name="job-application-method" value="">
													<label for="job-external-apply-input">
														Dirigir a los candidatos a un sitio web externo en el que puedan enviar su solicitud
													</label>
													<p class="job-external-apply-url-field field">
														<label for="job-external-apply-url-input">
															Introduce una URL
														</label>
														<input type="text" id="url_page" name="url_page" value="" data-model-param="externalApplyUrl" placeholder="http://tuempresa.com/empleo123" class="disabled" disabled="">
													</p>
												</div>
											</fieldset>
										</div>
									</div>
									<div class="form-options">
										<button type="submit" class="continue-button form-action-btn">
											<span aria-hidden="true">
												Publicar
											</span>
										</button>
									</div>
								</article>
							</div>
						</div>
					</div>
				</div>
			</form>
		</section>


		<!-- javascripts -->
		<script src="js/social/jquery.js"></script>
		<script src="js/social/jquery-ui-1.10.4.min.js"></script>
		<script src="js/social/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="js/social/jquery-ui-1.9.2.custom.min.js"></script>
		<!-- bootstrap -->
		<script src="js/social/bootstrap.min.js"></script>
		<!-- nice scroll -->
		<script src="js/social/jquery.scrollTo.min.js"></script>
		<script src="js/social/jquery.nicescroll.js" type="text/javascript"></script>
		<!-- charts scripts -->
		<script src="assets/jquery-knob/js/jquery.knob.js"></script>
		<script src="js/social/jquery.sparkline.js" type="text/javascript"></script>
		<script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script src="js/social/owl.carousel.js" ></script>
		<!-- jQuery full calendar -->
		<script src="js/social/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
		<script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
		<!--script for this page only-->
		<script src="js/social/calendar-custom.js"></script>
		<script src="js/social/jquery.rateit.min.js"></script>
		<!-- custom select -->
		<script src="js/social/jquery.customSelect.min.js" ></script>
		<script src="assets/chart-master/Chart.js"></script>
	   
		<!--custome script for all page-->
		<script src="js/social/scripts.js"></script>
		<!-- custom script for this page-->
		<script src="js/social/sparkline-chart.js"></script>
		<script src="js/social/easy-pie-chart.js"></script>
		<script src="js/social/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="js/social/jquery-jvectormap-world-mill-en.js"></script>
		<script src="js/social/xcharts.min.js"></script>
		<script src="js/social/jquery.autosize.min.js"></script>
		<script src="js/social/jquery.placeholder.min.js"></script>
		<script src="js/social/gdp-data.js"></script>  
		<script src="js/social/morris.min.js"></script>
		<script src="js/social/sparklines.js"></script>    
		<script src="js/social/charts.js"></script>
		<script src="js/social/jquery.slimscroll.min.js"></script>

		
		<script type="text/javascript">

if ( typeof( WebTracking ) !== 'undefined' ) {
  WebTracking.saveWebActionTrackURL =  "\/lite\/secure-web-action-track?csrfToken=ajax%3A6913851243134043551";
}
</script>



	<script>(function(root){
var jsRoutes = {}; (function(_root){
var _nS = function(c,f,b){var e=c.split(f||"."),g=b||_root,d,a;for(d=0,a=e.length;d<a;d++){g=g[e[d]]=g[e[d]]||{}}return g}
var _qS = function(items){var qs = ''; for(var i=0;i<items.length;i++) {if(items[i]) qs += (qs ? '&' : '') + items[i]}; return qs ? ('?' + qs) : ''}
var _s = function(p,s){return p+((s===true||(s&&s.secure))?'s':'')+'://'}
var _wA = function(r){return {ajax:function(c){c=c||{};c.url=r.url;c.type=r.method;return jQuery.ajax(c)}, method:r.method,type:r.method,url:r.url,absoluteURL: function(s){return _s('http',s)+'www.linkedin.com'+r.url},webSocketURL: function(s){return _s('ws',s)+'www.linkedin.com'+r.url}}}
_nS('controllers.api.DirectToCheckoutController'); _root.controllers.api.DirectToCheckoutController.directToCheckout = 
        function(companyName,companyId,title,titleId,trk,routerRedirected) {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting/directToCheckout" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("companyName", companyName), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("companyId", companyId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("title", title), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleId", titleId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("routerRedirected", routerRedirected)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.postJob = 
        function(promoCode,trk,sourcingChannelId) {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("promoCode", promoCode), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("sourcingChannelId", sourcingChannelId)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.saveJob = 
        function(promoCode,trk,sourcingChannelId) {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting/saveJob" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("promoCode", promoCode), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("sourcingChannelId", sourcingChannelId)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getSuggestedJobQualifications = 
        function(industries,jobFunctions,maybeLocationId,maybeJobTitleId,maybeExperienceLevel,maybeCompanyId,maybeJobDescription) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/jobPostingSuggestions" + _qS([(function(k,vs){var l=vs&&vs.length,r=[],i=0;for(;i<l;i++){r[i]=(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,vs[i])}return r.join('&')})("industries", industries), (function(k,vs){var l=vs&&vs.length,r=[],i=0;for(;i<l;i++){r[i]=(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,vs[i])}return r.join('&')})("jobFunctions", jobFunctions), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeLocationId", maybeLocationId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeJobTitleId", maybeJobTitleId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeExperienceLevel", maybeExperienceLevel), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeCompanyId", maybeCompanyId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeJobDescription", maybeJobDescription)])})
        }
      
_nS('controllers.api.SponsorJobController'); _root.controllers.api.SponsorJobController.getSponsoredUpsellModel = 
        function(jobFunctions,locationId,titleId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getSponsoredUpsellModel" + _qS([(function(k,vs){var l=vs&&vs.length,r=[],i=0;for(;i<l;i++){r[i]=(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,vs[i])}return r.join('&')})("jobFunctions", jobFunctions), (function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("locationId", locationId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleId", titleId)])})
        }
      
_nS('controllers.api.SponsorJobController'); _root.controllers.api.SponsorJobController.sponsorAndPurchaseJob = 
        function(jobPostingId,promoCode,trk) {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting/sponsorAndPurchaseAJob/" + (function(k,v) {return v})("jobPostingId", jobPostingId) + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("promoCode", promoCode), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk)])})
        }
      
_nS('controllers.api.JobPostingWowController'); _root.controllers.api.JobPostingWowController.getJobMetrics = 
        function(title,titleId,location) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/jobPostMetrics/" + (function(k,v) {return v})("title", encodeURIComponent(title)) + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleId", titleId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("location", location)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getCompanyPrefillData = 
        function(companyId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getCompanyPrefillData" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("companyId", companyId)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.processJob = 
        function(jobId,startMillis) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/processJob/" + (function(k,v) {return v})("jobId", jobId) + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("startMillis", startMillis)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.downgradeWrappedJob = 
        function(encryptedPosterId) {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting/downgradeWrappedJob" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("encryptedPosterId", encryptedPosterId)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.processVcGrant = 
        function() {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/processVcGrant"})
        }
      
_nS('controllers.api.TrackingController'); _root.controllers.api.TrackingController.sendPageTrackingEvent = 
        function(pageKey,trk) {
          return _wA({method:"POST", url:"/mjobs/api/" + "track" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("pageKey", pageKey), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk)])})
        }
      
_nS('controllers.api.TrackingController'); _root.controllers.api.TrackingController.trackWowClickPostButton = 
        function(trk,titleName) {
          return _wA({method:"POST", url:"/mjobs/api/" + "trackWowClickPostButton" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleName", titleName)])})
        }
      
_nS('controllers.api.TrackingController'); _root.controllers.api.TrackingController.trackTitleFieldChange = 
        function(trk,titleName) {
          return _wA({method:"POST", url:"/mjobs/api/" + "trackTitleFieldChange" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleName", titleName)])})
        }
      
_nS('controllers.api.TrackingController'); _root.controllers.api.TrackingController.trackPromoEligibility = 
        function() {
          return _wA({method:"POST", url:"/mjobs/api/" + "tracking/trackPromoEligibility"})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getSeats = 
        function(contractId,accessibleContractIds) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getSeats/" + (function(k,v) {return v})("contractId", contractId) + _qS([(function(k,vs){var l=vs&&vs.length,r=[],i=0;for(;i<l;i++){r[i]=(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,vs[i])}return r.join('&')})("accessibleContractIds", accessibleContractIds)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getPrefillJobId = 
        function(companyName,title,maybeLocationId,maybeCompanyId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getPrefillJobId" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("companyName", companyName), (function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("title", title), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeLocationId", maybeLocationId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeCompanyId", maybeCompanyId)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getPrefillJobPosting = 
        function(jobId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getPrefillJobPosting" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("jobId", jobId)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.validateJobTitleTierChange = 
        function() {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting/validateJobTitleTierChange"})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getJobTitleTier = 
        function(titleId,titleName) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getJobTitleTier" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleId", titleId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleName", titleName)])})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getJobTitleTierAndPrice = 
        function(titleId,titleName,locationId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getJobTitleTierAndPrice" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleId", titleId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleName", titleName), (function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("locationId", locationId)])})
        }
      
_nS('controllers.api.StandardizedTitleController'); _root.controllers.api.StandardizedTitleController.findTitles = 
        function(titleName,jobId,locationId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/findStandardizedTitles" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("titleName", titleName), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("jobId", jobId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("locationId", locationId)])})
        }
      
_nS('controllers.api.DailyBudgetController'); _root.controllers.api.DailyBudgetController.getDailyBudgetModel = 
        function(jobId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/postPaid/getDailyBudgetModel" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("jobId", jobId)])})
        }
      
_nS('controllers.api.DailyBudgetController'); _root.controllers.api.DailyBudgetController.getDailyBudgetForecast = 
        function(jobId,currency,dailyBudget,recommendedDailyBudget,inputLifetimeBudget,recommendedLifetimeBudget) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/postPaid/getDailyBudgetForecast" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("jobId", jobId), (function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("currency", currency), (function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("dailyBudget", dailyBudget), (function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("recommendedDailyBudget", recommendedDailyBudget), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("inputLifetimeBudget", inputLifetimeBudget), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("recommendedLifetimeBudget", recommendedLifetimeBudget)])})
        }
      
_nS('controllers.api.DailyBudgetController'); _root.controllers.api.DailyBudgetController.checkout = 
        function(promoCode,trk) {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting/postPaid/checkout" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("promoCode", promoCode), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk)])})
        }
      
_nS('controllers.api.SkillController'); _root.controllers.api.SkillController.deriveSkills = 
        function() {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting/deriveSkills"})
        }
      
_nS('controllers.api.SkillController'); _root.controllers.api.SkillController.getSkillsByJobId = 
        function(jobId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getSkillsByJobId/" + (function(k,v) {return v})("jobId", jobId)})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getPrefillData = 
        function(companyName,title,titleId,maybeLocationId,maybeCompanyId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getPrefillData" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("companyName", companyName), (function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("title", title), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleId", titleId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeLocationId", maybeLocationId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("maybeCompanyId", maybeCompanyId)])})
        }
      
_nS('controllers.api.TrackingController'); _root.controllers.api.TrackingController.trackUpdateSkill = 
        function() {
          return _wA({method:"POST", url:"/mjobs/api/" + "jobPosting/trackSkill"})
        }
      
_nS('controllers.api.JobPostingController'); _root.controllers.api.JobPostingController.getEstimatedCompensation = 
        function(countryCode,titleId,companyId,regionCode,placeCode) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPosting/getEstimatedCompensation" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("countryCode", countryCode), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("titleId", titleId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("companyId", companyId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("regionCode", regionCode), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("placeCode", placeCode)])})
        }
      
_nS('controllers.api.JobPostingInferringController'); _root.controllers.api.JobPostingInferringController.getInferredJobFunctions = 
        function(titleId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPostingInferring/getInferredFunctions" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("titleId", titleId)])})
        }
      
_nS('controllers.api.JobPostingInferringController'); _root.controllers.api.JobPostingInferringController.getInferredCompanyIndustries = 
        function(companyId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "jobPostingInferring/getInferredIndustries" + _qS([(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})("companyId", companyId)])})
        }
      
_nS('controllers.api.JobsTargetingController'); _root.controllers.api.JobsTargetingController.getPrefilledTargetingAttributes = 
        function(jobId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "targeting/getPrefilledTargetingAttributes/" + (function(k,v) {return v})("jobId", jobId)})
        }
      
_nS('controllers.api.JobsTargetingController'); _root.controllers.api.JobsTargetingController.saveTargetingAttributes = 
        function(promoCode,trk) {
          return _wA({method:"POST", url:"/mjobs/api/" + "targeting/saveTargetingAttributes" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("promoCode", promoCode), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk)])})
        }
      
_nS('controllers.api.JobsTargetingController'); _root.controllers.api.JobsTargetingController.postTargetingAttributes = 
        function(promoCode,trk) {
          return _wA({method:"POST", url:"/mjobs/api/" + "targeting/postTargetingAttributes" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("promoCode", promoCode), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("trk", trk)])})
        }
      
_nS('controllers.api.JobsTargetingMonitoringController'); _root.controllers.api.JobsTargetingMonitoringController.getJobPostingWithTargetingAttributes = 
        function(jobId) {
          return _wA({method:"GET", url:"/mjobs/api/" + "targeting/getJobPostingWithTargetingAttributes/" + (function(k,v) {return v})("jobId", jobId)})
        }
      
_nS('controllers.api.TypeaheadController'); _root.controllers.api.TypeaheadController.locationSuggestion = 
        function(companyId,numPreviousJobLocation,numCompanyLocation) {
          return _wA({method:"GET", url:"/mjobs/api/" + "typeahead/locationSuggestion" + _qS([(function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("companyId", companyId), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("numPreviousJobLocation", numPreviousJobLocation), (function(k,v){return v!=null?(function(k,v) {return encodeURIComponent(k)+'='+encodeURIComponent(v)})(k,v):''})("numCompanyLocation", numCompanyLocation)])})
        }
      
_nS('controllers.api.TrackingController'); _root.controllers.api.TrackingController.trackExperimentalBudget = 
        function() {
          return _wA({method:"POST", url:"/mjobs/api/" + "tracking/trackExperimentalBudget"})
        }
      
})(jsRoutes)
    ;
root.play = root.play || {};
root.play.jsRoutes = root.play.jsRoutes || {};
var extend = function (original, context) {
  for (key in context)
    if (context.hasOwnProperty(key))
      if (Object.prototype.toString.call(context[key]) === '[object Object]')
        original[key] = extend(original[key] || {}, context[key]);
      else
        original[key] = context[key];
  return original;
};
root.play.jsRoutes = extend(root.play.jsRoutes, jsRoutes);
})(this);</script>
		<script>

		  //knob
		  $(function() {
			$(".knob").knob({
			  'draw' : function () { 
				$(this.i).val(this.cv + '%')
			  }
			})
		  });

		  //carousel
		  $(document).ready(function() {
			  $("#owl-slider").owlCarousel({
				  navigation : true,
				  slideSpeed : 300,
				  paginationSpeed : 400,
				  singleItem : true

			  });
		  });

		  //custom select box

		  $(function(){
			  $('select.styled').customSelect();
		  });
		  
		  /* ---------- Map ---------- */
		$(function(){
		  $('#map').vectorMap({
			map: 'world_mill_en',
			series: {
			  regions: [{
				values: gdpData,
				scale: ['#000', '#000'],
				normalizeFunction: 'polynomial'
			  }]
			},
			backgroundColor: '#eef3f7',
			onLabelShow: function(e, el, code){
			  el.html(el.html()+' (GDP - '+gdpData[code]+')');
			}
		  });
		});

		</script>

	</body>

</html>
