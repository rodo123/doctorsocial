<!DOCTYPE html>
<html lang="en" >
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>RED MEDICA</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="EDoctor.com" />
		<meta name="keywords" content="edoctor, m�dicos, doctores, html5, css3, mobile first, responsive" />
		<meta name="author" content="NyR" />

		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
			
		<!-- Bootstrap CSS -->    
		<link href="css/social/bootstrap.min.css" rel="stylesheet">
		<!-- bootstrap theme -->
		<link href="css/social/bootstrap-theme.css" rel="stylesheet">
		<!--external css-->
		<!-- font icon -->
		<link href="css/social/elegant-icons-style.css" rel="stylesheet" />
		<link href="css/social/font-awesome.min.css" rel="stylesheet" />    
		<!-- full calendar css-->
		<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
		<link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
		<!-- easy pie chart-->
		<link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
		<!-- owl carousel -->
		<link rel="stylesheet" href="css/social/owl.carousel.css" type="text/css">
		<link href="css/social/jquery-jvectormap-1.2.2.css" rel="stylesheet">
		<!-- Custom styles -->
		<link rel="stylesheet" href="css/fullcalendar.css">
		<link href="css/social/widgets.css" rel="stylesheet">
		<link href="css/social/styleempleo.css" rel="stylesheet">
		<link href="css/social/style-responsive.css" rel="stylesheet" />
		<link href="css/social/xcharts.min.css" rel=" stylesheet"> 
		<link href="css/social/jquery-ui-1.10.4.min.css" rel="stylesheet">
	</head>

	<body>
	<!-- container section start -->
	<section id="container" class="">
        <header class="header dark-bg">
			<!--logo start-->
            <a href="index.jsp" class="logo"> <img src="images/logo2.png" alt="logo" style="width: 150px; margin-top: -8px;">
			</a>
            <!--logo end-->
			<div class="nav search-row" id="top_menu">
				<!-- Inicio de busqueda -->
                <ul class="nav top-menu">                    
                    <li>
                        <form class="navbar-form">
                            <a href="BusquedaDoctorControlle" class="buscar" placeholder="Busqueda" type="button" style="color: #fff;">Buscar</a>
                        </form>
                    </li>                    
                </ul>
                <!-- Fin de busqueda -->                
            </div>
            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    <!-- task notificatoin start -->
                    <li id="task_notificatoin_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="fa fa-home fa-lg" aria-hidden="true">
						    </i>
                            <span class="badge bg-important">6
						    </span>
                        </a>
                        <span style="font-size: 12px; line-height: 16px;">Inicio
					    </span>
                        <ul class="dropdown-menu extended tasks-bar">
                            <div class="notify-arrow notify-arrow-blue">
						    </div>
                            <li>
                                <p class="blue">You have 6 pending letter
								</p>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div class="desc">Design PSD 
										</div>
                                        <div class="percent">90%
										</div>
                                    </div>
                                </a>
                            </li>
                            <li class="external">
                                <a href="#">See All Tasks
							    </a>
                            </li>
                        </ul>
                    </li>
                    <li id="task_notificatoin_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="fa fa-users fa-lg" aria-hidden="true">
							</i>
                        </a>
                         <span style="font-size: 12px; line-height: 16px;">Mi Red</span>
                    </li>
					<li id="task_notificatoin_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i  class="fa fa-briefcase fa-lg" aria-hidden="true">
							</i>
                        </a>
                        <span style="font-size: 12px; line-height: 16px;">Trabajos
						</span>
                    </li>
                    <!-- task notificatoin end -->

                    <!-- inbox notificatoin start-->
                    <li id="mail_notificatoin_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="fa fa-comments-o fa-lg" aria-hidden="true">
							</i>
                            <span class="badge bg-important">6
							</span>
                        </a>
                        <span style="font-size: 12px; line-height: 16px;">Mensajes
						</span>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-blue">
							</div>
                            <li>
                                <p class="blue">You have 5 new messages
								</p>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="photo">
										<img alt="avatar" src="./img/avatar-mini.jpg">
										</img>
									</span>
                                    <span class="subject">
									</span>
                                    <span class="from">Greg  Martin
									</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">See all messages
								</a>
                            </li>
                        </ul>
                    </li>
                    <!-- inbox notificatoin end -->
                    <!-- alert notification start-->
                    <li id="alert_notificatoin_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="fa fa-bell-o fa-lg" aria-hidden="true">
							</i>
                        </a>
                        <span style="font-size: 12px; line-height: 16px;">Notificaciones
						</span>
                        <ul class="dropdown-menu extended notification">
                            <div class="notify-arrow notify-arrow-blue">
							</div>
                            <li>
                                <p class="blue">You have 4 new notifications
								</p>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-primary">
										<i class="icon_profile">
										</i>
									</span> 
                                    Friend Request
                                </a>
                            </li>     
                        </ul>
                    </li>
                    <!-- alert notification end-->
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="../img/avatar1_small.jpg">
                            </span>
                        </a>
                        <span style="font-size: 11px;">Yo</span>
                            <b class="caret"></b>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up">
							</div>
							<li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
							</li>
						</ul>
					</li>
                    <!-- user login dropdown end -->
				</ul>
                <!-- notificatoin dropdown end-->
                <!-- notificatoin dropdown end-->
            </div>
		</header>      
		<!--header end-->
		<br>
		<br>
		<br>
					      
		<!--main content start-->
		<section id="#fh5co-empleos" style="margin-top: 5px;">
			<div class="jobs-search-alerts">
				<div class="neptune-grid">
					<div id="ember1304" class="jobs-search-box pt3 ember-view">
						<div class="jobs-search-box__input jobs-search-box__input--keyword">
							<artdeco-typeahead id="ember1309" class="ember-view">
								<div aria-live="polite" class="artdeco-typeahead-live-region">
									<artdeco-typeahead-input id="ember1314" class="ember-view">  
										<input type="text" role="combobox" autocomplete="off" spellcheck="false" aria-autocomplete="list" aria-owns="ember1309-results" placeholder="Buscar empleos por cargo, palabra clave o empresa" aria-expanded="false">
									</artdeco-typeahead-input>
								</div>
							</artdeco-typeahead>
						</div>
						<div class="jobs-search-box__input jobs-search-box__input--location ">
							<artdeco-typeahead id="ember1315" class="ember-view">
								<div aria-live="polite" class="artdeco-typeahead-live-region">
								</div>
								<artdeco-typeahead-input id="ember1316" class="ember-view"> 
									<input type="text" role="combobox" autocomplete="off" spellcheck="false" aria-autocomplete="list" aria-owns="ember1315-results" placeholder="Ciudad, provincia/estado, c�digo postal o pa�s" aria-expanded="false">
								</artdeco-typeahead-input>
							</artdeco-typeahead>
						</div>
						<button class="jobs-search-box__submit-button button-secondary-large-inverse" data-ember-action="" data-ember-action-1317="1317">
							B�squeda
						</button>
					</div>
				</div>
			</div>
			<br>
			<section role="main" class="jobs-boxes  col-lg-10 col-lg-offset-1">
				<!--ferf-->
				<div id="ember1326" class="jobs-box jobs-box--full-width jobs-top-card ember-view">
					<ul class="jobs-nav">
						<li class="jobs-nav__item jobs-nav__item--first">
							<span class="jobs-nav-count jobs-nav-count--empty">
								<span class="jobs-nav-count__number Sans-21px-black-85-semibold-dense">0
								</span>
								<span class="Sans-15px-black-55">empleos guardados
								</span>
							</span>
						</li>
						<li class="jobs-nav__item">
							<span class="jobs-nav-count jobs-nav-count--empty">
								<span class="jobs-nav-count__number Sans-21px-black-85-semibold-dense">0
								</span>
								<span class="Sans-15px-black-55">empleos solicitados
								</span>
							</span>
						</li>
					</ul>

					<div class="jobs-post Sans-15px-black-55">
						<div class="jobs-post__label">�Est�s buscando candidatos?
						</div>
						<div class="jobs-post__p4p-hovercard-container relative mt1">
							<a href="publicarempleo.jsp" id="ember1864" class="jobs-post__btn Sans-15px-black-85-semibold ember-view">
								<span class="jobs-post__icon svg-icon-wrap">
									<span class="visually-hidden">Publicar un anuncio de empleo
									</span>
									<li-icon aria-hidden="true" type="compose-icon" size="medium">
										<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
											<g class="large-icon" style="fill: currentColor">
											</g>
										</svg>
									</li-icon>
								</span>
								Publicar un anuncio de empleo
							</a>
						</div>
					</div>
				</div>
				<div id="ember1867" class="open-candidate__modal ember-view">
					<div id="ember1880" class="ember-view">
					</div>
				</div>
				<div data-scroll-name="jobs-in-your-network">
				</div>
				<div id="ember1337" class="jobs-jymbii jobs-box jobs-box--full-width jobs-box--with-cta-large relative ember-view">
					<div class="pt5 ph5 pb0">
						<h2 class="jobs-box__title jobs-box__title--no-bottom">
							Empleos que podr�n interesarte
						</h2>
						<div id="ember1346" class="jobs-inline-pref Sans-15px-black-55 ember-view">
							<span class="a11y-text">Ubicaciones seleccionadas:
							</span>
							<span class="jobs-inline-pref__field jobs-inline-pref__field--location">Cualquier ubicaci�n
							</span>
							<span class="a11y-text">Sectores seleccionados:
							</span>
							<span class="jobs-inline-pref__field jobs-inline-pref__field--industry">
								Cualquier sector
							</span>
							<span class="a11y-text">Rango de tama�o de empresa seleccionado:
							</span>
							<span class="jobs-inline-pref__field jobs-inline-pref__field--truncated">
								De 0 a 10.000+ empleados
							</span>
							<a data-control-name="jobshome_career_interests_link" href="/jobs/career-interests/" id="ember1347" class="js-pref-close-focus link-without-hover-state link-without-visited-state ember-view">  Actualizar mis intereses de empleo
							</a>
						</div>
					</div>
					<ul class="card-list card-list--tile jobs-jymbii__list ">
						<li id="ember2256" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view">
							<a data-control-name="A_jobshome_job_link_click" href="/jobs/view/410291903/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2257" class="job-card__link-wrapper js-focusable-card ember-view"> 
								<div class="job-card__image-and-sponsored-container">
									<figure class="job-card__logo-wrapper">
										<img class="lazy-image job-card__logo-image loaded" title="Entel Perú" alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/p/3/005/0ae/11d/0905306.png">
									</figure>
									<button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2258="2258">
										<span class="job-card__dismiss-icon svg-icon-wrap">
											<li-icon aria-hidden="true" type="cancel-icon" size="small">
												<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
													<g class="small-icon" style="fill-opacity: 1">
														<path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z">
														</path>
													</g>
												</svg>
											</li-icon>
										</span>
										<span class="visually-hidden">Descartar empleo �COORDINADOR DE DESARROLLO ORGANIZACIONAL�
										</span>
									</button>
								</div>
								<div class="job-card__content-wrapper">
									<span class="visually-hidden">Cargo laboral
									</span>
									<div class="job-card__title-line">
										<h3 id="ember2259" class="job-card__title ember-view">  
											<div class="truncate-multiline--truncation-target">
												<span>COORDINADOR DE DESARROLLO 
												</span>
												<span class="truncate-multiline--last-line-wrapper">
													<span>ORGANIZACIONAL
													</span>
													<button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2490="2490">
													</button>
												</span>
											</div>
										</h3>
									</div>
									<span class="visually-hidden">Nombre de la empresa
									</span>
									<h4 class="job-card__company-name">Entel Per�
									</h4>
									<h5 class="job-card__location">
										<span class="visually-hidden">Ubicaci�n del empleo
										</span>
										Per�
									</h5>
									<div class="job-card__job-flavors-container">
										<ul id="ember2261" class="job-flavors ember-view"> 
											<li class="job-flavors__flavor-wrapper">
												<div id="ember2262" class="job-flavors__flavor job-flavors__flavor--company-recruit ember-view">  
													<div class="job-flavors__logo-container">
														<img class="lazy-image job-flavors__logo-image loaded" title="SUNAT" alt="SUNAT" src="https://media-exp2.licdn.com/mpr/mpr/shrinknp_100_100/p/3/000/213/30a/0f6e32c.png">
													</div>
													<div class="job-flavors__label">
														1 antiguo empleado
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div class="job-card__footer">
										<p class="job-card__listed-status">              
											<time class="job-card__time-badge">
												hace 5 d�as
											</time>
										</p>
									</div>
								</div>
							</a>
						</li>
						<li id="ember2264" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view">
							<a data-control-name="A_jobshome_job_link_click" href="/jobs/view/397741028/?recommendedFlavor=IN_NETWORK&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2265" class="job-card__link-wrapper js-focusable-card ember-view">  
								<div class="job-card__image-and-sponsored-container">
									<figure class="job-card__logo-wrapper">
										<img class="lazy-image job-card__logo-image loaded" title="everis Per�" alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAAYEAAAAJDBmN2Y1MzAxLWQ0ZDktNDc5ZC1hZTc3LTJkYWE1NGVhMmFlOQ.png">
									</figure>
									<button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2266="2266">
										<span class="job-card__dismiss-icon svg-icon-wrap">
											<li-icon aria-hidden="true" type="cancel-icon" size="small">
												<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
													<g class="small-icon" style="fill-opacity: 1">
														<path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z">
														</path>
													</g>
												</svg>
											</li-icon>
										</span>
										<span class="visually-hidden">Descartar empleo �Especialista Powercenter�
										</span>
									</button>
								</div>
								<div class="job-card__content-wrapper">
									<span class="visually-hidden">Cargo laboral
									</span>
									<div class="job-card__title-line">
										<h3 id="ember2267" class="job-card__title ember-view">  
											<div class="truncate-multiline--truncation-target">
												<span class="truncate-multiline--last-line-wrapper">
													<span>Especialista Powercenter
													</span>
													<button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2491="2491">
													</button>
												</span>
											</div>
										</h3>
									</div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">everis Per�</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicaci�n del empleo</span>
      Provincia de&nbsp;Lima, Per�
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2269" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2270" class="job-flavors__flavor job-flavors__flavor--in-network ember-view">
    <ul class="flavor-profile-image-container">
        <li id="ember2272" class="flavor-profile-image flavor-profile-image--size-1 ember-view">  <img class="lazy-image flavor-profile-image__image loaded" title="Leandro Javier Teves, SFC™" alt="Leandro Javier Teves, SFC™" src="https://media-exp2.licdn.com/mpr/mpr/shrinknp_100_100/p/8/005/020/311/0ee1d81.jpg">
</li>
        <li id="ember2274" class="flavor-profile-image flavor-profile-image--size-1 ember-view">  <img class="lazy-image flavor-profile-image__image loaded" title="BRYAN CESAR AGUILAR PEREZ" alt="BRYAN CESAR AGUILAR PEREZ" src="https://media-exp2.licdn.com/mpr/mpr/shrinknp_100_100/AAEAAQAAAAAAAA0gAAAAJDNhNzAwZDc4LTJkMTQtNGVkZS04YWQ3LWQzY2E3YmNhNTA4Zg.jpg">
</li>
    </ul>

  <div class="job-flavors__label">
    2 contactos
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 3 días
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>

   
    <li id="ember2276" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/423652878/?recommendedFlavor=SCHOOL_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2277" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="H&amp;M" alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/p/2/000/01b/344/28cb62d.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2278="2278">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Asistente de Remuneraciones Part Time»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2279" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span>Asistente de Remuneraciones </span><span class="truncate-multiline--last-line-wrapper"><span>Part Time</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2492="2492">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">H&amp;M</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Cajamarca, PE
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2281" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2282" class="job-flavors__flavor job-flavors__flavor--school-recruit ember-view">
  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image ghost-school loaded" title="Universidad Nacional del Callao" alt="Universidad Nacional del Callao" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
  </div>

  <div class="job-flavors__label">
    1 antiguo alumno
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 2 días
              </time></p>
<!---->        </div>

    
  </div>
</a></li>
    <li id="ember2284" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/458783483/?recommendedFlavor=SCHOOL_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2285" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="Brightstar Corp." alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAA0lAAAAJDI4NThmMjRhLTMzYzUtNDZkMi05NDI3LWUxYzNlNDk4MTRhYg.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2286="2286">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Analista de Administración - Seguros»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2287" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span>Analista de Administración - </span><span class="truncate-multiline--last-line-wrapper"><span>Seguros</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2493="2493">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Brightstar Corp.</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Santiago de Surco
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2289" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2290" class="job-flavors__flavor job-flavors__flavor--school-recruit ember-view">
  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image ghost-school" title="Universidad Nacional del Callao" alt="Universidad Nacional del Callao">
  </div>

  <div class="job-flavors__label">
    2 antiguos alumnos
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 5 días
              </time></p>
<!---->        </div>

    
  </div>
</a></li>
    <li id="ember2292" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/460395130/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2293" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="AUSTRAL GROUP S.A.A." alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAAc3AAAAJDJiY2QxZWM1LTNjNTgtNDAwOC04YTE0LTQzMjVmNDkzODM5Nw.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2294="2294">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Asistente de Logística»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2295" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Asistente de Logística</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2494="2494">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">AUSTRAL GROUP S.A.A.</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Distrito de&nbsp;San Isidro, Departamento de Lima, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2297" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2298" class="job-flavors__flavor job-flavors__flavor--company-recruit ember-view">  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image" title="SUNAT" alt="SUNAT">
  </div>

  <div class="job-flavors__label">
    1 antiguo empleado
  </div>
</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 2 días
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2300" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/410291601/?refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2301" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="NG Restaurants S.A." alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAAggAAAAJGY0YmM3MzBjLWYyMTItNDg0Ni1hODEzLTljNzNjYzY5MTdlZQ.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2302="2302">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Analista de operaciones»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2303" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Analista de operaciones</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2495="2495">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">NG Restaurants S.A.</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Distrito de&nbsp;Surco, Departamento de Lima, Peru
    </h5>

<!---->
<!---->
        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 5 días
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2306" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/455235570/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2307" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="BBVA Continental" alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAAyHAAAAJGVhYTkyN2MwLWI0NTQtNDlkZi1hMjMwLTVjNTA2Zjk1MGU1Ng.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2308="2308">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «ESPECIALISTA HOLDING SYSTEMS»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2309" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>ESPECIALISTA HOLDING SYSTEMS</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2496="2496">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">BBVA Continental</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Lima, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2311" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2312" class="job-flavors__flavor job-flavors__flavor--company-recruit ember-view">  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image" title="SUNAT" alt="SUNAT">
  </div>

  <div class="job-flavors__label">
    2 antiguos empleados
  </div>
</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 1 semana
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2314" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/448575588/?refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2315" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image ghost-company" title="Anonima" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2316="2316">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Gerente de administración de  Clientes»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2317" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span>Gerente de administración de  </span><span class="truncate-multiline--last-line-wrapper"><span>Clientes</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2497="2497">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Anonima</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Lima, Peru
    </h5>

<!---->
<!---->
        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 3 días
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2320" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/409037382/?refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2321" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image ghost-company" title="Confidencial" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2322="2322">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Secretaria»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2323" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Secretaria</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2498="2498">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Confidencial</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      San Isidro District, Lima Region, Peru
    </h5>

<!---->
<!---->
        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 3 semanas
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2326" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/461512749/?recommendedFlavor=SCHOOL_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2327" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="Alicorp" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2328="2328">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Ejecutivo de Negocio ( Trade - Comercial)»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2329" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span>Ejecutivo de Negocio ( Trade - </span><span class="truncate-multiline--last-line-wrapper"><span>Comercial)</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2499="2499">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Alicorp</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Perú
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2331" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2332" class="job-flavors__flavor job-flavors__flavor--school-recruit ember-view">
  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image ghost-school" title="Universidad Nacional del Callao" alt="Universidad Nacional del Callao">
  </div>

  <div class="job-flavors__label">
    28 antiguos alumnos
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 1 día
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2334" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/459847359/?recommendedFlavor=HIDDEN_GEM&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2335" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image ghost-company" title="Confidential" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2336="2336">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Practicante SAP GRC»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2337" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Practicante SAP GRC</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2500="2500">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Confidential</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Lima Province, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2339" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2344" class="job-flavors__flavor job-flavors__flavor--hidden-gem ember-view"><span class="job-flavors__icon svg-icon-wrap"><li-icon aria-hidden="true" type="clock-icon" size="medium"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><path d="M12,2C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.5,0,10-4.5,10-10C22,6.5,17.5,2,12,2zM12,20.1c-4.5,0-8.1-3.6-8.1-8.1c0-4.5,3.6-8.1,8.1-8.1s8.1,3.6,8.1,8.1C20.1,16.5,16.5,20.1,12,20.1zM12.9,11.6l3.3,2.4l-1.1,1.4l-3.6-2.6c-0.2-0.2-0.4-0.5-0.4-0.8V6h1.8V11.6z" class="large-icon" style="fill: currentColor"></path></svg></li-icon></span>

<div class="job-flavors__label">
  Sé de los <strong>primeros 10 solicitantes</strong>
</div>
</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 3 días
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2346" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/414948430/?recommendedFlavor=SCHOOL_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2347" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="L'Oréal" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2348="2348">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Practicante- ESAN»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2349" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Practicante- ESAN</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2501="2501">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">L'Oréal</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Lima
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2351" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2352" class="job-flavors__flavor job-flavors__flavor--school-recruit ember-view">
  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image ghost-school" title="Universidad Nacional del Callao" alt="Universidad Nacional del Callao">
  </div>

  <div class="job-flavors__label">
    1 antiguo alumno
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 2 días
              </time></p>
<!---->        </div>

    
  </div>
</a></li>
    <li id="ember2354" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/455854879/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2355" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="Auna" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2356="2356">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Asistente de Dirección Médica»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2357" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Asistente de Dirección Médica</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2502="2502">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Auna</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Piura, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2359" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2360" class="job-flavors__flavor job-flavors__flavor--company-recruit ember-view">  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image" title="SUNAT" alt="SUNAT">
  </div>

  <div class="job-flavors__label">
    1 antiguo empleado
  </div>
</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 1 semana
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2362" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/440195401/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2363" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="BanBif" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2364="2364">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Especialista de Estrategias de Administración de Portafolio»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2365" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span>Especialista de Estrategias de </span><span class="truncate-multiline--last-line-wrapper"><span>Administración de Portafolio</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2503="2503">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">BanBif</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Lima, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2367" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2368" class="job-flavors__flavor job-flavors__flavor--company-recruit ember-view">  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image" title="SUNAT" alt="SUNAT">
  </div>

  <div class="job-flavors__label">
    1 antiguo empleado
  </div>
</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 4 semanas
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2370" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/453781375/?refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2371" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="BitInka Limited" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2372="2372">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Analista financiero»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2373" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Analista financiero</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2504="2504">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">BitInka Limited</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Lima, Peru
    </h5>

<!---->
<!---->
        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 1 semana
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2376" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/443061596/?recommendedFlavor=SCHOOL_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2377" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="Adecco" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2378="2378">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Especialista de Regulación»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2379" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Especialista de Regulación</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2505="2505">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Adecco</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Lima, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2381" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2382" class="job-flavors__flavor job-flavors__flavor--school-recruit ember-view">
  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image ghost-school" title="Universidad Nacional del Callao" alt="Universidad Nacional del Callao">
  </div>

  <div class="job-flavors__label">
    4 antiguos alumnos
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 3 semanas
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2384" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/454433963/?recommendedFlavor=SCHOOL_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2385" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="Innova Schools" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2386="2386">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Jefe de proyectos T.I.»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2387" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Jefe de proyectos T.I.</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2506="2506">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Innova Schools</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Lima, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2389" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2390" class="job-flavors__flavor job-flavors__flavor--school-recruit ember-view">
  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image ghost-school" title="Universidad Nacional del Callao" alt="Universidad Nacional del Callao">
  </div>

  <div class="job-flavors__label">
    1 antiguo alumno
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 1 semana
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2392" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/409049524/?recommendedFlavor=SCHOOL_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2393" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="Enel Perú" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2394="2394">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Especialista de procesos y sistemas»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2395" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span>Especialista de procesos y </span><span class="truncate-multiline--last-line-wrapper"><span>sistemas</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2507="2507">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Enel Perú</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Lima, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2397" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2398" class="job-flavors__flavor job-flavors__flavor--school-recruit ember-view">
  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image ghost-school" title="Universidad Nacional del Callao" alt="Universidad Nacional del Callao">
  </div>

  <div class="job-flavors__label">
    15 antiguos alumnos
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 2 semanas
              </time></p>
<!---->        </div>

    
  </div>
</a></li>
    <li id="ember2400" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/439673428/?recommendedFlavor=SCHOOL_RECRUIT&amp;refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2401" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image" title="KPMG en Perú" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2402="2402">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Supervisor de Proyectos IT»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2403" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Supervisor de Proyectos IT</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2508="2508">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">KPMG en Perú</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Provincia de&nbsp;Lima, Peru
    </h5>

<!---->
      <div class="job-card__job-flavors-container">
        <ul id="ember2405" class="job-flavors ember-view">  <li class="job-flavors__flavor-wrapper">
    <div id="ember2406" class="job-flavors__flavor job-flavors__flavor--school-recruit ember-view">
  <div class="job-flavors__logo-container">
    <img class="lazy-image job-flavors__logo-image ghost-school" title="Universidad Nacional del Callao" alt="Universidad Nacional del Callao">
  </div>

  <div class="job-flavors__label">
    4 antiguos alumnos
  </div>

</div>
  </li>
</ul>
      </div>

        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 4 semanas
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
    <li id="ember2408" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/459723283/?refId=3ab53d1e-fb4c-4208-b6cd-3a1f48fca7eb&amp;trk=d_flagship3_job_home" id="ember2409" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image ghost-company" title="Confidential" alt="" height="64" width="64">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2410="2410">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «Practicante SAP Logistico»</span>
        </button>
        </div>

  <div class="job-card__content-wrapper">
<!---->    <span class="visually-hidden">Cargo laboral</span>
      <div class="job-card__title-line">
        <h3 id="ember2411" class="job-card__title ember-view">  <div class="truncate-multiline--truncation-target"><span class="truncate-multiline--last-line-wrapper"><span>Practicante SAP Logistico</span><button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2509="2509">
<!---->  </button></span></div>
  
</h3>
      </div>

    <span class="visually-hidden">Nombre de la empresa</span>
    <h4 class="job-card__company-name">Confidential</h4>
    <h5 class="job-card__location">
      <span class="visually-hidden">Ubicación del empleo</span>
      Lima Province, Peru
    </h5>

<!---->
<!---->
        <div class="job-card__footer">
          <p class="job-card__listed-status">              <time class="job-card__time-badge">
                hace 4 días
              </time></p>
            <div class="job-card__easy-apply">
              <figure class="job-card__easy-apply-icon">
                <span class="svg-icon-wrap"><li-icon aria-hidden="true" type="linkedin-icon"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon"><g style="fill: currentColor" class="solid-icon">
        <rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
        <path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z"></path>
      </g></svg></li-icon></span>
              </figure>
              <p class="job-card__easy-apply-text">Solicitud sencilla</p>
            </div>
        </div>

    
  </div>
</a></li>
</ul>

<div id="ember1430" class="fetch-more-items cta-wrap ember-view">  <button data-ember-action="" data-ember-action-1431="1431">
    Ver más empleos
    <span class="svg-icon-wrap"><span class="visually-hidden">Ver más empleos</span><li-icon aria-hidden="true" type="chevron-down-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M8,9l5.93-4L15,6.54l-6.15,4.2a1.5,1.5,0,0,1-1.69,0L1,6.54,2.07,5Z"></path>
      </g></svg></li-icon></span>
  </button>
</div>

</div>
                   
                   
<!-nelsi fin-->

<ul id="menu" class="card-list card-list--tile jobs-jymbii__list ">
    <li id="ember2042" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view col-lg-1"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/418863305/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=0885b2d3-7250-4687-bc27-14146344153f&amp;trk=d_flagship3_job_home" id="ember2043" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="Entel Perú" alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/p/3/005/0ae/11d/0905306.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2044="2044">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «EJECUTIVO DE CORPORACIONES»</span>
        </button>
        </div>

  
</a></li>
<li id="ember2042" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/418863305/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=0885b2d3-7250-4687-bc27-14146344153f&amp;trk=d_flagship3_job_home" id="ember2043" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="Entel Perú" alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/p/3/005/0ae/11d/0905306.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2044="2044">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «EJECUTIVO DE CORPORACIONES»</span>
        </button>
        </div>

  
</a></li>
<li id="ember2042" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/418863305/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=0885b2d3-7250-4687-bc27-14146344153f&amp;trk=d_flagship3_job_home" id="ember2043" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="Entel Perú" alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/p/3/005/0ae/11d/0905306.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2044="2044">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «EJECUTIVO DE CORPORACIONES»</span>
        </button>
        </div>

  
</a></li>
<li id="ember2042" class="jobs-jymbii__list-item card-list__item job-card job-card--tile job-card card-list__item job-card--tile ember-view"><a data-control-name="A_jobshome_job_link_click" href="/jobs/view/418863305/?recommendedFlavor=COMPANY_RECRUIT&amp;refId=0885b2d3-7250-4687-bc27-14146344153f&amp;trk=d_flagship3_job_home" id="ember2043" class="job-card__link-wrapper js-focusable-card ember-view">  <div class="job-card__image-and-sponsored-container">
    <figure class="job-card__logo-wrapper">
      <img class="lazy-image job-card__logo-image loaded" title="Entel Perú" alt="" height="64" width="64" src="https://media-exp2.licdn.com/mpr/mpr/shrink_100_100/p/3/005/0ae/11d/0905306.png">
    </figure>
        <button data-control-name="jobshome_delete_job_click" class="job-card__dismiss-button" data-ember-action="" data-ember-action-2044="2044">
          <span class="job-card__dismiss-icon svg-icon-wrap"><li-icon aria-hidden="true" type="cancel-icon" size="small"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="small-icon" style="fill-opacity: 1">
        <path d="M13,4.32L9.31,8,13,11.69,11.69,13,8,9.31,4.31,13,3,11.69,6.69,8,3,4.31,4.31,3,8,6.69,11.68,3Z"></path>
      </g></svg></li-icon></span>
          <span class="visually-hidden">Descartar empleo «EJECUTIVO DE CORPORACIONES»</span>
        </button>
        </div>

  
</a></li>
</ul>
                
              
              
              <!-- page end-->
          </section>
          
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->

    <!-- javascripts -->
    <script src="js/social/jquery.js"></script>
    <script src="js/social/jquery-ui-1.10.4.min.js"></script>
    <script src="js/social/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/social/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="js/social/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="js/social/jquery.scrollTo.min.js"></script>
    <script src="js/social/jquery.nicescroll.js" type="text/javascript"></script>
    <!-- charts scripts -->
    <script src="assets/jquery-knob/js/jquery.knob.js"></script>
    <script src="js/social/jquery.sparkline.js" type="text/javascript"></script>
    <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="js/social/owl.carousel.js" ></script>
    <!-- jQuery full calendar -->
    <<script src="js/social/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
    <script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="js/social/calendar-custom.js"></script>
    <script src="js/social/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="js/social/jquery.customSelect.min.js" ></script>
    <script src="assets/chart-master/Chart.js"></script>
   
    <!--custome script for all page-->
    <script src="js/social/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="js/social/sparkline-chart.js"></script>
    <script src="js/social/easy-pie-chart.js"></script>
    <script src="js/social/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="js/social/jquery-jvectormap-world-mill-en.js"></script>
    <script src="js/social/xcharts.min.js"></script>
    <script src="js/social/jquery.autosize.min.js"></script>
    <script src="js/social/jquery.placeholder.min.js"></script>
    <script src="js/social/gdp-data.js"></script>  
    <script src="js/social/morris.min.js"></script>
    <script src="js/social/sparklines.js"></script>    
    <script src="js/social/charts.js"></script>
    <script src="js/social/jquery.slimscroll.min.js"></script>
  <script>

      //knob
      $(function() {
        $(".knob").knob({
          'draw' : function () { 
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
          $("#owl-slider").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });
      
      /* ---------- Map ---------- */
    $(function(){
      $('#map').vectorMap({
        map: 'world_mill_en',
        series: {
          regions: [{
            values: gdpData,
            scale: ['#000', '#000'],
            normalizeFunction: 'polynomial'
          }]
        },
        backgroundColor: '#eef3f7',
        onLabelShow: function(e, el, code){
          el.html(el.html()+' (GDP - '+gdpData[code]+')');
        }
      });
    });

  </script>

  </body>
</html>
