<%@page import="edoctor.com.beans.EnterpriseBean"%>
<%@page import="edoctor.com.beans.DoctorBean"%>
<%@page import="edoctor.com.beans.DistritoBean"%>
<%@page import="edoctor.com.beans.ProvinciaBean"%>
<%@page import="edoctor.com.beans.RegionBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="edoctor.com.beans.SpecialityBean"%>

<!DOCTYPE html>

	<html class="no-js"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RED MEDICA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="EDoctor.com" />
        <meta name="keywords" content="edoctor, medicos, doctores, html5, css3, mobile first, responsive" />
        <meta name="author" content="NyR" />


        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">

        <!-- Animate.css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="css/icomoon.css">
        <!-- Simple Line Icons -->
        <link rel="stylesheet" href="css/simple-line-icons.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <!-- Style -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/hover.css">


        <!-- Modernizr JS -->
        <script src="js/modernizr-2.6.2.min.js"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>
    
	<body>
        

        <section id="fh5co-explore" class="search-background" data-section="services">
            <div class="container ">
                <h3>
				</h3>
                <p>
				</p>
                <form class="contact-form contact-form-search backgroud-transparent-black">
					<div class="row"> 
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<label for="nombres" class="sr-only">Nombres</label>
							<input name="nombre"  type="name" class="form-control" id="name" placeholder="Nombres" size="20">
						</div>
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<label for="apellidos" class="sr-only">Apellidos</label>
							<input name="apellido" type="name" class="form-control"  placeholder="Apellidos" size="20">
						</div>
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<label for="cmp" class="sr-only">CMP</label>
							<input name="cmp" type="name" class="form-control"  placeholder="CMP" size="20">
						</div>
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<label for="trabajo" class="sr-only">Trabajo</label>
							<input name="trabajo" type="name" class="form-control"  placeholder="Trabajo" size="20">
						</div>
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">          
							<select id = "trabajoLista" name = "trabajoLista" class="form-control">
								<option value="">Selecciona un Hospital</option>
								<% List<EnterpriseBean> listaEnterprise = (ArrayList<EnterpriseBean>) request.getAttribute("listEnterprise");
								%>
								<% for (EnterpriseBean enterpriseBean : listaEnterprise) {%>
								<option value="<%=enterpriseBean.getEn_name()%>"> <%= enterpriseBean.getEn_name()%> </option>
								<%}   
								%>
						   </select>
						</div>
						<div class="form-group col-md-4 col-sm-6 col-xs-6 col-xxs-12">
							<select id = "especialidad" name = "especialidad" class="form-control">
								<option value="">Selecciona una Especialidad</option>
								<% List<SpecialityBean> listaEspecialidades = (ArrayList<SpecialityBean>) request.getAttribute("listEspecialidades");
									int i = 1;
								%>
								<% for (SpecialityBean sp : listaEspecialidades) {%>
								<option value="<%=sp.getSp_name()%>"> <%= sp.getSp_name()%> </option>
								<% i++;
								}%>
							</select>
						</div>
						<div class="form-group col-md-12 col-sm-12 col-xs-12">
							<input type="submit" id="btn-submit" class="btn btn-primary btn-send-message btn-md col-md-12 col-sm-12 col-xs-12 " value="Buscar">
						</div>
					</div>
				</form>
			</div>
        </section>
        
		
		<section id="fh5co-cards">
            <div class="container ">
                <% int counter = 1;
                List<DoctorBean> listaDoctor = (ArrayList<DoctorBean>) request.getAttribute("listDoctor");
                if(listaDoctor.size()==0){
                %>
					<div style=" text-align:center; ">
                        <img src="images/pediatrician.png" style="width:100px; text-align:center; height:100px;" />
                        <br><br>
                        <h4>Encuentre al medico de su preferencia</h4>
                    </div>
                <% }                       
                for (DoctorBean doctorBean : listaDoctor)
				{%>
                <div class="  col-md-6 col-sm-6">     
                    <div class="card backgroud-card">
                        <row>
                            <div class="col-md-4 col-sm-12 col-xs-4 col-xxs-12" style="padding-left: 0px; padding-right: 0px;">
								<img src="images/default_doctor.png" style="background:center no-repeat;  background-size: cover;  height: 180px;  padding-left: 0px;  padding-right: 0px;"/>
						    </div>
							<form name="perfil" method="post" action="PerfilController/selectPerfil.do">
							<div class="col-md-8 col-sm-12 col-xs-8 col-xxs-12">
								<div class="car-top">
									<h4 class="title-card-doctor "><% out.print(doctorBean.getDoc_fname());%> <% out.print(doctorBean.getDoc_lname());%></h4>
								</div>
								<div class="">
									<ul class="profile-list">
												<li class="clearfix">
                                                    <strong class="title"><i class="fa fa-id-card-o" aria-hidden="true" style="color:#730053"></i> CMP </strong>
                                                    <span class="cont"><% out.print(doctorBean.getDoc_cmp());%></span>
													<input type="hidden" name="cmp" value="<% out.print(doctorBean.getDoc_cmp());%>" style="visibility:hidden">
                                                </li>
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-phone fa-lg" style="color:#730053" aria-hidden="true"></i> Telefono </strong>
                                                    <span class="cont"><% out.print(doctorBean.getDoc_phone());%></span>
                                                </li>
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-envelope" aria-hidden="true" style="color:#730053" ></i> Correo </strong>
                                                    <span class="cont"><a href="<% out.print(doctorBean.getDoc_email());%>"><% out.print(doctorBean.getDoc_email());%></a></span>
                                                </li>
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-university" aria-hidden="true" style="color:#730053"></i> Consultorio </strong>
                                                    <span class="cont">
													<%
													if (doctorBean.getDoc_en_name().length() > 30) {
														out.print(doctorBean.getDoc_en_name().substring(0,30)); 
													}
													else {
														out.print(doctorBean.getDoc_en_name());
													}
													%>
													</span>
                                                </li>
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-stethoscope fa-lg" aria-hidden="true" style="color:#730053"></i> 1� Especialidad </strong>
                                                    <span class="cont">
													
													<% out.print(doctorBean.getDoc_sp_name1());%></span>
                                                </li>
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-stethoscope fa-lg" aria-hidden="true" style="color:#730053"></i> 2� Especialidad </strong>
                                                    <span class="cont"><% out.print("");%></span>
                                                </li>
                                                <li class="clearfix">
                                                    <strong class="title"><i class="fa fa-comments  fa-lg" aria-hidden="true" style="color:#730053"></i> Redes Sociales </strong>
                                                    <span class="cont"> 
                                                   <a href="<% out.print(doctorBean.getTwitter());%>"><i class="fa fa-twitter fa-2x" aria-hidden="true" style="color:#730053"></i></a>
                                                   <a href="<% out.print(doctorBean.getFacebook());%>"><i class="fa fa-facebook-official fa-2x" aria-hidden="true" style="color:#730053"></i></a>
												   </span>

                                                </li>
                                                <li class="clearfix">
                  									<div class="form-group col-md-6 col-sm-6 col-xs-6">
														<input type="submit" id="btn-submit" class="btn btn-primary btn-send-message btn-md col-md-12 col-sm-12 col-xs-12 " value="perfil">
													</div>
                                                </li>
												
                                    </ul>
                                </div>
							</div>
							</form>
						</row>
                    </div>
                </div>
                <%  }  %>
            </div>
             
            
        </section>

        
        
        
      

        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Stellar Parallax -->
        <script src="js/jquery.stellar.min.js"></script>
        <!-- Owl Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- Counters -->
        <script src="js/jquery.countTo.js"></script>
        <!-- Google Map -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
        <script src="js/google_map.js"></script>
        <!-- Main JS (Do not remove) -->
        <script src="js/main.js"></script>

    </body>
</html>

