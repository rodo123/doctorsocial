<%@page import="edoctor.com.beans.ExperienciaPerfilBean"%>
<%@page import="edoctor.com.dao.ExperienciaPerfilDAO"%>
<%@page import="edoctor.com.beans.EducacionPerfilBean"%>
<%@page import="edoctor.com.dao.EducacionPerfilDAO"%>
<%@page import="edoctor.com.beans.LogroPerfilBean"%>
<%@page import="edoctor.com.dao.LogroPerfilDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import = "java.util.LinkedList"%> 

<!DOCTYPE html>
<html lang="en" >
	
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RED MEDICA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="EDoctor.com" />
        <meta name="keywords" content="edoctor, m�dicos, doctores, html5, css3, mobile first, responsive" />
        <meta name="author" content="NyR" />

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Bootstrap CSS -->    
        <link href="css/social/bootstrap.min.css" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="css/social/bootstrap-theme.css" rel="stylesheet">
        <!--external css-->
        <!-- font icon -->
        <link href="css/social/elegant-icons-style.css" rel="stylesheet" />
        <link href="css/social/font-awesome.min.css" rel="stylesheet" />    
        <!-- full calendar css-->
        <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
        <link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
        <!-- easy pie chart-->
        <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
        <!-- owl carousel -->
        <link rel="stylesheet" href="css/social/owl.carousel.css" type="text/css">
        <link href="css/social/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <!-- Custom styles -->
        <link rel="stylesheet" href="css/fullcalendar.css">
        <link href="css/social/widgets.css" rel="stylesheet">
        <link href="css/social/style.css" rel="stylesheet">
	
        <link href="css/social/style-responsive.css" rel="stylesheet" />
        <link href="css/social/xcharts.min.css" rel=" stylesheet"> 
        <link href="css/social/jquery-ui-1.10.4.min.css" rel="stylesheet">

    </head>

	
	<body>
        <!-- Inicio de seccion de contenedores -->
        <section id="container" class="">
			<!-- Inicio de header -->
            <header class="header dark-bg">	
                <!-- Inicio de logo -->
                <a href="index.jsp" class="logo"> <img src="images/logo2.png" alt="logo" style="width: 150px; margin-top: -8px;">
                </a>
                <!-- Fin de logo -->
                <div class="nav search-row" id="top_menu">
                    <!-- Inicio de busqueda -->
                    <ul class="nav top-menu">                    
                        <li>
                            <form class="navbar-form">
                                <a href="BusquedaDoctorControlle" class="buscar" placeholder="Busqueda" type="button" >Buscar
                                </a>
                            </form>
                        </li>                    
                    </ul>
                    <!-- Fin de busqueda -->                
                </div>
				
                <div class="top-nav notification-row">                
                    <!-- Inicio de notificacion dropdown -->
                    <ul class="nav pull-right top-menu">
                        <!-- Inicio de notificaciones de tareas -->
                        <li id="task_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-home fa-lg" aria-hidden="true">
                                </i>
                                <span class="badge bg-important">1
                                </span>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Inicio
                            </span>
                            <ul class="dropdown-menu extended tasks-bar">
                                <div class="notify-arrow notify-arrow-blue">
                                </div>
                                <li>
                                    <p class="blue">You have 6 pending letter
                                    </p>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="task-info">
                                            <div class="desc">Design PSD 
                                            </div>
                                            <div class="percent">80%
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="external">
                                    <a href="#">See All Tasks
                                    </a>
                                </li>
                            </ul>
                        </li>
						
                        <li id="task_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-users fa-lg" aria-hidden="true">
                                </i>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Mi Red
                            </span>
                        </li>
						
						
                        <li id="task_notificatoin_bar" class="dropdown" >
                            
							<a href="empleos.jsp" data-toggle="dropdown"   class="dropdown-toggle" >
                                <i  class="fa fa-briefcase fa-lg" aria-hidden="true" >
							
                                </i> 
                            </a>
							<a href="empleos.jsp" style="padding-top: 0px;">
                          <span style="font-size: 12px; line-height: 16px;" href="empleos.jsp">Trabajos
                            </span></a>
                        </li>
                        <!-- task notificatoin end -->

                        <!-- inbox notificatoin start-->
                        <li id="mail_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-comments-o fa-lg" aria-hidden="true">
                                </i>
                                <span class="badge bg-important">1
                                </span>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Mensajes
                            </span>
                            <ul class="dropdown-menu extended inbox">
                                <div class="notify-arrow notify-arrow-blue">
                                </div>
                                <li>
                                    <p class="blue">You have 5 new messages
                                    </p>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img alt="avatar" src="./img/avatar-mini.jpg">
                                            </img>
                                        </span>
                                        <span class="subject">
                                        </span>
                                        <span class="from">Greg  Martin
                                        </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">See all messages
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- inbox notificatoin end -->
                        
						<!-- Inicio de notificacion de alerta -->
                        <li id="alert_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-bell-o fa-lg" aria-hidden="true">
                                </i>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Notificaciones
                            </span>
                            <ul class="dropdown-menu extended notification">
                                <div class="notify-arrow notify-arrow-blue">
                                </div>
                                <li>
                                    <p class="blue">You have 4 new notifications
                                    </p>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="label label-primary">
                                            <i class="icon_profile">
                                            </i>
                                        </span> 
                                        Friend Request
                                    </a>
                                </li>     
                            </ul>
                        </li>
                        <!-- Fin de notificacion de alerta -->
						
                        <!-- user login dropdown start-->
                        <li class="dropdown open">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="profile-ava">
                                    <img alt="" src="../img/avatar1_small.jpg">
                                </span>
                            </a>
                            <span style="font-size: 11px;">Yo</span>
                                <b class="caret"></b>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li class="eborder-top">
                                    <a href="#"><i class="icon_profile"></i> My Profile</a>
                                </li>
                                <li>
                                    <a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
								</li>
							</ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!-- Fin de notificacion dropdown -->
                </div>
            </header>      
            <!-- Fin de header -->

            <!-- Inicio de contenedor principal -->
            <div id="profile-content" class="extended">	  
                <div class="body">
                    <section>
                        <section class="wrapper col-lg-7 col-lg-offset-1">
                            <div class="row margin">
                                <!-- profile-widget -->
                                <div class="col-lg-12 ">
                                    <div class="profile-widget profile-widget-info-2">
                                        <div class="panel-body">
                                            <div class="col-lg-12 col-sm-12">        
                                                <div class="follow-ava">
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <img src="img/profile-widget-avatar.jpg" alt="" style=" width: 165px; height: 165px; margin-bottom: -80px;     border-radius: 50%;   -webkit-border-radius: 50%;    border: 5px solid rgba(255,255,255,05);    display: inline-block; ">
                                                    </img>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile-widget profile-widget-info">                 
                                        <div class="panel-body">
                                            <div class="col-lg-12 col-sm-12 follow-info" style="padding-top: 0px !important; ">
                                                <!--- EDITAR PERFIN-->
                                                <div class="pv-top-card-section__actions pv-top-card-section__actions--at-top display-flex">
                                                    					
                                                    
                                                </div>
                                                <!--- FIN EDITAR PERFIL-->
                                                <br>
                                                <br>
                                                <br>
                                                <h1> Dr. <%= session.getAttribute("fname")%> <%= session.getAttribute("lname")%>
                                                </h1>   
                                                <h2> <%= session.getAttribute("titular")%>
                                                </h2>    
                                                <h3> CMP: <%= session.getAttribute("cmp")%> / <%= session.getAttribute("universidad")%>
                                                </h3>
                                                <h3> <%= session.getAttribute("email")%>
                                                </h3>
                                                <h3>
                                                    <span>
                                                        <i class="icon_pin_alt">
                                                        </i>
                                                    </span>
                                                    Peru
                                                </h3>
                                            </div>		
                                            <div id="ember1582" class="pv-top-card-section__summary mt4 ph2 ember-view">    
                                                <p id="ember1592" class="pv-top-card-section__summary-text Sans-15px-black-55 mt5 pt5 ember-view">  
                                                <div class="truncate-multiline--truncation-target">
                                                    <span> <%= session.getAttribute("lema")%>
                                                    </span>
                                                    <span class="truncate-multiline--last-line-wrapper">
                                                        <span class="truncate-multiline--last-line"> .
                                                        </span>
                                                        <button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2655="2655"> 
                                                        </button>
                                                    </span>
                                                </div>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row margin">
                                <!-- profile-widget -->
                                <div class="col-lg-12 ">
                                    <div class=" panel_seccion">
                                        <div class="col-lg-12 col-sm-12" >
                                            <h2>Tu panel
                                            </h2>   
                                            <h3><%= session.getAttribute("speciality1")%>
                                            </h3>    
                                        </div>
										
                                        <div class="col-lg-12 col-sm-12 seccion_espace seccion_font" >
                                            <a class="pv_card_action pv_section__metric "> 
                                                <span class="pv_section__metric-count block"><%= session.getAttribute("visitas")%>
                                                </span> 
                                                <span class="pv_section__metric-text">Qui�n ah visto tu perfil
                                                </span>
                                            </a> 
                                            <a class="pv_card_action pv_section__metric "> 
                                                <span class="pv_section__metric-count block">0
                                                </span> 
                                                <span class="pv_section__metric-text">visualizaciones de publicaciones
                                                </span>
                                            </a>
                                            <a class="pv_card_action pv_section__metric "> 
                                                <span class="pv_section__metric-count block"><%= session.getAttribute("busquedas")%>
                                                </span> 
                                                <span class="pv_section__metric-text">aparici�n en b�squedas
                                                </span>
                                            </a>
                                        </div>

                                        <div class="col-lg-12 col-sm-12 seccion_espace seccion_font" >                                                           
                                            <a data-control-name="career_interests" href="/jobs/career-interests/" id="ember3371" class="pv-dashboard-section__card-action pv-dashboard-section__cta career-interests relative block p3 ember-view">    
                                                <span class="pv-dashboard-section__cta-icon absolute svg-icon-wrap">
                                                    <li-icon aria-hidden="true" type="briefcase-icon" size="large">
                                                        <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                            <g class="large-icon" style="fill: currentColor">
                                                                <path d="M21,7H17V6a3,3,0,0,0-3-3H10A3,3,0,0,0,7,6V7H3A1,1,0,0,0,2,8V19a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V8A1,1,0,0,0,21,7ZM9,6a1,1,0,0,1,1-1h4a1,1,0,0,1,1,1V7H9V6ZM20,18H4V13H20v5Zm0-6H4V9H20v3Z">
                                                                </path> 
                                                            </g>
                                                        </svg>
                                                    </li-icon>
                                                </span>  
                                                <h4 class="pv-dashboard-section__cta-title pv-dashboard-section__cta-title--no-space Sans-15px-black-85-semibold ml6">Intereses de empleo
                                                </h4>    
                                                <p class="Sans-15px-black-85 ml6 inline">Informa a los t�cnicos de selecci�n de que est�s interesado:
                                                </p>     
                                                <span class="js-pv-dashboard-section-label label-16dp  mb2">Desactivado
                                                </span>
                                                <p class="pv-dashboard-section__cta-subtitle Sans-13px-black-55 ml6">Elige que oportunidades te interesan
                                                </p>
                                            </a>
                                        </div>      
                                    </div>
                                </div>

                                <!-- Inicio de la seccion de experiencia-->
                                <div class="pv-oc"> 
                                    <span class="background-details">
                                        <section class="pv-profile-section pv-profile-section--reorder-enabled background-section artdeco-container-card">  
                                            <!---inicio experiencia--->
                                            <section class="pv-profile-section experience-section">
                                                <!---inicio header experiencia--->
                                                <header class="pv-profile-section__card-header">
                                                    <h2 class="pv-profile-section__card-heading Sans-21px-black-85">Experiencia 
                                                    </h2>
                                                    												
                                                </header>
                                                <!---fin header experiencia--->
                                                <ul class="pv-profile-section__section-info section-info pv-profile-section__section-info--has-no-more">
												
												<% List<ExperienciaPerfilBean> listaExperiencia = (ArrayList<ExperienciaPerfilBean>) session.getAttribute("listExperiencia");
												%> 
												
												<%for (ExperienciaPerfilBean a : listaExperiencia) {
												%>
												<li id="851949953" class="pv-profile-section__card-item pv-position-entity ember-view">  
														<div class="pv-entity__actions">
															<a data-control-name="edit_position" href="/in/nelsi-melgarejo-50a120102/edit/position/851949953/" id="ember3204" class="pv-profile-section__edit-action pv-profile-section__hoverable-action ember-view">      
																
															</a>
														</div>

														<a data-control-name="background_details_company" href="/search/results/index/?keywords=UNIBANCA" id="ember3205" class="ember-view">
															<div class="pv-entity__logo company-logo">
																<img class="lazy-image pv-entity__logo-img pv-entity__logo-img EntityPhoto-square-5 loaded" alt="SUNAT" src="https://media-exp2.licdn.com/mpr/mpr/shrinknp_100_100/AAIA_wDGAAAAAQAAAAAAAAycAAAAJDg5ODc2YmI3LTZlYTctNGQ1MC05Y2ZiLTI0ZjQxYzM0OGEyZQ.png">
																</img>
															</div>
															<div class="pv-entity__summary-info">
																<h3 class="Sans-17px-black-85-semibold"><%=a.getCargo()%>
																</h3>

																<h4 class="Sans-17px-black-85">
																	<span class="visually-hidden">Nombre de la empresa
																	</span>
																	<span class="pv-entity__secondary-title"><%=a.getEmpresa()%>
																	</span>
																</h4>
																<h4 class="pv-entity__date-range inline-block Sans-15px-black-70">
																	<span class="visually-hidden">Fechas de empleo
																	</span>
																	<span><%=a.getMes_inicio()%> de <%=a.getAnio_inicio()%> - <%=a.getMes_fin()%> de <%=a.getAnio_fin()%>
																	</span>
																</h4>
																<h4 class="inline-block Sans-15px-black-70">
																  <span class="visually-hidden">Duraci�n del empleo
																  </span>
																  <span class="pv-entity__bullet-item">
																  </span>
																</h4>

																<h4 class="pv-entity__location Sans-15px-black-70 block">
																	<span class="visually-hidden">Ubicaci�n
																	</span>
																	<span><%=a.getLugar()%>
																	</span>
																</h4>
															</div>
														</a>
													
														<div class="pv-entity__extra-details">
															<p class="pv-entity__description Sans-15px-black-70 mt4">
															<%=a.getDescripcion()%>
															</p>
														</div>
													</li>
												<%
												}
												%>	
                                                </ul>    
                                            </section>
                                            <!--fin experiencias-->
                                        </section>
                                    </span>
                                </div>				
                                <!-- Fin de la seccion de experiencia-->

                                <!--section education-->
                                <div class="pv-oc"> 
                                    <span class="background-details">
                                        <section class="pv-profile-section pv-profile-section--reorder-enabled background-section artdeco-container-card">  
                                            <!--inicio education-->
                                            <section id="ember3217" class="pv-profile-section education-section ember-view">
                                                <!---inicio header educacion--->
                                                <header class="pv-profile-section__card-header">
                                                    <h2 class="pv-profile-section__card-heading Sans-21px-black-85">Educaci�n
                                                    </h2>
                                                    
                                                </header>
                                                <!---fin header educacion--->
                                                <ul class="pv-profile-section__section-info section-info pv-profile-section__section-info--has-no-more">
												<% List<EducacionPerfilBean> listaEducacion = (ArrayList<EducacionPerfilBean>) session.getAttribute("listEducacion");
												%> 
												
												<%for (EducacionPerfilBean b : listaEducacion) {
												%>
												<li id="293819097" class="pv-education-entity pv-profile-section__card-item ember-view">
														<div class="pv-entity__actions">
															<a data-control-name="edit_education" href="/in/nelsi-melgarejo-50a120102/edit/education/293819097/" id="ember3225" class="pv-profile-section__edit-action pv-profile-section__hoverable-action ember-view">      
																
															</a>
														</div>

														<a data-control-name="background_details_school" href="/school/15586/?legacySchoolId=15586" id="ember3226" class="ember-view">    
															<div class="pv-entity__logo">
																<img class="lazy-image pv-entity__logo-img pv-entity__logo-img EntityPhoto-square-4 ghost-school loaded" alt="Universidad Nacional del Callao" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
																</img>
															</div>
															
															<div class="pv-entity__summary-info">
																<div class="pv-entity__degree-info">
																	<h3 class="pv-entity__school-name Sans-17px-black-85-semibold"><%=b.getUniversidad()%>
																	</h3>
																	<p class="pv-entity__secondary-title pv-entity__degree-name pv-entity__secondary-title Sans-15px-black-85">
																		<span class="visually-hidden">Nombre de la titulaci�n
																		</span>
																		<span class="pv-entity__comma-item"><%=b.getTitulacion()%>
																		</span>
																	</p>
																	<p class="pv-entity__secondary-title pv-entity__fos pv-entity__secondary-title Sans-15px-black-70">
																		<span class="visually-hidden">Disciplina acad�mica
																		</span>
																		<span class="pv-entity__comma-item"><%=b.getDisciplina()%>
																		</span>
																	</p>
																</div>
																
																<p class="pv-entity__dates Sans-15px-black-70">
																	<span class="visually-hidden">Fechas de estudios o fecha de graduaci�n prevista
																	</span>
																	<span>
																		<time><%=b.getAnio_inicio()%>
																		</time> � 
																		<time><%=b.getAnio_fin()%>
																		</time>
																	</span>
																</p>
															</div>
														</a>
													</li>
												<%
												}
												%>
                                                </ul>
                                            </section>
                                            <!--fin section education-->
                                        </section>
                                    </span>
                                </div>
                                <!--fin section education-->
                                
                                <!--section logros-->
								<div id="ember1585" class="pv-deferred-area logros ember-view">            
									<section id="ember2506" class="pv-profile-section pv-accomplishments-section artdeco-container-card ember-view">
										<header class="card-header clearfix">
											<h2 class="card-heading Sans-21px-black-85 fl">
												Logros
											</h2>
											<div id="ember2507" class="dropdown pv-accomplishments-dropdown fr closed ember-view">
												
												<div class="pv-accomplishments-dropdown__options-list">
													<ul id="ember2507-options" style="display: none;" class="dropdown-options ember-view" tabindex="-1"><!---->
													</ul>  	
												</div>

											</div>
										</header>
										
										<% List<LogroPerfilBean> listLogro = (ArrayList<LogroPerfilBean>) session.getAttribute("listLogro");
										%> 
										<section id="ember2517" class="accordion-panel pv-profile-section pv-accomplishments-block languages ember-view">
											<h3 class="pv-accomplishments-block__count Sans-34px-black-100 pr3">
												<span class="visually-hidden">
													Nelsi tiene 2 idiomas
												</span>
												<span>
													<%=listLogro.size()%>
												</span>
											</h3>

											<div class="pv-accomplishments-block__content break-words">

												<h3 class="pv-accomplishments-block__title">idiomas</h3>

												<div tabindex="-1" class="pv-accomplishments-block__list-container">
													<ul class="pv-accomplishments-block__summary-list Sans-15px-black-70 ">
														
														<% for (int i = 0; i < listLogro.size(); i++){ %>
														<li class="pv-accomplishments-block__summary-list-item"><%=listLogro.get(i).getNombre_logro()%></li>
														<% } %>
													</ul>
												</div>
											</div>
										</section>
										
									</section>
								</div>
								
                                <!-- inicio intereces-->
								<div style="" class="pv-deferred-area">  
									<div class="pv-deferred-area__content intereces">
										<section class="pv-profile-section pv-interests-section artdeco-container-card">
											<h2 class="card-heading Sans-21px-black-70-dense">Intereses
											</h2>
											<ul class="pv-profile-section__section-info section-info display-flex justify-flex-start overflow-hidden">
												<li class="pv-interest-entity pv-profile-section__card-item">
													<a class="pv-interest-entity-link block full-width">    
														<div class="pv-recent-activity-item__entity-logo company-logo">
															<img class="lazy-image pv-entity__logo-img EntityPhoto-square-4 loaded" src="https://media.licdn.com/mpr/mpr/shrinknp_100_100/p/3/005/0b0/25f/3f24429.png">
															</img>
														</div>
														<div class="pv-entity__summary-info">
															<h3 class="pv-entity__summary-title Sans-17px-black-85-semibold">
																<span class="pv-entity__summary-title-text">Universidad Peruana de Ciencias Aplicadas
																</span>
															</h3>
															<p class="pv-entity__occupation Sans-15px-black-55">
															</p>
															<p class="pv-entity__follower-count Sans-15px-black-55">75.357 seguidores
															</p>
														</div>
													</a>
												</li>
												
												<li class="pv-interest-entity pv-profile-section__card-item">
													<a class="pv-interest-entity-link block full-width">    
														<div class="pv-recent-activity-item__entity-logo company-logo">
															<img class="lazy-image pv-entity__logo-img EntityPhoto-square-4 loaded" src="https://media.licdn.com/mpr/mpr/shrinknp_100_100/p/3/005/0b0/25f/3f24429.png">
															</img>
														</div>

														<div class="pv-entity__summary-info">
															<h3 class="pv-entity__summary-title Sans-17px-black-85-semibold">
																<span class="pv-entity__summary-title-text">Universidad Peruana de Ciencias Aplicadas
																</span>
															</h3>
															<p class="pv-entity__occupation Sans-15px-black-55">
															</p>
															<p class="pv-entity__follower-count Sans-15px-black-55">92.052 seguidores
															</p>
														</div>
													</a>
												</li>
								
												<li class="pv-interest-entity pv-profile-section__card-item">
													<a class="pv-interest-entity-link block full-width">    
														<div class="pv-recent-activity-item__entity-logo company-logo">
															<img class="lazy-image pv-entity__logo-img EntityPhoto-square-4 loaded" src="https://media.licdn.com/mpr/mpr/shrinknp_100_100/AAEAAQAAAAAAAAb3AAAAJDkxMDA1MmIyLTUxMzQtNGUzOC1iOWExLWYxNjYzM2Q5YjMzNg.png">
															</img>
														</div>

														<div class="pv-entity__summary-info">
															<h3 class="pv-entity__summary-title Sans-17px-black-85-semibold">
																<span class="pv-entity__summary-title-text">INFOBOX LATINOAMERICA SAC
																</span>
															</h3>
															
															<p class="pv-entity__occupation Sans-15px-black-55">
															</p>
															<p class="pv-entity__follower-count Sans-15px-black-55">14 seguidores
															</p>
														</div>
													</a>
												</li>
											</ul>
										</section>
									</div>
								</div>
								<!-- fin intereces-->
                                
                            </div>
                        </section>
                                                    
                        <!-- inicio a�adir perfil-->		
                        <section class="wrapper col-lg-3 ">
                            
                            
                            <!-- inicio publicidad-->
                            <div class="row margin" style="padding-top: 10px !important; ">
                                <!-- profile-widget -->
                                <div class="col-lg-12 ">
                                    <div class="profile-widget profile-widget-info">
                                        <div class="panel-body">
                                            <div class="col-lg-12 col-sm-12 follow-info" >
                                                <br>
                                                <br>
                                                <br>
                                                <h1>Publicidad
                                                </h1>   
                                                <h2>Publicidad
                                                </h2>    
                                                <h3>
                                                    <span>
                                                        <i class="icon_pin_alt">
                                                        </i>
                                                    </span>
                                                    Per�
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- fin publicidad-->

                            <!-- inicio ver contactos-->
							<div style="" id="ember3468" class="pv-deferred-area ember-view">  
								<div class="pv-deferred-area__content">
									<section class="right-rail__info-container">
										<section tabindex="-1" id="ember3752" class="pv-profile-section pv-contact-info artdeco-container-card ember-view">
											<div id="ember3763" class="ember-view">
													<h2 class="pv-profile-section__card-heading Sans-16px-black-85" pv-profile-section__card-heading  Sans-17px-black-85%>
														Datos personales y de contacto
													</h2>
												<div id="ember4749" class="ember-view">
													<div tabindex="-1" class="pv-profile-section__section-info section-info">
														<section class="pv-contact-info__contact-type ci-vanity-url">
															<span class="pv-contact-info__contact-icon svg-icon-wrap">
																<li-icon aria-hidden="true" type="linkedin-icon">
																	<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon" focusable="false">
																		<g style="fill: currentColor" class="solid-icon">
																			<rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
																				<path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z">
																				</path>
																		</g>
																	</svg>
																</li-icon>
															</span>

															<header class="pv-contact-info__header Sans-17px-black-85-semibold-dense">
																Tu perfil
															</header>
															<div class="pv-contact-info__ci-container">
															  <a href="<%= session.getAttribute("facebook")%>" class="pv-contact-info__contact-link">
																<%= session.getAttribute("facebook")%>
															  </a>
															</div>
														</section>

														<section class="pv-contact-info__contact-type ci-phone">
															<span class="pv-contact-info__contact-icon svg-icon-wrap">
																<li-icon aria-hidden="true" type="phone-handset-icon">
																	<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false">
																		<g class="large-icon" style="fill: currentColor">
																			<path d="M6.8,5.26l3.11,3.1L8.35,11,13,15.65l2.64-1.56,3.1,3.11-1.58,1.4a2.31,2.31,0,0,1-1.74.52c-1.2,0-3.8-1.4-6.47-4.07S4.88,9.78,4.88,8.59a2.3,2.3,0,0,1,.52-1.73L6.8,5.26M6.76,3h0A1,1,0,0,0,6,3.3L4,5.61a4.14,4.14,0,0,0-1,3c0,1.95,1.8,5,4.61,7.8S13.47,21,15.41,21a4.14,4.14,0,0,0,3-1l2.31-2a1,1,0,0,0,.3-0.73h0a1,1,0,0,0-.3-0.73L16.5,12.3a1.05,1.05,0,0,0-.75-0.3,1,1,0,0,0-.49.14l-2,1.15L10.7,10.7l1.15-2A1,1,0,0,0,12,8.25a1.05,1.05,0,0,0-.3-0.75L7.49,3.3A1,1,0,0,0,6.76,3h0Z">
																			</path>
																		</g>
																	</svg>
																</li-icon>
															</span>

															<header class="pv-contact-info__header Sans-17px-black-85-semibold-dense">
																n�mero de tel�fono
															</header>
															<ul class="list-style-none">
																<li class="pv-contact-info__ci-container">
																	<a href="tel:940978985" class="pv-contact-info__contact-link">
																	  <%=session.getAttribute("telefono")%>
																	</a>
																</li>
															</ul>
														</section>

														<section class="pv-contact-info__contact-type ci-email">
															<span class="pv-contact-info__contact-icon svg-icon-wrap">
																<li-icon aria-hidden="true" type="envelope-icon">
																	<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false">
																		<g class="large-icon" style="fill: currentColor">
																			<path d="M21,4H3A1,1,0,0,0,2,5V19a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V5A1,1,0,0,0,21,4ZM20,6V8l-8,5.24L4,8V6H20ZM4,18V9l7.32,4.78a1.25,1.25,0,0,0,1.37,0L20,9v9H4Z">
																			</path>
																		</g>
																	</svg>
																</li-icon>
															</span>

															<header class="pv-contact-info__header Sans-17px-black-85-semibold-dense">
																correo electr�nico
															</header>
															<div class="pv-contact-info__ci-container">
																<a href="mailto:nelsibelly@gmail.com" target="_blank" rel="noopener" class="pv-contact-info__contact-link">
																	<%= session.getAttribute("email")%>
																</a>
															</div>
														</section>

													</div>
												</div>

												
											</div>
										</section>
									</section>
								</div>
							</div>
                            <!-- fin ver contactos-->
                        </section>
                        <!-- fin otros perfiles-->
                    </section> 
                    <!--main content end-->
                </div>
            </div>
			<!-- Fin de contenedor principal -->
        </section>
        <!-- Fin de seccion de contenedores -->

		

        <!-- javascripts -->
        <script src="js/social/jquery.js"></script>
        <script src="js/social/jquery-ui-1.10.4.min.js"></script>
        <script src="js/social/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/social/jquery-ui-1.9.2.custom.min.js"></script>
        <!-- bootstrap -->
        <script src="js/social/bootstrap.min.js"></script>
        <!-- nice scroll -->
        <script src="js/social/jquery.scrollTo.min.js"></script>
        <script src="js/social/jquery.nicescroll.js" type="text/javascript"></script>
        <!-- charts scripts -->
        <script src="assets/jquery-knob/js/jquery.knob.js"></script>
        <script src="js/social/jquery.sparkline.js" type="text/javascript"></script>
        <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
        <script src="js/social/owl.carousel.js" ></script>
        <!-- jQuery full calendar -->
        <<script src="js/social/fullcalendar.min.js"></script>
        <script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
        <!--script for this page only-->
        <script src="js/social/calendar-custom.js"></script>
        <script src="js/social/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script src="js/social/jquery.customSelect.min.js" ></script>
        <script src="assets/chart-master/Chart.js"></script>

        <!--custome script for all page-->
        <script src="js/social/scripts.js"></script>
        <!-- custom script for this page-->
        <script src="js/social/sparkline-chart.js"></script>
        <script src="js/social/easy-pie-chart.js"></script>
        <script src="js/social/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="js/social/jquery-jvectormap-world-mill-en.js"></script>
        <script src="js/social/xcharts.min.js"></script>
        <script src="js/social/jquery.autosize.min.js"></script>
        <script src="js/social/jquery.placeholder.min.js"></script>
        <script src="js/social/gdp-data.js"></script>  
        <script src="js/social/morris.min.js"></script>
        <script src="js/social/sparklines.js"></script>    
        <script src="js/social/charts.js"></script>
        <script src="js/social/jquery.slimscroll.min.js"></script>

        <script>

                                                                    //knob
                                                                    $(function () {
                                                                        $(".knob").knob({
                                                                            'draw': function () {
                                                                                $(this.i).val(this.cv + '%')
                                                                            }
                                                                        })
                                                                    });

                                                                    //carousel
                                                                    $(document).ready(function () {
                                                                        $("#owl-slider").owlCarousel({
                                                                            navigation: true,
                                                                            slideSpeed: 300,
                                                                            paginationSpeed: 400,
                                                                            singleItem: true

                                                                        });
                                                                    });

                                                                    //custom select box

                                                                    $(function () {
                                                                        $('select.styled').customSelect();
                                                                    });

                                                                    /* ---------- Map ---------- */
                                                                    $(function () {
                                                                        $('#map').vectorMap({
                                                                            map: 'world_mill_en',
                                                                            series: {
                                                                                regions: [{
                                                                                        values: gdpData,
                                                                                        scale: ['#000', '#000'],
                                                                                        normalizeFunction: 'polynomial'
                                                                                    }]
                                                                            },
                                                                            backgroundColor: '#eef3f7',
                                                                            onLabelShow: function (e, el, code) {
                                                                                el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                                                                            }
                                                                        });
                                                                    });

        </script>

    </body>


</html>
