
<%@page import="edoctor.com.beans.ProvinciaBean"%>
<%@page import="edoctor.com.beans.RegionBean"%>
<%@page import="edoctor.com.beans.DistritoBean"%>
<%@page import="edoctor.com.beans.DistritoBean"%>
<%@page import="edoctor.com.beans.SpecialityBean"%>
<%@page import="edoctor.com.beans.EnterpriseBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RED MEDICA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="EDoctor.com" />
        <meta name="keywords" content="m�dicos, medico, doctores, promoci�n, responsive" />
        <meta name="author" content="NyR" />


        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">

        <!-- <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'> -->

        <!-- Animate.css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="css/icomoon.css">
        <!-- Simple Line Icons -->
        <link rel="stylesheet" href="css/simple-line-icons.css">
        <link rel="stylesheet" href="css/register.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <!-- Style -->
        <link rel="stylesheet" href="css/style.css">


        <!-- Modernizr JS -->
        <script src="js/modernizr-2.6.2.min.js"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->

        <script src="http://code.jquery.com/jquery-latest.js"></script>

        <script>
            function mergeForms() {
                alert("fd");
                var forms = [];
                $.each($.makeArray(arguments), function (index, value) {
                    forms[index] = document.forms[value];
                });
                var targetForm = forms[0];
                console.log(targetForm);

                $.each(forms, function (i, f) {
                    if (i != 0) {
                        $(f).find('input, select, textarea')
                                .hide()
                                .appendTo($(targetForm));
                    }
                });
                $(targetForm).submit();
            }



            $(document).ready(function () {
                $('#region').change(function (event) {
                    var regionId = document.getElementById('region').value;

                    // Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
                    $.get('RegionControlle?region=' + regionId, {
                    }, function (responseText) {

                        document.getElementById('provincia').innerHTML = responseText;
                        document.getElementById('distrito').innerHTML = '<option value=0>SELECCIONE...</option>';
                    });
                });
            });

            $(document).ready(function () {
                $('#provincia').change(function (event) {
                    var provinciaId = document.getElementById('provincia').value;

                    // Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
                    $.get('RegionControlle?provincia=' + provinciaId, {
                    }, function (responseText) {
                        var x = document.getElementById('distrito').innerHTML = responseText;
                    });
                });
            });
        </script>
    </head>
    <body>
        <header role="banner" id="fh5co-header">
            <div class="fluid-container">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <!-- Mobile Toggle Menu Button -->
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                       <a class="navbar-brand" atastellar-background-ratio="0.5" href="index.jsp"><img src="images/logo.png" style="width: 170px;"/></a> 
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                        </ul>
                    </div>
                </nav>
            </div>
        </header>


        <section id="fh5co-register" class="fh5co-bg-color" data-section="register">
            <div id="fh5co-register-in" role="contentinfo">
                <div class="container" style="padding-left: 0px;  padding-right: 0px; ">
                    <div class="row">
                        <div class="col-md-12 to-animate" style="height: 600px;">
                            <form  id="msform" class="" method="post" action="DoctorController/DoctorInsert.do" onsubmit="return valida(this)">

                                <ul id="progressbar">
                                    <li class="active">Datos Personales</li>
                                    <li>Datos Especificos</li>
                                    <li>Datos de Profesi�n</li>
                                </ul>
                                
                            <fieldset>
                                    <h2 class="fs-title"> Crea Tu cuenta </h2>
                                    <h3 class="fs-subtitle">Ingresa tus datos Personales</h3>
                                <div class="form-group">
                                    <label for="name" class="sr-only">Nombres</label>
                                    <input id="nombre" type="name" class="form-control" name="nombre" placeholder="Nombres:*">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="sr-only">Apellidos</label>
                                    <input id="apellido" type="name"  name="apellido" class="form-control" placeholder="Apellidos:*">
                                </div>

                                <div class="form-group">
                                    <label for="name"  class="sr-only">Contrase�a</label>
                                    <input id="contrase�a" type="password" name="password" class="form-control" placeholder="Contrase�a:*">
                                </div>

                                <div class="form-group">
                                    <label for="name" class="sr-only">Contrase�a</label>
                                    <input id="recontrase�a" type="password"  class="form-control" placeholder="Confirmar Contrase�a:*">
                                </div>
                                
                                <input type="button" name="next" class="next action-button btn btn-send-message btn-md" value="Registrar Soy Medico"/>
                                        <input type="submit" id="btn-submit" class="btn btn-primary btn-send-message btn-md" value="Registrar Soy Usuario" >
                            </fieldset>
                                
                            <fieldset>
                                    <h2 class="fs-title"> Datos Especificos </h2>
                                     <h3 class="fs-subtitle">Ingresa tus especialidades</h3>
                                     <div class="form-group">
                                        <label for="name" class="sr-only">CMP</label>
                                        <input id="cmp" type="name" name="cmp" class="form-control" placeholder="CMP:*">
                                    </div>
                                    
                                    <div class="form-group">

                                                <select id = "especialidades1" class="form-control" name = "especialidad1" style="color: black" >
                                                    <option value="0">1ra Especialidad</option>
                                                    <% List<SpecialityBean> listaEspecialidades = (ArrayList<SpecialityBean>) request.getAttribute("listEspecialidades");
                                                        int i = 1;
                                                    %>
                                                    <% for (SpecialityBean sp : listaEspecialidades) {%>
                                                    <option value="<%=sp.getSp_id()%>"> <%= sp.getSp_name()%> </option>
                                                    <% i++;
                                                        }   %>
                                                </select>

                                    </div>

                                            <div class="form-group">

                                                <select id = "especialidades2" class="form-control" name = "especialidad2" style="color: black">
                                                    <option value="0">2da Especialidad</option>

                                                    <% for (SpecialityBean sp : listaEspecialidades) {%>
                                                    <option value="<%=sp.getSp_id()%>"> <%= sp.getSp_name()%> </option>
                                                    <% i++;
                                                        }   %>
                                                </select>
                                            </div>

                                    <div class="form-group">
                                        <label for="name" class="sr-only">Telefono</label>
                                        <input id="telefono" type="name" class="form-control" name="telefono"  placeholder="Telefono:">
                                    </div>

                                    <div class="form-group">
                                        <label for="name" class="sr-only">Correo</label>
                                        <input id="correo" type="name" class="form-control" name="correo"   placeholder="Correo:">
                                    </div>
                                    
                                    <input type="button" name="previous" class="previous action-button btn btn-send-message btn-md" value="Anterior"/>
                                    <input type="button" name="next" class="next action-button btn btn-send-message btn-md" value="Siguiente"/>
                            </fieldset>
                                    
                            <fieldset>
                                <h2 class="fs-title"> Datos Profesionales </h2>
                                <h3 class="fs-subtitle">Ingresa tu lugar de trabajo</h3>
                                
                                

                                            <% List<EnterpriseBean> listaEnterprise = (ArrayList<EnterpriseBean>) request.getAttribute("listEnterprise");
                                                int cantidad = 0;
                                            %>


                                            <div class="form-group">
                                                <select id = "trabajo1" class="form-control" name = "trabajo1" style="color: black">
                                                    <option value="0">Lugar de Trabajo</option>
                                                    <% 
                                                    for (EnterpriseBean ep : listaEnterprise) {
                                                    %>
                                                    <option value="<%=ep.getEn_id()%>"> <%=ep.getEn_name()%></option>
                                                    <% cantidad = ep.getEn_id(); }%>
                                                    <option value="<%=cantidad+1%>">OTRO</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="sr-only">Trabajo</label>
                                                <input id="trabajo2" type="name" class="form-control"  name="trabajo2" placeholder="Lugar de Trabajo:*">
                                            </div>




                                            <div class="form-group">

                                                <select id = "region" class="form-control" name = "region" style="color: black" >

                                                    <% String re_id = request.getParameter("region");
                                                        List<RegionBean> listaRegion = (ArrayList<RegionBean>) request.getAttribute("listRegion"); %>
                                                    <option value="0">Regi�n</option>
                                                    <% for (RegionBean re : listaRegion) {
                                                            if (re_id != null && Integer.parseInt(re_id) == re.getRe_id()) {%>
                                                    <option value="<%=re.getRe_id()%>" selected="selected"> <%=re.getRe_name()%></option>
                                                    <% } else {%>
                                                    <option value="<%=re.getRe_id()%>"> <%=re.getRe_name()%></option>

                                                    <%}%>
                                                    <%  }   %>
                                                </select>
                                             </div>
                                            <div class="form-group">

                                                <select id = "provincia"  class="form-control" name = "provincia" style="color: black">
                                                    <%
                                                        String pr_id = request.getParameter("provincia");
                                                        List<ProvinciaBean> listaProvincia = (ArrayList<ProvinciaBean>) request.getAttribute("listProvincia"); %>
                                                    <option value="0">Provincia</option>
                                                    <% for (ProvinciaBean pr : listaProvincia) {
                                                            if (pr_id != null && Integer.parseInt(pr_id) == pr.getPr_id()) {%>
                                                    <option value="<%= pr.getPr_id()%>" selected="selected"> <%=pr.getPr_name()%></option>
                                                    <% } else {%>
                                                    <option value="<%= pr.getPr_id()%>" > <%=pr.getPr_name()%></option>
                                                    <%}%>
                                                    <% }   %>
                                                </select>
                                             </div>   
                                            <div class="form-group">
                                                <select id = "distrito" class="form-control" name = "distrito" style="color: black">
                                                    <%
                                                        System.out.println(pr_id);
                                                        List<DistritoBean> listaDistrito = (ArrayList<DistritoBean>) request.getAttribute("listDistrito"); %>
                                                    <option value="0">Distrito</option>
                                                    <% for (DistritoBean di : listaDistrito) {%>
                                                    <option value="<%=di.getDi_id()%>"> <%=di.getDi_name()%></option>
                                                    <%}%>
                                                </select>
                                            </div>
                                        <input type="button" name="previous" class="previous action-button btn btn-send-message btn-md" value="Anterior"/>
                                        <input type="submit" id="btn-submit" class="btn btn-primary btn-send-message btn-md" value="REGISTRAR" >
                                    </fieldset>                    
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <script type="text/javascript">
            function valida(f) {
                var ok = true;
                var msg = "Debes escribir algo en los campos:\n";
                if (f.elements["nombre"].value == "")
                {
                    msg += "- Nombres \n";
                    ok = false;
                }

                if (f.elements["apellido"].value == "")
                {
                    msg += "- Apellidos \n";
                    ok = false;
                }

                if (f.elements["contrase�a"].value == "")
                {
                    msg += "- Contrase�a \n";
                    ok = false;
                }

                if (f.elements["contrase�a"].value != f.elements["recontrase�a"].value)
                {
                    msg += "- Las contrase�as no coinciden \n";
                    ok = false;
                }

                if (f.elements["cmp"].value == "")
                {
                    msg += "- CMP \n";
                    ok = false;
                }
                
                if (f.elements["correo"].value == "")
                {
                    msg += "- Correo \n";
                    ok = false;
                }

                if (f.elements["especialidad1"].value == "0" && f.elements["especialidad2"].value == "0")
                {
                    msg += "- Especialidades \n";
                    ok = false;
                } else if (f.elements["especialidad1"].value == "0")
                {
                    msg += "- Especialidad1 \n";
                    ok = false;
                }
                console.log(f.elements["trabajo1"].value)
                if (f.elements["trabajo1"].value == "0" )
                {
                    msg += "- Trabajo \n";
                    ok = false;
                }

                if (f.elements["especialidad1"].value != "0" || f.elements["especialidad2"].value != "0") {
                    if (f.elements["region"].value == "0" || f.elements["provincia"].value == "0" || f.elements["distrito"].value == "0") {
                        msg += "- Ubigeo \n";
                        ok = false;
                    }
                }

                if (ok == false)
                    alert(msg);
                return ok;
            }
        </script>

        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Stellar Parallax -->
        <script src="js/jquery.stellar.min.js"></script>
        <!-- Owl Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- Counters -->
        <script src="js/jquery.countTo.js"></script>
        <!-- Google Map -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
        <script src="js/google_map.js"></script>
        <!-- Main JS (Do not remove) -->
        <script src="js/main.js"></script>
        <script src="js/register.js"></script>

    </body>
</html>

