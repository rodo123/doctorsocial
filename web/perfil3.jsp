<%@page import="edoctor.com.beans.ExperienciaPerfilBean"%>
<%@page import="edoctor.com.dao.ExperienciaPerfilDAO"%>
<%@page import="edoctor.com.beans.EducacionPerfilBean"%>
<%@page import="edoctor.com.dao.EducacionPerfilDAO"%>
<%@page import="edoctor.com.beans.LogroPerfilBean"%>
<%@page import="edoctor.com.dao.LogroPerfilDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import = "java.util.LinkedList"%> 

<!DOCTYPE html>
<html lang="en" >
	
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RED MEDICA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="EDoctor.com" />
        <meta name="keywords" content="edoctor, m�dicos, doctores, html5, css3, mobile first, responsive" />
        <meta name="author" content="NyR" />

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Bootstrap CSS -->    
        <link href="css/social/bootstrap.min.css" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="css/social/bootstrap-theme.css" rel="stylesheet">
        <!--external css-->
        <!-- font icon -->
        <link href="css/social/elegant-icons-style.css" rel="stylesheet" />
        <link href="css/social/font-awesome.min.css" rel="stylesheet" />    
        <!-- full calendar css-->
        <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
        <link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
        <!-- easy pie chart-->
        <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
        <!-- owl carousel -->
        <link rel="stylesheet" href="css/social/owl.carousel.css" type="text/css">
        <link href="css/social/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <!-- Custom styles -->
        <link rel="stylesheet" href="css/fullcalendar.css">
        <link href="css/social/widgets.css" rel="stylesheet">
        <link href="css/social/style.css" rel="stylesheet">
	
        <link href="css/social/style-responsive.css" rel="stylesheet" />
        <link href="css/social/xcharts.min.css" rel=" stylesheet"> 
        <link href="css/social/jquery-ui-1.10.4.min.css" rel="stylesheet">

    </head>

	
	<body>
        <!-- Inicio de seccion de contenedores -->
        <section id="container" class="">
			<!-- Inicio de header -->
            <header class="header dark-bg">	
                <!-- Inicio de logo -->
                <a href="index.jsp" class="logo"> <img src="images/logo2.png" alt="logo" style="width: 150px; margin-top: -8px;">
                </a>
                <!-- Fin de logo -->
                <div class="nav search-row" id="top_menu">
                    <!-- Inicio de busqueda -->
                    <ul class="nav top-menu">                    
                        <li>
                            <form class="navbar-form">
                                <a href="BusquedaDoctorControlle" class="buscar" placeholder="Busqueda" type="button" >Buscar
                                </a>
                            </form>
                        </li>                    
                    </ul>
                    <!-- Fin de busqueda -->                
                </div>
				
                <div class="top-nav notification-row">                
                    <!-- Inicio de notificacion dropdown -->
                    <ul class="nav pull-right top-menu">
                        <!-- Inicio de notificaciones de tareas -->
                        <li id="task_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-home fa-lg" aria-hidden="true">
                                </i>
                                <span class="badge bg-important">1
                                </span>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Inicio
                            </span>
                            <ul class="dropdown-menu extended tasks-bar">
                                <div class="notify-arrow notify-arrow-blue">
                                </div>
                                <li>
                                    <p class="blue">You have 6 pending letter
                                    </p>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="task-info">
                                            <div class="desc">Design PSD 
                                            </div>
                                            <div class="percent">80%
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="external">
                                    <a href="#">See All Tasks
                                    </a>
                                </li>
                            </ul>
                        </li>
						
                        <li id="task_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-users fa-lg" aria-hidden="true">
                                </i>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Mi Red
                            </span>
                        </li>
						
						
                        <li id="task_notificatoin_bar" class="dropdown" >
                            
							<a href="empleos.jsp" data-toggle="dropdown"   class="dropdown-toggle" >
                                <i  class="fa fa-briefcase fa-lg" aria-hidden="true" >
							
                                </i> 
                            </a>
							<a href="BusquedaEmpleosController" style="padding-top: 0px;">
                          <span style="font-size: 12px; line-height: 16px;" href="empleos.jsp">Trabajos
                            </span></a>
                        </li>
                        <!-- task notificatoin end -->

                        <!-- inbox notificatoin start-->
                        <li id="mail_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-comments-o fa-lg" aria-hidden="true">
                                </i>
                                <span class="badge bg-important">1
                                </span>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Mensajes
                            </span>
                            <ul class="dropdown-menu extended inbox">
                                <div class="notify-arrow notify-arrow-blue">
                                </div>
                                <li>
                                    <p class="blue">You have 5 new messages
                                    </p>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img alt="avatar" src="./img/avatar-mini.jpg">
                                            </img>
                                        </span>
                                        <span class="subject">
                                        </span>
                                        <span class="from">Greg  Martin
                                        </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">See all messages
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- inbox notificatoin end -->
                        
						<!-- Inicio de notificacion de alerta -->
                        <li id="alert_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="fa fa-bell-o fa-lg" aria-hidden="true">
                                </i>
                            </a>
                            <span style="font-size: 12px; line-height: 16px;">Notificaciones
                            </span>
                            <ul class="dropdown-menu extended notification">
                                <div class="notify-arrow notify-arrow-blue">
                                </div>
                                <li>
                                    <p class="blue">You have 4 new notifications
                                    </p>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="label label-primary">
                                            <i class="icon_profile">
                                            </i>
                                        </span> 
                                        Friend Request
                                    </a>
                                </li>     
                            </ul>
                        </li>
                        <!-- Fin de notificacion de alerta -->
						
                        <!-- user login dropdown start-->
                        <li class="dropdown open">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="profile-ava">
                                    <img alt="" src="../img/avatar1_small.jpg">
                                </span>
                            </a>
                            <span style="font-size: 11px;">Yo</span>
                                <b class="caret"></b>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li class="eborder-top">
                                    <a href="#"><i class="icon_profile"></i> My Profile</a>
                                </li>
                                <li>
                                    <a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
								</li>
							</ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!-- Fin de notificacion dropdown -->
                </div>
            </header>      
            <!-- Fin de header -->

            <!-- Inicio de contenedor principal -->
            <div id="profile-content" class="extended">	  
                <div class="body">
                    <section>
                        <section class="wrapper col-lg-7 col-lg-offset-1">
                            <div class="row margin">
                                <!-- profile-widget -->
                                <div class="col-lg-12 ">
                                    <div class="profile-widget profile-widget-info-2">
                                        <div class="panel-body">
                                            <div class="col-lg-12 col-sm-12">        
                                                <div class="follow-ava">
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <img src="<%= session.getAttribute("url_imagen_personal")%>" alt="" style=" width: 165px; height: 165px; margin-bottom: -80px;     border-radius: 50%;   -webkit-border-radius: 50%;    border: 5px solid rgba(255,255,255,05);    display: inline-block; ">
                                                    </img>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile-widget profile-widget-info">                 
                                        <div class="panel-body">
                                            <div class="col-lg-12 col-sm-12 follow-info" style="padding-top: 0px !important; ">
                                                <!--- EDITAR PERFIN-->
                                                <div class="pv-top-card-section__actions pv-top-card-section__actions--at-top display-flex">
                                                    					
                                                    <button id="boton1" onclick="document.getElementById('ember1').style.display = 'block'">
                                                        <a data-control-name="edit_top_card" id="ember3044" class="pv-top-card-section__edit button-tertiary-medium-round ember-view">    
                                                            <span class="svg-icon-wrap" >
                                                                <span class="visually-hidden">Editar perfil
                                                                </span>
                                                                <li-icon aria-hidden="true" type="pencil-icon">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                                        <g class="large-icon" style="fill: currentColor">
                                                                            <path d="M21.71,5L19,2.29a1,1,0,0,0-1.41,0L4,15.85,2,22l6.15-2L21.71,6.45A1,1,0,0,0,22,5.71,1,1,0,0,0,21.71,5ZM6.87,18.64l-1.5-1.5L15.92,6.57l1.5,1.5ZM18.09,7.41l-1.5-1.5,1.67-1.67,1.5,1.5Z">
                                                                            </path>
                                                                        </g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                        </a>
                                                    </button>
                                                </div>
                                                <!--- FIN EDITAR PERFIL-->
                                                <br>
                                                <br>
                                                <br>
                                                <h1> Dr. <%= session.getAttribute("fname")%> <%= session.getAttribute("lname")%>
                                                </h1>   
                                                <h2> <%= session.getAttribute("titular")%>
                                                </h2>    
                                                <h3> CMP: <%= session.getAttribute("cmp")%> / <%= session.getAttribute("universidad")%>
                                                </h3>
                                                <h3> <%= session.getAttribute("email")%>
                                                </h3>
                                                <h3>
                                                    <span>
                                                        <i class="icon_pin_alt">
                                                        </i>
                                                    </span>
                                                    Peru
                                                </h3>
                                            </div>		
                                            <div id="ember1582" class="pv-top-card-section__summary mt4 ph2 ember-view">    
                                                <p id="ember1592" class="pv-top-card-section__summary-text Sans-15px-black-55 mt5 pt5 ember-view">  
                                                <div class="truncate-multiline--truncation-target">
                                                    <span> <%= session.getAttribute("lema")%>
                                                    </span>
                                                    <span class="truncate-multiline--last-line-wrapper">
                                                        <span class="truncate-multiline--last-line"> .
                                                        </span>
                                                        <button class="truncate-multiline--button-hidden" data-ember-action="" data-ember-action-2655="2655"> 
                                                        </button>
                                                    </span>
                                                </div>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row margin">
                                <!-- profile-widget -->
                                <div class="col-lg-12 ">
                                    <div class=" panel_seccion">
                                        <div class="col-lg-12 col-sm-12" >
                                            <h2>Tu panel
                                            </h2>   
                                            <h3><%= session.getAttribute("speciality1")%>
                                            </h3>    
                                        </div>
										
                                        <div class="col-lg-12 col-sm-12 seccion_espace seccion_font" >
                                            <a class="pv_card_action pv_section__metric "> 
                                                <span class="pv_section__metric-count block"><%= session.getAttribute("visitas")%>
                                                </span> 
                                                <span class="pv_section__metric-text">Cuantos  visitaron tu perfil
                                                </span>
                                            </a> 
                                            <a class="pv_card_action pv_section__metric "> 
                                                <span class="pv_section__metric-count block">0
                                                </span> 
                                                <span class="pv_section__metric-text">visualizaciones de publicaciones
                                                </span>
                                            </a>
                                            <a class="pv_card_action pv_section__metric "> 
                                                <span class="pv_section__metric-count block"><%= session.getAttribute("busquedas")%>
                                                </span> 
                                                <span class="pv_section__metric-text">aparici�n en b�squedas
                                                </span>
                                            </a>
                                        </div>

                                        <div class="col-lg-12 col-sm-12 seccion_espace seccion_font" >                                                           
                                            <a data-control-name="career_interests" href="empleos.jsp" id="ember3371" class="pv-dashboard-section__card-action pv-dashboard-section__cta career-interests relative block p3 ember-view">    
                                                <span class="pv-dashboard-section__cta-icon absolute svg-icon-wrap">
                                                    <li-icon aria-hidden="true" type="briefcase-icon" size="large">
                                                        <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                            <g class="large-icon" style="fill: currentColor">
                                                                <path d="M21,7H17V6a3,3,0,0,0-3-3H10A3,3,0,0,0,7,6V7H3A1,1,0,0,0,2,8V19a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V8A1,1,0,0,0,21,7ZM9,6a1,1,0,0,1,1-1h4a1,1,0,0,1,1,1V7H9V6ZM20,18H4V13H20v5Zm0-6H4V9H20v3Z">
                                                                </path> 
                                                            </g>
                                                        </svg>
                                                    </li-icon>
                                                </span>  
                                                <h4 class="pv-dashboard-section__cta-title pv-dashboard-section__cta-title--no-space Sans-15px-black-85-semibold ml6">Intereses de empleo
                                                </h4>    
                                                <p class="Sans-15px-black-85 ml6 inline">Informa a los t�cnicos de selecci�n de que est�s interesado:
                                                </p>     
                                                <span class="js-pv-dashboard-section-label label-16dp  mb2">Desactivado
                                                </span>
                                                <p class="pv-dashboard-section__cta-subtitle Sans-13px-black-55 ml6">Elige que oportunidades te interesan
                                                </p>
                                            </a>
                                        </div>      
                                    </div>
                                </div>

                                <!-- Inicio de la seccion de experiencia-->
                                <div class="pv-oc"> 
                                    <span class="background-details">
                                        <section class="pv-profile-section pv-profile-section--reorder-enabled background-section artdeco-container-card">  
                                            <!---inicio experiencia--->
                                            <section class="pv-profile-section experience-section">
                                                <!---inicio header experiencia--->
                                                <header class="pv-profile-section__card-header">
                                                    <h2 class="pv-profile-section__card-heading Sans-21px-black-85">Experiencia 
                                                    </h2>
                                                    <button onclick="document.getElementById('ember2').style.display = 'block'" >
                                                        <a data-control-name="add_position"  id="ember2825" class="pv-profile-section__header-add-action add-position ember-view" style="margin-right: 0; ">      
                                                            <span class="svg-icon-wrap">
                                                                <span class="visually-hidden" >A�adir nuevo puesto
                                                                </span>
                                                                <li-icon aria-hidden="true" type="plus-icon">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                                        <g class="large-icon" style="fill: currentColor">
                                                                            <path d="M21,13H13v8H11V13H3V11h8V3h2v8h8v2Z">
                                                                            </path>
                                                                        </g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                        </a>
                                                    </button>													
                                                </header>
                                                <!---fin header experiencia--->
                                                <ul class="pv-profile-section__section-info section-info pv-profile-section__section-info--has-no-more">
												
												<% List<ExperienciaPerfilBean> listaExperiencia = (ArrayList<ExperienciaPerfilBean>) session.getAttribute("listExperiencia");
												%> 
												
												<% int parse=0;
												for (ExperienciaPerfilBean a : listaExperiencia) {
												%>
												<li id="851949953" class="pv-profile-section__card-item pv-position-entity ember-view">  
														<div class="pv-entity__actions">
															<button onclick="document.getElementById('ember6').style.display = 'block'" class="pv-profile-section__edit-action pv-profile-section__hoverable-action ember-view">
																<input id="car" name="car" value="<%=parse%>" type="hidden"></input>
																<a data-control-name="edit_position" id="ember3204" class="pv-profile-section__edit-action pv-profile-section__hoverable-action ember-view">      
																<span class="svg-icon-wrap">
																	<span class="visually-hidden">Editar puesto: Practicante pre-profesional
																	</span>
																	<li-icon aria-hidden="true" type="pencil-icon">
																		<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
																			<g class="large-icon" style="fill: currentColor">
																				<path d="M21.71,5L19,2.29a1,1,0,0,0-1.41,0L4,15.85,2,22l6.15-2L21.71,6.45A1,1,0,0,0,22,5.71,1,1,0,0,0,21.71,5ZM6.87,18.64l-1.5-1.5L15.92,6.57l1.5,1.5ZM18.09,7.41l-1.5-1.5,1.67-1.67,1.5,1.5Z">
																				</path>
																			</g>
																		</svg>
																	</li-icon>
																</span>
																</a>
															</button>
														</div>

														<a data-control-name="background_details_company" href="/search/results/index/?keywords=UNIBANCA" id="ember3205" class="ember-view">
															<div class="pv-entity__logo company-logo">
																<img class="lazy-image pv-entity__logo-img pv-entity__logo-img EntityPhoto-square-5 loaded" alt="" src="">
																</img>
															</div>
															<div class="pv-entity__summary-info">
																<h3 class="Sans-17px-black-85-semibold"><%=a.getCargo()%>
																</h3>

																<h4 class="Sans-17px-black-85">
																	<span class="visually-hidden">Nombre de la empresa
																	</span>
																	<span class="pv-entity__secondary-title"><%=a.getEmpresa()%>
																	</span>
																</h4>
																<h4 class="pv-entity__date-range inline-block Sans-15px-black-70">
																	<span class="visually-hidden">Fechas de empleo
																	</span>
																	<span><%=a.getMes_inicio()%> de <%=a.getAnio_inicio()%> - <%=a.getMes_fin()%> de <%=a.getAnio_fin()%>
																	</span>
																</h4>
																<h4 class="inline-block Sans-15px-black-70">
																  <span class="visually-hidden">Duraci�n del empleo
																  </span>
																  <span class="pv-entity__bullet-item">
																  </span>
																</h4>

																<h4 class="pv-entity__location Sans-15px-black-70 block">
																	<span class="visually-hidden">Ubicaci�n
																	</span>
																	<span><%=a.getLugar()%>
																	</span>
																</h4>
															</div>
														</a>
													
														<div class="pv-entity__extra-details">
															<p class="pv-entity__description Sans-15px-black-70 mt4">
															<%=a.getDescripcion()%>
															</p>
														</div>
													</li>
												<%
												}
												%>	
                                                </ul>    
                                            </section>
                                            <!--fin experiencias-->
                                        </section>
                                    </span>
                                </div>				
                                <!-- Fin de la seccion de experiencia-->

                                <!--section education-->
                                <div class="pv-oc"> 
                                    <span class="background-details">
                                        <section class="pv-profile-section pv-profile-section--reorder-enabled background-section artdeco-container-card">  
                                            <!--inicio education-->
                                            <section id="ember3217" class="pv-profile-section education-section ember-view">
                                                <!---inicio header educacion--->
                                                <header class="pv-profile-section__card-header">
                                                    <h2 class="pv-profile-section__card-heading Sans-21px-black-85">Educaci�n
                                                    </h2>
                                                    <button onclick="document.getElementById('ember3').style.display = 'block'">
                                                        <a data-control-name="add_education"  id="ember3218" class="pv-profile-section__header-add-action add-education ember-view">
                                                            <span class="svg-icon-wrap">
                                                                <span class="visually-hidden">A�adir nueva educaci�n
                                                                </span>
                                                                <li-icon aria-hidden="true" type="plus-icon">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                                        <g class="large-icon" style="fill: currentColor">
                                                                            <path d="M21,13H13v8H11V13H3V11h8V3h2v8h8v2Z">
                                                                            </path>
                                                                        </g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                        </a>
                                                    </button>
                                                </header>
                                                <!---fin header educacion--->
                                                <ul class="pv-profile-section__section-info section-info pv-profile-section__section-info--has-no-more">
												<% List<EducacionPerfilBean> listaEducacion = (ArrayList<EducacionPerfilBean>) session.getAttribute("listEducacion");
												%> 
												
												<%for (EducacionPerfilBean b : listaEducacion) {
												%>
												<li id="293819097" class="pv-education-entity pv-profile-section__card-item ember-view">
														<div class="pv-entity__actions">
															<button onclick="document.getElementById('ember7').style.display = 'block'" class="pv-profile-section__edit-action pv-profile-section__hoverable-action ember-view">
																<a data-control-name="edit_education" id="ember3225" class="pv-profile-section__edit-action pv-profile-section__hoverable-action ember-view">      
																<span class="svg-icon-wrap">
																	<span class="visually-hidden">Editar educaci�n: Universidad Nacional del Callao
																	</span>
																	<li-icon aria-hidden="true" type="pencil-icon">
																		<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
																			<g class="large-icon" style="fill: currentColor">
																				<path d="M21.71,5L19,2.29a1,1,0,0,0-1.41,0L4,15.85,2,22l6.15-2L21.71,6.45A1,1,0,0,0,22,5.71,1,1,0,0,0,21.71,5ZM6.87,18.64l-1.5-1.5L15.92,6.57l1.5,1.5ZM18.09,7.41l-1.5-1.5,1.67-1.67,1.5,1.5Z">
																				</path>
																			</g>
																		</svg>
																	</li-icon>
																</span>
															</a>
															</button>
															
														</div>

														<a data-control-name="background_details_school" href="/school/15586/?legacySchoolId=15586" id="ember3226" class="ember-view">    
															<div class="pv-entity__logo">
																<img class="lazy-image pv-entity__logo-img pv-entity__logo-img EntityPhoto-square-4 ghost-school loaded" alt="Universidad Nacional del Callao" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
																</img>
															</div>
															
															<div class="pv-entity__summary-info">
																<div class="pv-entity__degree-info">
																	<h3 class="pv-entity__school-name Sans-17px-black-85-semibold"><%=b.getUniversidad()%>
																	</h3>
																	<p class="pv-entity__secondary-title pv-entity__degree-name pv-entity__secondary-title Sans-15px-black-85">
																		<span class="visually-hidden">Nombre de la titulaci�n
																		</span>
																		<span class="pv-entity__comma-item"><%=b.getTitulacion()%>
																		</span>
																	</p>
																	<p class="pv-entity__secondary-title pv-entity__fos pv-entity__secondary-title Sans-15px-black-70">
																		<span class="visually-hidden">Disciplina acad�mica
																		</span>
																		<span class="pv-entity__comma-item"><%=b.getDisciplina()%>
																		</span>
																	</p>
																</div>
																
																<p class="pv-entity__dates Sans-15px-black-70">
																	<span class="visually-hidden">Fechas de estudios o fecha de graduaci�n prevista
																	</span>
																	<span>
																		<time><%=b.getAnio_inicio()%>
																		</time> � 
																		<time><%=b.getAnio_fin()%>
																		</time>
																	</span>
																</p>
															</div>
														</a>
													</li>
												<%
												}
												%>
                                                </ul>
                                            </section>
                                            <!--fin section education-->
                                        </section>
                                    </span>
                                </div>
                                <!--fin section education-->
                                
                                <!--section logros-->
								<div id="ember1585" class="pv-deferred-area logros ember-view">            
									<section id="ember2506" class="pv-profile-section pv-accomplishments-section artdeco-container-card ember-view">
										<header class="card-header clearfix">
											<h2 class="card-heading Sans-21px-black-85 fl">
												Logros
											</h2>
											<div id="ember2507" class="dropdown pv-accomplishments-dropdown fr closed ember-view">
												<button type="button" onclick="document.getElementById('ember4').style.display='block'">
													<a aria-controls="ember2507-options" aria-expanded="false" data-control-name="add_accomplishment" id="ember2507-trigger" class="dropdown-trigger button-tertiary-medium-round add-action ember-view">
													<span class="svg-icon-wrap">
														<span class="visually-hidden">
															A�adir logro
														</span>
														<li-icon aria-hidden="true" type="plus-icon" size="large">
															<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
																<g class="large-icon" style="fill: currentColor">
																	<path d="M21,13H13v8H11V13H3V11h8V3h2v8h8v2Z">
																	</path>
																</g>
															</svg>
														</li-icon>
													</span>
													</a>
												</button>
												<div class="pv-accomplishments-dropdown__options-list">
													<ul id="ember2507-options" style="display: none;" class="dropdown-options ember-view" tabindex="-1"><!---->
													</ul>  	
												</div>

											</div>
										</header>
										
										<% List<LogroPerfilBean> listLogro = (ArrayList<LogroPerfilBean>) session.getAttribute("listLogro");
										   if (listLogro.size()>0) {
										%> 
										<section id="ember2517" class="accordion-panel pv-profile-section pv-accomplishments-block languages ember-view">
											<h3 class="pv-accomplishments-block__count Sans-34px-black-100 pr3">
												<span class="visually-hidden">
												</span>
												<span>
													<%=listLogro.size()%>
												</span>
											</h3>

											<div class="pv-accomplishments-block__content break-words">
												<button onclick="document.getElementById('ember4').style.display='block'" data-control-name="accomplishments_expand_languages" class="pv-accomplishments-block__expand" data-ember-action="" data-ember-action-2518="2518"><span class="svg-icon-wrap"><span class="visually-hidden">Ampliar la secci�n de idiomas</span><li-icon aria-hidden="true" type="chevron-down-icon" size="medium"><svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon"><g class="large-icon" style="fill: currentColor">
													<path d="M12,14l8.94-6L22,9.55l-9.15,6.19a1.5,1.5,0,0,1-1.69,0L2,9.55,3.06,8Z"></path>
													</g></svg></li-icon></span>
												</button>

												<h3 class="pv-accomplishments-block__title">idiomas</h3>

												<div tabindex="-1" class="pv-accomplishments-block__list-container">
													<ul class="pv-accomplishments-block__summary-list Sans-15px-black-70 ">
														
														<% for (int i = 0; i < listLogro.size(); i++){ %>
														<li class="pv-accomplishments-block__summary-list-item"><%=listLogro.get(i).getNombre_logro()%></li>
														<% } %>
													</ul>
												</div>
											</div>
										</section>
										<%} %>
									</section>
								</div>
								
                                <!-- inicio intereces
								<div style="" class="pv-deferred-area">  
									<div class="pv-deferred-area__content intereces">
										<section class="pv-profile-section pv-interests-section artdeco-container-card">
											<h2 class="card-heading Sans-21px-black-70-dense">Intereses
											</h2>
											<ul class="pv-profile-section__section-info section-info display-flex justify-flex-start overflow-hidden">
												<li class="pv-interest-entity pv-profile-section__card-item">
													<a class="pv-interest-entity-link block full-width">    
														<div class="pv-recent-activity-item__entity-logo company-logo">
															<img class="lazy-image pv-entity__logo-img EntityPhoto-square-4 loaded" src="https://media.licdn.com/mpr/mpr/shrinknp_100_100/p/3/005/0b0/25f/3f24429.png">
															</img>
														</div>
														<div class="pv-entity__summary-info">
															<h3 class="pv-entity__summary-title Sans-17px-black-85-semibold">
																<span class="pv-entity__summary-title-text">Universidad Peruana de Ciencias Aplicadas
																</span>
															</h3>
															<p class="pv-entity__occupation Sans-15px-black-55">
															</p>
															<p class="pv-entity__follower-count Sans-15px-black-55">75.357 seguidores
															</p>
														</div>
													</a>
												</li>
												
												<li class="pv-interest-entity pv-profile-section__card-item">
													<a class="pv-interest-entity-link block full-width">    
														<div class="pv-recent-activity-item__entity-logo company-logo">
															<img class="lazy-image pv-entity__logo-img EntityPhoto-square-4 loaded" src="https://media.licdn.com/mpr/mpr/shrinknp_100_100/p/3/005/0b0/25f/3f24429.png">
															</img>
														</div>

														<div class="pv-entity__summary-info">
															<h3 class="pv-entity__summary-title Sans-17px-black-85-semibold">
																<span class="pv-entity__summary-title-text">Universidad Peruana de Ciencias Aplicadas
																</span>
															</h3>
															<p class="pv-entity__occupation Sans-15px-black-55">
															</p>
															<p class="pv-entity__follower-count Sans-15px-black-55">92.052 seguidores
															</p>
														</div>
													</a>
												</li>
								
												<li class="pv-interest-entity pv-profile-section__card-item">
													<a class="pv-interest-entity-link block full-width">    
														<div class="pv-recent-activity-item__entity-logo company-logo">
															<img class="lazy-image pv-entity__logo-img EntityPhoto-square-4 loaded" src="https://media.licdn.com/mpr/mpr/shrinknp_100_100/AAEAAQAAAAAAAAb3AAAAJDkxMDA1MmIyLTUxMzQtNGUzOC1iOWExLWYxNjYzM2Q5YjMzNg.png">
															</img>
														</div>

														<div class="pv-entity__summary-info">
															<h3 class="pv-entity__summary-title Sans-17px-black-85-semibold">
																<span class="pv-entity__summary-title-text">INFOBOX LATINOAMERICA SAC
																</span>
															</h3>
															
															<p class="pv-entity__occupation Sans-15px-black-55">
															</p>
															<p class="pv-entity__follower-count Sans-15px-black-55">14 seguidores
															</p>
														</div>
													</a>
												</li>
											</ul>
										</section>
									</div>
								</div>
								fin intereces-->
                                
                            </div>
                        </section>
                                                    
                        <!-- inicio a�adir perfil-->		
                        <section class="wrapper col-lg-3 ">
                            
							<!-- inicio ver contactos-->
							<div style="" id="ember3468" class="pv-deferred-area ember-view">  
								<div class="pv-deferred-area__content">
									<section class="right-rail__info-container">
										<section tabindex="-1" id="ember3752" class="pv-profile-section pv-contact-info artdeco-container-card ember-view">
											<div id="ember3763" class="ember-view">
												<button id="boton1" onclick="document.getElementById('ember5').style.display = 'block'" style="float: right;">
                                                        <a data-control-name="edit_top_card" id="ember3764" class="pv-top-card-section__edit button-tertiary-medium-round ember-view">    
                                                            <span class="svg-icon-wrap" >
                                                                <span class="visually-hidden">
																Editar perfil
                                                                </span>
                                                                <li-icon aria-hidden="true" type="pencil-icon">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                                        <g class="large-icon" style="fill: currentColor">
                                                                            <path d="M21.71,5L19,2.29a1,1,0,0,0-1.41,0L4,15.85,2,22l6.15-2L21.71,6.45A1,1,0,0,0,22,5.71,1,1,0,0,0,21.71,5ZM6.87,18.64l-1.5-1.5L15.92,6.57l1.5,1.5ZM18.09,7.41l-1.5-1.5,1.67-1.67,1.5,1.5Z">
                                                                            </path>
                                                                        </g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                        </a>
                                                    </button>
													<h2 class="pv-profile-section__card-heading Sans-16px-black-85" pv-profile-section__card-heading  Sans-17px-black-85%>
														Datos personales y de contacto
													</h2>
												

												<div id="ember4749" class="ember-view">
													<div tabindex="-1" class="pv-profile-section__section-info section-info">
														<section class="pv-contact-info__contact-type ci-vanity-url">
															<span class="pv-contact-info__contact-icon svg-icon-wrap">
																<li-icon aria-hidden="true" type="linkedin-icon">
																	<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="social-icon" focusable="false">
																		<g style="fill: currentColor" class="solid-icon">
																			<rect x="-0.003" style="fill:none;" width="24" height="24"></rect>
																				<path style="" d="M20,2h-16c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V4C22,2.9,21.1,2,20,2zM8,19h-3v-9h3V19zM6.5,8.8C5.5,8.8,4.7,8,4.7,7s0.8-1.8,1.8-1.8S8.3,6,8.3,7S7.5,8.8,6.5,8.8zM19,19h-3v-4c0-1.4-0.6-2-1.5-2c-1.1,0-1.5,0.8-1.5,2.2V19h-3v-9h2.9v1.1c0.5-0.7,1.4-1.3,2.6-1.3c2.3,0,3.5,1.1,3.5,3.7V19z">
																				</path>
																		</g>
																	</svg>
																</li-icon>
															</span>

															<header class="pv-contact-info__header Sans-17px-black-85-semibold-dense">
																Tu perfil
															</header>
															<div class="pv-contact-info__ci-container">
															  <a href="<%= session.getAttribute("facebook")%>" class="pv-contact-info__contact-link">
																<%= session.getAttribute("facebook")%>
															  </a>
															</div>
														</section>

														<section class="pv-contact-info__contact-type ci-phone">
															<span class="pv-contact-info__contact-icon svg-icon-wrap">
																<li-icon aria-hidden="true" type="phone-handset-icon">
																	<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false">
																		<g class="large-icon" style="fill: currentColor">
																			<path d="M6.8,5.26l3.11,3.1L8.35,11,13,15.65l2.64-1.56,3.1,3.11-1.58,1.4a2.31,2.31,0,0,1-1.74.52c-1.2,0-3.8-1.4-6.47-4.07S4.88,9.78,4.88,8.59a2.3,2.3,0,0,1,.52-1.73L6.8,5.26M6.76,3h0A1,1,0,0,0,6,3.3L4,5.61a4.14,4.14,0,0,0-1,3c0,1.95,1.8,5,4.61,7.8S13.47,21,15.41,21a4.14,4.14,0,0,0,3-1l2.31-2a1,1,0,0,0,.3-0.73h0a1,1,0,0,0-.3-0.73L16.5,12.3a1.05,1.05,0,0,0-.75-0.3,1,1,0,0,0-.49.14l-2,1.15L10.7,10.7l1.15-2A1,1,0,0,0,12,8.25a1.05,1.05,0,0,0-.3-0.75L7.49,3.3A1,1,0,0,0,6.76,3h0Z">
																			</path>
																		</g>
																	</svg>
																</li-icon>
															</span>

															<header class="pv-contact-info__header Sans-17px-black-85-semibold-dense">
																n�mero de tel�fono
															</header>
															<ul class="list-style-none">
																<li class="pv-contact-info__ci-container">
																	<a href="tel:940978985" class="pv-contact-info__contact-link">
																	  <%=session.getAttribute("telefono")%>
																	</a>
																</li>
															</ul>
														</section>

														<section class="pv-contact-info__contact-type ci-email">
															<span class="pv-contact-info__contact-icon svg-icon-wrap">
																<li-icon aria-hidden="true" type="envelope-icon">
																	<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false">
																		<g class="large-icon" style="fill: currentColor">
																			<path d="M21,4H3A1,1,0,0,0,2,5V19a1,1,0,0,0,1,1H21a1,1,0,0,0,1-1V5A1,1,0,0,0,21,4ZM20,6V8l-8,5.24L4,8V6H20ZM4,18V9l7.32,4.78a1.25,1.25,0,0,0,1.37,0L20,9v9H4Z">
																			</path>
																		</g>
																	</svg>
																</li-icon>
															</span>

															<header class="pv-contact-info__header Sans-17px-black-85-semibold-dense">
																correo electr�nico
															</header>
															<div class="pv-contact-info__ci-container">
																<a href="mailto:nelsibelly@gmail.com" target="_blank" rel="noopener" class="pv-contact-info__contact-link">
																	<%= session.getAttribute("email")%>
																</a>
															</div>
														</section>

													</div>
												</div>

												
											</div>
										</section>
									</section>
								</div>
							</div>
                            <!-- fin ver contactos-->
                            <!-- inicio publicidad-->
                            <div class="row margin" style="padding-top: 10px !important; ">
                                <!-- profile-widget -->
                                <div class="col-lg-12 ">
                                    <div class="profile-widget profile-widget-info">
                                        <div class="panel-body">
                                            <div class="col-lg-12 col-sm-12 follow-info" >
                                                <br>
                                                <br>
                                                <br>
                                                <h1>Publicidad
                                                </h1>   
                                                <h2>Publicidad
                                                </h2>    
                                                <h3>
                                                    <span>
                                                        <i class="icon_pin_alt">
                                                        </i>
                                                    </span>
                                                    Per�
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- fin publicidad-->

                            
                        </section>
                        <!-- fin otros perfiles-->
                    </section> 
                    <!--main content end-->
                </div>
            </div>
			<!-- Fin de contenedor principal -->
        </section>
        <!-- Fin de seccion de contenedores -->

		
		
        <!-- Modals -->	

        <!-- inico perfil -->
        <div id="ember1" class="modal-wormhole visible pe-s-modal shared-modal-container pe-modal ember-view modals" >
            <div aria-labelledby="ember5616-modal-label" role="dialog" tabindex="-1" class="modal-wormhole-content">
                <div  class="modal-content-wrapper">
                    <h2 id="ember5616-modal-label" class="visually-hidden">
						Editar formulario
					</h2>
                    <div id="ember5626" class="carousel-form ember-view">
                        <div id="ember5658" class="breadcrumb-carousel ember-view">
                            <div id="ember5659" class="carousel-header ember-view"><!---->
                                <div id="ember5660" class="carousel-header__item Sans-21px-black-85 carousel-header__item--show ember-view">
                                    <div id="carousel-header-item-main" class="carousel-header__item-content">
                                        Editar presentaci�n
                                    </div>
                                </div>
                                <div id="ember5661" class="carousel-header__item Sans-21px-black-85 carousel-header__item--hide ember-view">
                                    <div id="carousel-header-item-profile-photo-cropper" class="carousel-header__item-content">
                                        Foto del perfil
                                    </div>
                                </div>
                                <div id="ember5662" class="carousel-header__item Sans-21px-black-85 carousel-header__item--hide ember-view">
                                    <div id="carousel-header-item-background-image-cropper" class="carousel-header__item-content">
                                        Recortar foto
                                    </div>
                                </div>
                                <div id="ember5663" class="carousel-header__item Sans-21px-black-85 carousel-header__item--hide ember-view">
                                    <div id="carousel-header-item-edit-treasury" class="carousel-header__item-content">
                                        Editar contenido multimedia
                                    </div>
                                </div>    
                                <div id="ember5664" class="ember-view">
                                    <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-5665="5665">
                                        <span class="svg-icon-wrap close" onclick="document.getElementById('ember1').style.display = 'none'"  >
                                            <span class="visually-hidden" >
												Cancelar
											</span>
                                            <li-icon aria-hidden="true" type="cancel-icon">
												<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
													<g class="large-icon" style="fill: currentColor">
														<path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z">
														</path>
													</g>
												</svg>
                                            </li-icon>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div id="ember5666" class="carousel-body ember-view">
                                <div id="ember5667" class="carousel-item focused-easeInOut-motion carousel-item--show ember-view">  
                                    <div id="ember5668" class="form-edit ember-view">
                                        <form name="perfil" method="post" action="PerfilController/updatePerfil.do" class="pe-form  pe-top-card-form" data-ember-action="" data-ember-action-5686="5686">
                                            <div class="pe-s-form__container">
                                                <header class="pe-s-form__header pe-form-header">
                                                    <h2 class="pe-form-header__title">
														Presentaci�n
													</h2>
                                                    <button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                        Guardar
                                                    </button>
                                                </header>
                                                <div class="pe-s-form__body pe-form-body pe-form-body--align-top">
                                                    <div class="pe-top-card-form__background-image-container relative">
                                                        <div id="ember5690" class="profile-background-image--no-rounded-corners profile-background-image profile-background-image--loading ember-view">
														</div>
                                                        <div id="ember5693" class="profile-background-image-edit-button ember-view">  
                                                            <div class="profile-background-image-edit-button__hint visibility-hidden Sans-15px-white-100">
                                                                1,584 x 396 px recomendado
                                                            </div>
                                                            <span class="profile-background-image-edit-button__edit-icon profile-background-image-edit-button__edit-icon--upload svg-icon-wrap">
                                                                <li-icon aria-hidden="true" type="pencil-icon" size="small">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
																		<g class="small-icon" style="fill-opacity: 1">
																			<path d="M14.71,4L12,1.29a1,1,0,0,0-1.41,0L3,8.85,1,15l6.15-2,7.55-7.55A1,1,0,0,0,15,4.71,1,1,0,0,0,14.71,4Zm-8.84,7.6-1.5-1.5L9.42,5.07l1.5,1.5Zm5.72-5.72-1.5-1.5,1.17-1.17,1.5,1.5Z">
																			</path>
																		</g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                            <input id="imagen_fondo" name="imagen_fondo" value="" type="file" id="top-card-form-background-image-edit-upload-input" accept="image/*" class="profile-background-image-edit-button__upload">
                                                        </div>
                                                    </div>
                                                    <div class="pe-form-body__content">
                                                        <div class="pe-form-body_top-area relative display-flex">
														</div>
                                                        <div class="pe-form-field pe-top-card-form__photo-field pe-top-card-form__photo-field--floating pe-top-card-form__photo-wrapper EntityPhoto-circle-8">
                                                            <div id="ember5694" class="pe-top-card-form__photo profile-photo-edit--large-preview profile-photo-edit ember-view">
                                                                <button data-control-name="edit_profile_photo" class="profile-photo-edit__edit-btn" data-ember-action="" data-ember-action-5695="5695">
                                                                    <img src="<%= session.getAttribute("url_imagen_personal")%>" class="profile-photo-edit__preview" alt="Editar foto" height="128" width="128">
                                                                    <span class="profile-photo-edit__edit-icon svg-icon-wrap">
                                                                        <li-icon aria-hidden="true" type="pencil-icon" size="small">
                                                                            <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
																				<g class="small-icon" style="fill-opacity: 1">
																					<path d="M14.71,4L12,1.29a1,1,0,0,0-1.41,0L3,8.85,1,15l6.15-2,7.55-7.55A1,1,0,0,0,15,4.71,1,1,0,0,0,14.71,4Zm-8.84,7.6-1.5-1.5L9.42,5.07l1.5,1.5Zm5.72-5.72-1.5-1.5,1.17-1.17,1.5,1.5Z">
																					</path>
																				</g>
                                                                            </svg>
                                                                        </li-icon>
                                                                    </span>
																	<input id="imagen_personal" name="imagen_personal" value="" type="file" id="top-card-form-background-image-edit-upload-input" accept="image/*" class="profile-background-image-edit-button__upload">
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="pe-s-multi-field">
                                                            <div class="pe-top-card-form__name-field">
                                                                <div data-form-elem-focus="true" class="pe-s-multi-field__item pe-form-field pe-top-card-form__first-name-field floating-label  ">
                                                                    <label for="topcard-firstname" class="pe-form-field__label label-text required">
                                                                        Nombres
                                                                    </label>
                                                                    <input type="text" name="nombre" maxlength="20" id="nombre" value="" class="ember-text-field pe-form-field__text-input ember-view">
                                                                </div>

                                                            </div>
                                                            <div class="pe-top-card-form__name-field">
                                                                <div data-form-elem-focus="true" class="pe-s-multi-field__item pe-form-field pe-top-card-form__last-name-field floating-label  ">
                                                                    <label for="topcard-lastname" class="pe-form-field__label label-text required">
                                                                        Apellidos
                                                                    </label>
                                                                    <input type="text" name="apellido" value="" maxlength="50" id="apellido" class="ember-text-field pe-form-field__text-input ember-view">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="pe-top-card-form__add-former-name-btn clearfix">
                                                            <button class="Sans-15px-black-semibold pe-form__action-on-field" data-ember-action="" data-ember-action-5700="5700">
                                                                A�adir nombre anterior
                                                            </button>
                                                        </div>
                                                        
                                                        <div class="pe-form-field pe-top-card-form__current-position-field">
                                                            <label for="topcard-position" class="pe-form-field__label">
                                                                Puesto actual
                                                            </label>
                                                            
															<select id="titular" name="titular" class="ember-view">
																<% List<ExperienciaPerfilBean> listaExperiencia2 = (ArrayList<ExperienciaPerfilBean>) session.getAttribute("listExperiencia");
																%> 
												
																<%for (ExperienciaPerfilBean lisexp : listaExperiencia2) {
																%>
                                                                <option value="<%= lisexp.getCargo()%> en <%= lisexp.getEmpresa()%>">
																	<%= lisexp.getCargo()%> en <%= lisexp.getEmpresa()%>
																</option>
                                                                <%}
																%>
                                                            </select>
															
                                                            <a href="/in/nelsi-melgarejo-50a120102/edit/position/new/" id="ember5705" class="Sans-15px-black-semibold pe-top-card-form__add-new-link ember-view">      
                                                                <span class="pe-top-card-form__add-new-position">
                                                                    A�adir nuevo puesto
                                                                </span>
                                                            </a>
                                                        </div>
                                                        <div class="pe-form-field pe-top-card-form__education-field">
                                                            <label for="topcard-education" class="pe-form-field__label">
                                                                Educaci�n
                                                            </label>
                                                            <select name="universidad" id="universidad" class="ember-view">
															<% List<EducacionPerfilBean> listaEducacion3 = (ArrayList<EducacionPerfilBean>) session.getAttribute("listEducacion");
																%> 
												
																<%for (EducacionPerfilBean d : listaEducacion3) {
																%>
                                                                <option value="<%=d.getUniversidad()%>">
                                                                    <%=d.getUniversidad()%>
                                                                </option>
																<%}
																%>
                                                            </select>
                                                            <a href="/in/nelsi-melgarejo-50a120102/edit/education/new/" id="ember5708" class="Sans-15px-black-semibold pe-top-card-form__add-new-link ember-view">      
                                                                <span class="pe-top-card-form__add-new-education">
                                                                    A�adir nueva educaci�n
                                                                </span>
                                                            </a>
                                                        </div>
                                                        <div id="ember5711" class="pe-top-card-form__location-picker ember-view">
                                                            <div class="pe-s-multi-field">
                                                                <div class="pe-s-multi-field__item pe-form-field ">
                                                                    <label for="location-country" class="pe-form-field__label">
                                                                        Pa�s
                                                                    </label>
                                                                    <select name="locationCountry" id="location-country" data-control-name="location_country_chooser" class="ember-view">
                                                                        <option value="al">Per�</option>
                                                                    </select>

                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="pe-form-field industry-field">
                                                            <label for="topcard-industry" class="pe-form-field__label required">
                                                                Sector
                                                            </label>
                                                            <select name="topcard-industry" id="topcard-industry" class="ember-view">  
                                                                <option value="Medicina Humana">Medicina Humana</option>				  
                                                            </select>
                                                        </div>
                                                        <div data-form-elem-focus="true" class="pe-form-field summary-field floating-label  ">
                                                            <label for="topcard-summary" class="pe-form-field__label label-text">
                                                                Extracto
                                                            </label>
                                                            <textarea id="lema" name="lema" value="" class="ember-text-area pe-form-field__textarea ember-view"></textarea>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                                <footer class="pe-s-form__footer pe-form-footer">
                                                    <!-- <div class="pe-form-osmosis__no-toggle-text">Los cambios anteriores no se compartir�n con tu red.</div> -->
                                                    <div class="pe-form-footer__actions">    
                                                        <button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                            Guardar
                                                        </button>
                                                    </div>
                                                </footer>
                                            </div>
                                        </form>
                                        <div id="ember6117" class="ember-view">
                                            <div id="ember6118" class="ember-view">
											</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="ember6119" class="carousel-item focused-easeInOut-motion carousel-item--pop visibility-hidden ember-view"><!----></div>
                                <div id="ember6120" class="carousel-item focused-easeInOut-motion carousel-item--pop visibility-hidden ember-view"><!----></div>
                                <div id="ember6121" class="carousel-item focused-easeInOut-motion carousel-item--pop visibility-hidden ember-view"><!----></div>
                            </div>
                        </div>
                        <div id="ember6122" class="ember-view">
                            <div id="ember6123" class="ember-view">
							</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-wormhole-overlay overlay-actions-are-disabled" data-ember-action="" data-ember-action-6124="6124">
            </div>
        </div>
        <!-- fin perfil -->

        <!-- inicio experiencia  -->
        <div id="ember2" class="modal-wormhole visible pe-s-modal shared-modal-container pe-modal ember-view">
            <div aria-labelledby="ember3585-modal-label" role="dialog" tabindex="-1" class="modal-wormhole-content">
                <div class="modal-content-wrapper">
                    <h2 id="ember3585-modal-label" class="visually-hidden">Editar formulario
					</h2>
                    <div id="ember3607" class="carousel-form ember-view">
                        <div id="ember3637" class="breadcrumb-carousel ember-view">
                            <div id="ember3642" class="carousel-header ember-view">
                                <div id="ember3649" class="carousel-header__item Sans-21px-black-85 carousel-header__item--show ember-view">
                                    <div id="carousel-header-item-main" class="carousel-header__item-content">
                                        Experiencia	
                                    </div>
                                </div>
                                <div id="ember3650" class="carousel-header__item Sans-21px-black-85 carousel-header__item--hide ember-view">
                                    <div id="carousel-header-item-edit-treasury" class="carousel-header__item-content">
                                        Editar contenido multimedia
                                    </div>
                                </div>    
                                <div id="ember3653" class="ember-view">
                                    <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-3654="3654">
                                        <span class="svg-icon-wrap" onclick="document.getElementById('ember2').style.display = 'none'">
                                            <span class="visually-hidden">
                                                Cancelar
                                            </span>
                                            <li-icon aria-hidden="true" type="cancel-icon">
                                                <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                <g class="large-icon" style="fill: currentColor">
                                                <path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z">
                                                </path>
                                                </g>
                                                </svg>
                                            </li-icon>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div id="ember3659" class="carousel-body ember-view">
                                <div id="ember3664" class="carousel-item focused-easeInOut-motion carousel-item--show ember-view">        
                                    <div id="ember3677" class="pe-position-form form-new ember-view">
                                        <form name="experiencia" method="post" action="ExperienciaPerfilController/insertExperiencia.do" class="pe-form " data-ember-action="" data-ember-action-3740="3740">
                                            <div class="pe-s-form__container">
                                                <header class="pe-s-form__header pe-form-header">
                                                    <div id="ember3741" class="ember-view">
                                                        <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-3742="3742">
                                                            <span class="svg-icon-wrap">
                                                                <span class="visually-hidden">
                                                                    Cancelar
                                                                </span>
                                                                <li-icon aria-hidden="true" type="cancel-icon">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                                    <g class="large-icon" style="fill: currentColor">
                                                                    <path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z"></path>
                                                                    </g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <h2 class="pe-form-header__title">
                                                        Experiencia
                                                    </h2>
                                                    <button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                        Guardar
                                                    </button>
                                                </header>
                                                <div class="pe-s-form__body pe-form-body">
                                                    <div id="ember3754" class="pe-form-field position-title floating-label ember-view">
                                                        <div class="pe-form-field position-title  ">
                                                            <section id="ember3776" class="pe-form-field__type-ahead ember-view">
                                                                <div id="ember3780" class="ember-view">  
                                                                    <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                        <div role="search" class="type-ahead-input-container">
                                                                            <div class="type-ahead-input-wrapper">
                                                                                <div class="type-ahead-input">
                                                                                    <label for="a11y-ember3776" class="label-text">
                                                                                        Cargo
                                                                                    </label>
                                                                                    <input id="cargo" value="" aria-autocomplete="list" type="text" name="cargo" autocomplete="off" spellcheck="false" placeholder="" maxlength="100" autocorrect="off" autocapitalize="off" id="a11y-ember3776" role="combobox" class="ember-text-field ember-view">
                                                                                    <div class="type-ahead-input-icons">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div aria-live="polite" class="live-region visually-hidden">
                                                                        </div>
																	</div>
                                                                </div>
                                                            </section>
                                                            <div id="ember3788" class="ember-view">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="ember3793" class="pe-form-field position-company-name has-logo floating-label ember-view"><!---->
                                                        <div class="pe-logo-container">
                                                            <img class="lazy-image pe-logo-container__img ghost-company loaded" alt="" height="24" width="24" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                                                            <section id="ember3796" class="type-ahead-logo-field pe-form-field__type-ahead ember-view">
                                                                <div id="ember3800" class="ember-view">  
                                                                    <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                        <div role="search" class="type-ahead-input-container">
                                                                            <div class="type-ahead-input-wrapper">
                                                                                <div class="type-ahead-input">
                                                                                    <label for="a11y-ember3796" class="label-text">
                                                                                        Empresa
                                                                                    </label>
                                                                                    <input id="empresa" value="" aria-autocomplete="list" type="text" name="empresa" autocomplete="off" spellcheck="false" placeholder="" maxlength="100" autocorrect="off" autocapitalize="off" id="a11y-ember3796" role="combobox" class="ember-text-field ember-view">
                                                                                    <div class="type-ahead-input-icons">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div aria-live="polite" class="live-region visually-hidden">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                        </div>
                                                        <div id="ember3804" class="ember-view">
                                                        </div>
                                                    </div>
                                                    <div id="ember3809" class="pe-form-field position-location-name floating-label ember-view">
                                                        <div class="pe-form-field position-location-name  ">
                                                            <section id="ember3812" class="pe-form-field__type-ahead ember-view">
                                                                <div id="ember3816" class="ember-view">  <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                        <div role="search" class="type-ahead-input-container">
                                                                            <div class="type-ahead-input-wrapper">
                                                                                <div class="type-ahead-input">
                                                                                    <label for="a11y-ember3812" class="label-text">
                                                                                        Ubicaci�n
                                                                                    </label>
                                                                                    <input id="ubicacion" value="" aria-autocomplete="list" type="text" name="ubicacion" autocomplete="off" spellcheck="false" placeholder="" maxlength="80" autocorrect="off" autocapitalize="off" id="a11y-ember3812" role="combobox" class="ember-text-field ember-view">
                                                                                    <div class="type-ahead-input-icons">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div aria-live="polite" class="live-region visually-hidden"></div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            <div id="ember3820" class="ember-view">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <fieldset id="ember3823" class="pe-form-time-period pe-form-field ember-view">
                                                        <div class="pe-form-time-period__container">
                                                            <fieldset class="pe-form-time-period__start-date">
                                                                <legend class="pe-form-field__label Sans-15px-black-55 block">
                                                                    De
                                                                </legend>
                                                                <div class="pe-form-time-period__date-field ">
                                                                    <div class="pe-form-time-period__date-unit">
                                                                        <label for="position-start-month" class="visually-hidden">
                                                                            Mes de inicio
                                                                        </label>
                                                                        <span id="ember3836" class="ember-view">
                                                                            <select name="mes_inicio" id="mes_inicio" data-control-name="edit_position_start_date_month" class="ember-view">  
																				<option value="0">Mes</option>
                                                                                <option value="Enero">Enero</option>
                                                                                <option value="Febrero">Febrero</option>
																				<option value="Marzo">Marzo</option>
                                                                                <option value="Abril">Abril</option>
																				<option value="Mayo">Mayo</option>
                                                                                <option value="Junio">Junio</option>
																				<option value="Julio">Julio</option>
                                                                                <option value="Agosto">Agosto</option>
																				<option value="Setiembre">Setiembre</option>
                                                                                <option value="Octubre">Octubre</option>
																				<option value="Noviembre">Noviembre</option>
                                                                                <option value="Diciembre">Diciembre</option>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                    <div class="pe-form-time-period__date-unit">
                                                                        <label for="position-start-year" class="visually-hidden">
                                                                            A�o inicio
                                                                        </label>
                                                                        <span id="ember3858" class="ember-view">
                                                                            <select name="anio_inicio" id="anio_inicio" data-control-name="edit_position_start_date_year" class="ember-view">
																			<option value="0">A�o</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2017">2017</option>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                            <fieldset class="pe-form-time-period__end-date">
                                                                <legend class="pe-form-field__label Sans-15px-black-55 block">
                                                                    a
                                                                </legend>
                                                                <div class="pe-form-time-period__date-field ">
                                                                    <div class="pe-form-time-period__date-unit">
                                                                        <label for="position-end-month" class="visually-hidden">
                                                                            Mes de finalizaci�n
                                                                        </label>
                                                                        <span id="ember3920" class="ember-view">
                                                                            <select name="mes_fin" id="mes_fin" data-control-name="edit_position_end_date_month" class="ember-view">  <option value="0">Mes</option>
                                                                                <option value="Enero">enero</option>
                                                                                <option value="Febrero">febrero</option>
                                                                                <option value="Marzo">marzo</option>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                    <div class="pe-form-time-period__date-unit">
                                                                        <label for="position-end-year" class="visually-hidden">
                                                                            A�o de finalizaci�n
                                                                        </label>
                                                                        <span id="ember3934" class="ember-view">
                                                                            <select name="anio_fin" id="anio_fin" data-control-name="edit_position_end_date_year" class="ember-view">  <option value="0">A�o</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2016">2016</option>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div id="ember3996" class="ember-view">
                                                        </div>
                                                        <div id="ember3999" class="pe-form-time-period__ongoing-activity mt4 ember-view">
                                                            <input data-control-name="edit_position_date_toggle" tabindex="0" type="checkbox" id="position-currently-works-here" class="ember-checkbox state-checkbox visually-hidden ember-view">
                                                            <label for="position-currently-works-here">
                                                                <span class="label-text ml2">
                                                                    Trabajo aqu�� actualmente
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </fieldset>
                                                    <div id="ember4005" class="pe-form-field position-description floating-label ember-view"><!---->
                                                        <label for="position-description" class="pe-form-field__label label-text">
                                                            Descripci�n
                                                        </label>
                                                        <textarea name="descripcion" id="descripcion" class="ember-text-area pe-form-field__textarea ember-view">
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <footer class="pe-s-form__footer pe-form-footer">
                                                    <div class="pe-form-footer__actions">
                                                        <button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                            Guardar
                                                        </button>
                                                    </div>
                                                </footer>
                                            </div>
                                        </form>

                                        <div id="ember4045" class="ember-view">
                                            <div id="ember4046" class="ember-view">
                                            </div>
                                        </div>
                                        <div id="ember4047" class="ember-view">
                                            <div id="ember4048" class="ember-view">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="ember4049" class="carousel-item focused-easeInOut-motion carousel-item--pop visibility-hidden ember-view">
                                </div>
                            </div>
                        </div>
                        <div id="ember4050" class="ember-view">
                            <div id="ember4051" class="ember-view">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-wormhole-overlay overlay-actions-are-disabled" data-ember-action="" data-ember-action-4052="4052">
            </div>
        </div>
        <!-- fin experiencia  -->

        <!-- Inicio educacion -->
        <div id="ember3" class="modal-wormhole visible pe-s-modal shared-modal-container pe-modal ember-view">
            <div aria-labelledby="ember4627-modal-label" role="dialog" tabindex="-1" class="modal-wormhole-content">
                <div class="modal-content-wrapper">        
                    <h2 id="ember4627-modal-label" class="visually-hidden">Editar formulario</h2>
                    <div id="ember4631" class="carousel-form ember-view">
                        <div id="ember4645" class="breadcrumb-carousel ember-view">
                            <div id="ember4646" class="carousel-header ember-view">
                                <div id="ember4647" class="carousel-header__item Sans-21px-black-85 carousel-header__item--show ember-view">
                                    <div id="carousel-header-item-main" class="carousel-header__item-content">
                                        Educaci�n
                                    </div>
                                </div>
                                <div id="ember4648" class="carousel-header__item Sans-21px-black-85 carousel-header__item--hide ember-view">
                                    <div id="carousel-header-item-edit-treasury" class="carousel-header__item-content">
                                        Editar contenido multimedia
                                    </div>
                                </div>    
                                <div id="ember4649" class="ember-view">
                                    <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-4650="4650">
                                        <span class="svg-icon-wrap" onclick="document.getElementById('ember3').style.display = 'none'">
                                            <span class="visually-hidden">
                                                Cancelar
                                            </span>
                                            <li-icon aria-hidden="true" type="cancel-icon">
                                                <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                <g class="large-icon" style="fill: currentColor">
                                                <path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z">
                                                </path>
                                                </g>
                                                </svg>
                                            </li-icon>
                                        </span>
                                    </button>
                                </div>

                            </div>
                            <div id="ember4651" class="carousel-body ember-view">
                                <div id="ember4652" class="carousel-item focused-easeInOut-motion carousel-item--show ember-view">        
                                    <div id="ember4653" class="pe-education-form form-new ember-view">
                                        <form name="educacion" method="post" action="EducacionPerfilController/insertEducacion.do" class="pe-form "  data-ember-action="" data-ember-action-4663="4663">
                                            <div class="pe-s-form__container">
                                                <header class="pe-s-form__header pe-form-header">
                                                    <div id="ember4664" class="ember-view">
                                                        <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-4665="4665">
                                                            <span class="svg-icon-wrap">
                                                                <span class="visually-hidden">
                                                                    Cancelar
                                                                </span>
                                                                <li-icon aria-hidden="true" type="cancel-icon">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                                    <g class="large-icon" style="fill: currentColor">
                                                                    <path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z">
                                                                    </path>
                                                                    </g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <h2 class="pe-form-header__title">
                                                        Educaci�n
                                                    </h2>
                                                    <button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                        Guardar
                                                    </button>
                                                </header>
                                                <div class="pe-s-form__body pe-form-body">
                                                    <div class="pe-form-field edu-school-name has-logo  ">
               											<div class="pe-logo-container">
                                                            <img class="lazy-image pe-logo-container__img ghost-school loaded" alt="" height="24" width="24" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                                                            <section id="ember4667" class="type-ahead-logo-field pe-form-field__type-ahead ember-view">
                                                                <div id="ember4671" class="ember-view">  
                                                                    <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                        <div role="search" class="type-ahead-input-container">
                                                                            <div class="type-ahead-input-wrapper">
                                                                                <div class="type-ahead-input">
                                                                                    <label for="a11y-ember4667" class="label-text">
                                                                                        Universidad
                                                                                    </label>
                                                                                    <input aria-autocomplete="list" type="text" value="" name="universidad" autocomplete="off" spellcheck="false" placeholder="" autocorrect="off" autocapitalize="off" id="universidad" role="combobox" class="ember-text-field ember-view">
                                                                                    <div class="type-ahead-input-icons">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div aria-live="polite" class="live-region visually-hidden">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                        </div>
                                                        <div id="ember4673" class="ember-view">
                                                        </div>
                                                    </div>
                                                    <div class="pe-form-field edu-degree-name  ">
                                                        <section id="ember4674" class="pe-form-field__type-ahead ember-view">
                                                            <div id="ember4678" class="ember-view">  
                                                                <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                   													<div role="search" class="type-ahead-input-container">
                                                                        <div class="type-ahead-input-wrapper">
                                                                            <div class="type-ahead-input">
                                                                                <label for="a11y-ember4674" class="label-text">
                                                                                    Titulaci�n
                                                                                </label>
                                                                                <input aria-autocomplete="list" type="text" value="" name="titulacion" autocomplete="off" spellcheck="false" placeholder="" autocorrect="off" autocapitalize="off" id="titulacion" role="combobox" class="ember-text-field ember-view">
                                                                                <div class="type-ahead-input-icons">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div aria-live="polite" class="live-region visually-hidden">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <div id="ember4680" class="ember-view">
                                                        </div>
                                                    </div>
                                                    <div class="pe-form-field edu-field-of-study  ">
                                                        <section id="ember4681" class="pe-form-field__type-ahead ember-view">
                                                            <div id="ember4685" class="ember-view">  
                                                                <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                    <div role="search" class="type-ahead-input-container">
                                                                        <div class="type-ahead-input-wrapper">
                                                                            <div class="type-ahead-input">
                                                                                <label for="a11y-ember4681" class="label-text">
                                                                                    Disciplina acad�mica
                                                                                </label>
                                                                                <input aria-autocomplete="list" type="text" value="" name="disciplina" autocomplete="off" spellcheck="false" placeholder="" autocorrect="off" autocapitalize="off" id="disciplina" role="combobox" class="ember-text-field ember-view">
                                                                                <div class="type-ahead-input-icons">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div aria-live="polite" class="live-region visually-hidden">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <div id="ember4687" class="ember-view">
                                                        </div>
                                                    </div>
                                                    <div class="pe-form-field floating-label  ">
                                                        <label for="edu-grade" class="pe-form-field__label label-text">
                                                            Nota
                                                        </label>
                                                        <input type="text" maxlength="80" value="" id="nota_media" name="nota_media" class="ember-text-field pe-form-field__text-input ember-view">

                                                    </div>
                                                    <div class="pe-form-field pe-education-form__activities-field floating-label  ">
                                                        <label for="edu-activities-societies" class="pe-form-field__label label-text">
                                                            Actividades y grupos
                                                        </label>
                                                        <textarea id="actividades" name="actividades" value="" class="pe-education-form__activities-text ember-text-area pe-form-field__textarea ember-view"></textarea>
                                                        <div class="pe-form-field__aux">
                                                            <p class="pe-form-field__help-text Sans-13px-black-55-italic">
                                                                Ejemplos: equipo de f�tbol, tuna, coro
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <fieldset class="pe-form-field pe-form-time-period ">
                                                        <legend class="pe-form-field__label Sans-13px-black-55 block">
                                                            Periodo de tiempo
                                                        </legend>
                                                        <div class="pe-form-time-period__container">
                                                            <div class="pe-form-time-period__start-date">
                                                                <label for="pe-education-form__start-year" class="pe-form-field__label">
                                                                    Desde el a�o
                                                                </label>
                                                                <span id="ember4694" class="ember-view">
                                                                    <select id="anio_inicio" name="anio_inicio" data-control-name="edit_education_start_year" class="pe-education-form__start-year ember-view">  
																		<option value="">A�o</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2015">2015</option>
                                                                    </select>
                                                                </span>
                                                            </div>

                                                            <div class="pe-form-time-period__end-date">
                                                                <label for="pe-education-form__end-year" class="pe-form-field__label">
                                                                    Hasta el a�o (o previsto)
                                                                </label>
                                                                <span id="ember4756" class="ember-view">
                                                                    <select id="anio_fin" name="anio_fin" data-control-name="edit_education_end_year" class="pe-education-form__end-year ember-view">  
																		<option value="">A�o</option>
                                                                        <option value="2024">2024</option>
                                                                        <option value="2023">2023</option>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div id="ember4818" class="ember-view">
                                                        </div>
                                                    </fieldset>

                                                    <div class="pe-form-field edu-description floating-label  ">
                                                        <label for="edu-description" class="pe-form-field__label label-text">
                                                            Descripci�n
                                                        </label>
                                                        <textarea name="descripcion" id="descripcion" value="" class="ember-text-area pe-form-field__textarea ember-view">
                                                        </textarea>
                                                    </div>
 
                                                </div>
                                                <footer class="pe-s-form__footer pe-form-footer">
                                                    <div class="pe-form-footer__actions">
                                                        <button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                            Guardar
                                                        </button>
                                                    </div>
                                                </footer>
                                            </div>
                                        </form>
                                        <div id="ember4825" class="ember-view">
                                            <div id="ember4826" class="ember-view">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="ember4827" class="carousel-item focused-easeInOut-motion carousel-item--pop visibility-hidden ember-view"><!---->
                                </div>
                            </div>
                        </div>
                        <div id="ember4828" class="ember-view">
                            <div id="ember4829" class="ember-view">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-wormhole-overlay overlay-actions-are-disabled" data-ember-action="" data-ember-action-4830="4830">
            </div>
        </div>
        <!-- fin educacion -->	
		
		<!-- inicio logros -->
		<div id="ember4" class="modal-wormhole visible pe-s-modal shared-modal-container pe-modal ember-view">
		<div aria-labelledby="ember3822-modal-label" role="dialog" tabindex="-1" class="modal-wormhole-content">
			<div class="modal-content-wrapper" style="margin-top: 65px;">
			<h2 id="ember3822-modal-label" class="visually-hidden">Editar formulario</h2>
				<div id="ember3842" class="form-new ember-view">  
					<form name="logro" method="post" action="LogroPerfilController/insertLogro.do" class="pe-form " data-ember-action="" data-ember-action-3868="3868">
						<div class="pe-s-form__container">
							<header class="pe-s-form__header pe-form-header">
								<div id="ember3871" class="ember-view">
									<button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-3872="3872">
										<span class="svg-icon-wrap" onclick="document.getElementById('ember4').style.display='none'">
											<span class="visually-hidden">
												Cancelar
											</span>
											<li-icon aria-hidden="true" type="cancel-icon">
												<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
													<g class="large-icon" style="fill: currentColor">
														<path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z">
														</path>
													</g>
												</svg>
											</li-icon>
										</span>
									</button>
								</div>
								<h2 class="pe-form-header__title">
								  A�adir idioma
								</h2>
							</header>
							<div class="pe-s-form__body pe-form-body">
								<div class="pe-form-field language-name floating-label ">
									<section id="language-name" class="pe-form-field__type-ahead ember-view">
										<div id="ember3904" class="ember-view">  
											<div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
												<div role="search" class="type-ahead-input-container">
													<div class="type-ahead-input-wrapper">
														<div class="type-ahead-input">
															<label for="a11y-ember3903" class="label-text">
																Idioma
															</label>
															<input id="nombre_logro" name="nombre_logro" value="" aria-autocomplete="list" type="text" autocomplete="off" spellcheck="false" placeholder="" maxlength="80" autocorrect="off" autocapitalize="off" id="a11y-ember3903" role="combobox" class="ember-text-field ember-view">
															<div class="type-ahead-input-icons">
															</div>
														</div>
													</div>
												</div>
												<div aria-live="polite" class="live-region visually-hidden">
												</div>
											</div>
										</div>
									</section>
								</div>

								<div class="pe-form-field language-proficiency floating-label">
									<label for="language-proficiency" class="pe-form-field__label">
										Competencia
									</label>

									<select id="language-proficiency" data-control-name="select_proficiency" class="ember-view">  <option value="">-</option>

										<option value="ELEMENTARY">Competencia b�sica</option>
										<option value="LIMITED_WORKING">Competencia b�sica limitada</option>
										<option value="PROFESSIONAL_WORKING">Competencia b�sica profesional</option>
										<option value="FULL_PROFESSIONAL">Competencia profesional completa</option>
										<option value="NATIVE_OR_BILINGUAL">Competencia biling�e o nativa</option>
									</select>
								</div>
							</div>
							<footer class="pe-s-form__footer pe-form-footer">
								<div class="pe-form-footer__actions">
									<button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
										Guardar
									</button>
							  </div>
							</footer>
						</div>
					</form>

					<div id="ember3928" class="ember-view">
						<div id="ember3929" class="ember-view"><!---->
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="modal-wormhole-overlay overlay-actions-are-disabled" data-ember-action="" data-ember-action-3930="3930">
		</div>
	</div>
	
		<!-- infromacion de contacto -->
		<div id="ember5" class="modal-wormhole visible pe-s-modal shared-modal-container pe-modal ember-view modals" >
			<div aria-labelledby="ember5616-modal-label" role="dialog" tabindex="-1" class="modal-wormhole-content">
				<div  class="modal-content-wrapper" style="margin-top: 65px;">
					<h2 id="ember4879-modal-label" class="visually-hidden">
						Editar formulario
					</h2>
					<div id="ember4883" class="pe-contact-info-form form-edit ember-view">  
						<form name="contacto" method="post" action="PersonalController/updatePersonal.do" class="pe-form " data-ember-action="" data-ember-action-4885="4885">
							<div class="pe-s-form__container">
								<header class="pe-s-form__header pe-form-header">
									<div id="ember4886" class="ember-view">
										<button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-4887="4887">
		  
											<span class="svg-icon-wrap" onclick="document.getElementById('ember5').style.display = 'none'">
												<span class="visually-hidden">
													Cancelar
												</span>
													<li-icon aria-hidden="true" type="cancel-icon">
													<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false">
														<g class="large-icon" style="fill: currentColor">
															<path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z"></path>
														</g>
													</svg>
													</li-icon>
											</span>
										</button>
									</div>
									<h2 class="pe-form-header__title" style="Sans-21px-black-85">
										Informaci�n de contacto
									</h2>
								</header>
								<div class="pe-s-form__body pe-form-body">
									<div class="pe-form-field pe-contact-info-form__profile-url-field">
										<label role="presentation" class="pe-form-field__label">
											URL del perfil
										</label>

										<a target="_blank" href="<%= session.getAttribute("facebook")%>" id="ember4890" class="pe-contact-info-form__external-link Sans-15px-black-55-dense ember-view">    
											<%= session.getAttribute("facebook")%>
											<span class="pe-contact-info-form__external-link-icon icon-flipped svg-icon-wrap">
												<span class="visually-hidden">
													URL del perfil
												</span>
												<li-icon aria-hidden="true" type="arrow-up-left-icon" size="small">
													<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false">
														<g class="small-icon" style="fill-opacity: 1">
															<path d="M12.68,14L4,5.33V11H2V3A1,1,0,0,1,3,2h8V4H5.32L14,12.68Z">
															</path>
														</g>
													</svg>
												</li-icon>
											</span>
										</a>
									</div>
									<div id="ember4892" class="pe-contact-info-form__phone-field ember-view">
										<div data-form-elem-focus="true" class="pe-form-field floating-label  ">
											<label for="contact-phone-ember4892" class="pe-form-field__label label-text">
												Facebook
											</label>

											<div class="pe-contact-info-form__types-field">
												<div class="pe-contact-info-form__types-input">
													<input type="text" id="facebook" value="<%= session.getAttribute("facebook")%>" name="facebook" class="ember-text-field pe-form-field__text-input ember-view">
												</div>
											</div>
										</div>
									</div>
									<div id="ember4892" class="pe-contact-info-form__phone-field ember-view">
										<div data-form-elem-focus="true" class="pe-form-field floating-label  ">
											<label for="contact-phone-ember4892" class="pe-form-field__label label-text">
												Twitter
											</label>

											<div class="pe-contact-info-form__types-field">
												<div class="pe-contact-info-form__types-input">
													<input type="text" id="twitter" value="" name="twitter" class="ember-text-field pe-form-field__text-input ember-view">
												</div>
											</div>
										</div>
									</div>
									<div id="ember4892" class="pe-contact-info-form__phone-field ember-view">
										<div data-form-elem-focus="true" class="pe-form-field floating-label  ">
											<label for="contact-phone-ember4892" class="pe-form-field__label label-text">
												Tel�fono
											</label>

											<div class="pe-contact-info-form__types-field">
												<div class="pe-contact-info-form__types-input">
													<input type="text" maxlength="25" id="telefono" value="<%=session.getAttribute("telefono")%>" name="telefono" class="ember-text-field pe-form-field__text-input ember-view">
												</div>
												<label for="contact-phone-type-ember4892" class="visually-hidden">
													Tipo de n�mero de tel�fono
												</label>
												<select id="contact-phone-type-ember4892" data-control-name="edit_phone_type" class="pe-contact-info-form__types-select ember-view"><!---->
												  <option value="MOBILE">M�vil</option>
												  <option value="HOME">Casa</option>
												  <option value="WORK">Trabajo</option>
												  
												</select>
											</div>
										</div>
									</div>
	
									

									<div class="pe-form-field pe-contact-info-form__email-field">
										<label role="presentation" class="pe-form-field__label">
											Direcci�n de correo electr�nico
										</label>
										<a target="_blank" href="mailto:<%= session.getAttribute("email")%>?cc=<%= session.getAttribute("email")%>&bcc=<%= session.getAttribute("email")%>&subject=Subject Using Mailto.co.uk&body=Email Using Body" id="ember4899" class="pe-contact-info-form__external-link Sans-15px-black-55-dense ember-view">    
											<%= session.getAttribute("email")%>
											<span class="pe-contact-info-form__external-link-icon icon-flipped svg-icon-wrap">
												<span class="visually-hidden">
												Direcci�n de correo electr�nico
												</span>
												<li-icon aria-hidden="true" type="arrow-up-left-icon" size="small">
												<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false">
													<g class="small-icon" style="fill-opacity: 1">
														<path d="M12.68,14L4,5.33V11H2V3A1,1,0,0,1,3,2h8V4H5.32L14,12.68Z"></path>
													</g>
												</svg>
											  </li-icon>
											</span>
										</a>
									</div>

									
									<!--

									<fieldset class="pe-form-field birthday-field ">
										<legend class="visually-hidden">
											Cumplea�os
										</legend>
										<label aria-hidden="true" class="pe-form-field__label">
											Cumplea�os
										</label>

										<div class="pe-form-time-period__single-date-container">
											<div class="pe-form-field__month-year-date">
												<label for="birthday-day" class="visually-hidden">
													D�a de nacimiento
												</label>

												<span id="ember4901" class="ember-view">
													<select id="birthday-day" data-control-name="edit_birthday_day" class="ember-view">  
														<option value="">D�a</option>

														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														<option value="13">13</option>
														<option value="14">14</option>
														<option value="15">15</option>
														<option value="16">16</option>
														<option value="17">17</option>
														<option value="18">18</option>
														<option value="19">19</option>
														<option value="20">20</option>
														<option value="21">21</option>
														<option value="22">22</option>
														<option value="23">23</option>
														<option value="24">24</option>
														<option value="25">25</option>
														<option value="26">26</option>
														<option value="27">27</option>
														<option value="28">28</option>
														<option value="29">29</option>
														<option value="30">30</option>
														<option value="31">31</option>
													</select>
												</span>
											</div>
											<div class="pe-form-field__month-year-date">
												<label for="birthday-month" class="visually-hidden">
													Mes de nacimiento
												</label>

												<span id="ember4934" class="ember-view">
													<select id="birthday-month" data-control-name="edit_birthday_month" class="ember-view">  
														<option value="">Mes</option>

														  <option value="1">enero</option>
														  <option value="2">febrero</option>
														  <option value="3">marzo</option>
														  <option value="4">abril</option>
														  <option value="5">mayo</option>
														  <option value="6">junio</option>
														  <option value="7">julio</option>
														  <option value="8">agosto</option>
														  <option value="9">septiembre</option>
														  <option value="10">octubre</option>
														  <option value="11">noviembre</option>
														  <option value="12">diciembre</option>
													</select>
												</span>
											</div>
										</div>

										<div id="ember4948" class="mt1 visibility-setting visibility-setting--trailing ember-view">
											<div id="ember4949" class="artdeco-dropdown visibility-setting__dropdown dropdown closed ember-view">
												<button type="button" aria-controls="ember4949-options" aria-expanded="false" data-control-name="change_visibility" id="ember4949-trigger" class="visibility-setting__trigger Sans-15px-black-55 dropdown-trigger ember-view">    
													<span class="visibility-setting__trigger-content">
														<span class="visibility-setting__eyeball visibility-setting__eyeball--non-rl svg-icon-wrap">
															<li-icon aria-hidden="true" type="eyeball-icon" size="small">
																<svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon" focusable="false">
																<g class="small-icon" style="fill-opacity: 1">
																		<path d="M8,2C2.4,2,0,8,0,8s2.35,6,8,6,8-6,8-6S13.6,2,8,2ZM8,12.25C4.57,12.25,2.64,9.34,1.94,8,2.65,6.68,4.61,3.75,8,3.75S13.35,6.67,14.06,8C13.36,9.33,11.42,12.25,8,12.25ZM8,5a3,3,0,1,0,3,3A3,3,0,0,0,8,5ZM8,9.25A1.25,1.25,0,1,1,9.25,8,1.25,1.25,0,0,1,8,9.25Z"></path>
																</g>
																</svg>
															</li-icon>
														</span>
														<span class="mh1">
															El cumplea�os es visible para:
														</span>
															Tu red
													</span>
												</button>
												<ul id="ember4949-options" style="display: none;" class="visibility-options visibility-options__tooltip visibility-options__tooltip--bottom ember-view" tabindex="-1">
												</ul>
											</div>
										</div>

									</fieldset>
									-->
								</div>

								<footer class="pe-s-form__footer pe-form-footer">
									
									<div class="pe-form-footer__actions">   
										<button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100 ">
											Guardar
										</button>
									</div>
								</footer>

							</div>
						</form>

						<div id="ember4957" class="ember-view">
							<div id="ember4958" class="ember-view">
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="modal-wormhole-overlay overlay-actions-are-disabled" data-ember-action="" data-ember-action-4959="4959"></div>
		</div>

		
		<!-- inicio experiencia 2  -->
        <div id="ember6" class="modal-wormhole visible pe-s-modal shared-modal-container pe-modal ember-view">
            <div aria-labelledby="ember3585-modal-label" role="dialog" tabindex="-1" class="modal-wormhole-content">
                <div class="modal-content-wrapper">
                    <h2 id="ember3585-modal-label" class="visually-hidden">Editar formulario
					</h2>
                    <div id="ember3607" class="carousel-form ember-view">
                        <div id="ember3637" class="breadcrumb-carousel ember-view">
                            <div id="ember3642" class="carousel-header ember-view">
                                <div id="ember3649" class="carousel-header__item Sans-21px-black-85 carousel-header__item--show ember-view">
                                    <div id="carousel-header-item-main" class="carousel-header__item-content">
                                        Experiencia	
                                    </div>
                                </div>
                                <div id="ember3650" class="carousel-header__item Sans-21px-black-85 carousel-header__item--hide ember-view">
                                    <div id="carousel-header-item-edit-treasury" class="carousel-header__item-content">
                                        Editar contenido multimedia
                                    </div>
                                </div>    
                                <div id="ember3653" class="ember-view">
                                    <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-3654="3654">
                                        <span class="svg-icon-wrap" onclick="document.getElementById('ember6').style.display = 'none'">
                                            <span class="visually-hidden">
                                                Cancelar
                                            </span>
                                            <li-icon aria-hidden="true" type="cancel-icon">
                                                <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                <g class="large-icon" style="fill: currentColor">
                                                <path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z">
                                                </path>
                                                </g>
                                                </svg>
                                            </li-icon>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div id="ember3659" class="carousel-body ember-view">
                                <div id="ember3664" class="carousel-item focused-easeInOut-motion carousel-item--show ember-view">        
                                    <div id="ember3677" class="pe-position-form form-new ember-view">
                                        <form name="experiencia2" method="post" action="ExperienciaPerfilController/updateExperiencia.do" class="pe-form " data-ember-action="" data-ember-action-3740="3740">
										
                                            <div class="pe-s-form__container">
                                                <header class="pe-s-form__header pe-form-header">
                                                    <div id="ember3741" class="ember-view">
                                                        <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-3742="3742">
                                                            <span class="svg-icon-wrap">
                                                                <span class="visually-hidden">
                                                                    Cancelar
                                                                </span>
                                                                <li-icon aria-hidden="true" type="cancel-icon">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                                    <g class="large-icon" style="fill: currentColor">
                                                                    <path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z"></path>
                                                                    </g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <h2 class="pe-form-header__title">
                                                        Experiencia
                                                    </h2>
                                                    <button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                        Guardar
                                                    </button>
                                                </header>
                                                <div class="pe-s-form__body pe-form-body">
                                                    <div id="ember3754" class="pe-form-field position-title floating-label ember-view">
                                                        <div class="pe-form-field position-title  ">
                                                            <section id="ember3776" class="pe-form-field__type-ahead ember-view">
                                                                <div id="ember3780" class="ember-view">  
                                                                    <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                        <div role="search" class="type-ahead-input-container">
                                                                            <div class="type-ahead-input-wrapper">
                                                                                <div class="type-ahead-input">
                                                                                    <label for="a11y-ember3776" class="label-text">
                                                                                        Cargo
                                                                                    </label>
																					<select name="cargo_update" id="cargo_update" onchange="ShowSelectedExperiencia();" data-control-name="edit_position_start_date_month" class="ember-view">  
																						<% int z = 0;
																						for (ExperienciaPerfilBean aa : listaExperiencia) {
																							String cadena = aa.getExp_id()+"-"+aa.getCargo()+"-"+aa.getEmpresa()+"-"+aa.getMes_inicio()+"-"+aa.getAnio_inicio()+"-"+aa.getMes_fin()+"-"+aa.getAnio_fin()+"-"+aa.getLugar()+"-"+aa.getDescripcion();
																						%>
																							<option value="<%=cadena%>"><%=aa.getCargo()%></option>
																						<%
																						z++;
																						}%>
																					</select>
																					<input id="exp_id" name="exp_id" type="hidden" value=""></input>
                                                                                    <div class="type-ahead-input-icons">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div aria-live="polite" class="live-region visually-hidden">
                                                                        </div>
																	</div>
                                                                </div>
                                                            </section>
                                                            <div id="ember3788" class="ember-view">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="ember3793" class="pe-form-field position-company-name has-logo floating-label ember-view"><!---->
                                                        <div class="pe-logo-container">
                                                            <img class="lazy-image pe-logo-container__img ghost-company loaded" alt="" height="24" width="24" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                                                            <section id="ember3796" class="type-ahead-logo-field pe-form-field__type-ahead ember-view">
                                                                <div id="ember3800" class="ember-view">  
                                                                    <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                        <div role="search" class="type-ahead-input-container">
                                                                            <div class="type-ahead-input-wrapper">
                                                                                <div class="type-ahead-input">
                                                                                    <label for="a11y-ember3796" class="label-text">
                                                                                        Empresa
                                                                                    </label>
                                                                                    <input id="empresa" value="" aria-autocomplete="list" type="text" name="empresa" autocomplete="off" spellcheck="false" placeholder="" maxlength="100" autocorrect="off" autocapitalize="off" id="a11y-ember3796" role="combobox" class="ember-text-field ember-view">
                                                                                    <div class="type-ahead-input-icons">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div aria-live="polite" class="live-region visually-hidden">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                        </div>
                                                        <div id="ember3804" class="ember-view">
                                                        </div>
                                                    </div>
                                                    <div id="ember3809" class="pe-form-field position-location-name floating-label ember-view">
                                                        <div class="pe-form-field position-location-name  ">
                                                            <section id="ember3812" class="pe-form-field__type-ahead ember-view">
                                                                <div id="ember3816" class="ember-view">  <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                        <div role="search" class="type-ahead-input-container">
                                                                            <div class="type-ahead-input-wrapper">
                                                                                <div class="type-ahead-input">
                                                                                    <label for="a11y-ember3812" class="label-text">
                                                                                        Ubicaci�n
                                                                                    </label>
                                                                                    <input id="ubicacion" value="" aria-autocomplete="list" type="text" name="ubicacion" autocomplete="off" spellcheck="false" placeholder="" maxlength="80" autocorrect="off" autocapitalize="off" id="a11y-ember3812" role="combobox" class="ember-text-field ember-view">
                                                                                    <div class="type-ahead-input-icons">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div aria-live="polite" class="live-region visually-hidden"></div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            <div id="ember3820" class="ember-view">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <fieldset id="ember3823" class="pe-form-time-period pe-form-field ember-view">
                                                        <div class="pe-form-time-period__container">
                                                            <fieldset class="pe-form-time-period__start-date">
                                                                <legend class="pe-form-field__label Sans-15px-black-55 block">
                                                                    De
                                                                </legend>
                                                                <div class="pe-form-time-period__date-field ">
                                                                    <div class="pe-form-time-period__date-unit">
                                                                        <label for="position-start-month" class="visually-hidden">
                                                                            Mes de inicio
                                                                        </label>
                                                                        <span id="ember3836" class="ember-view">
                                                                            <select name="mes_inicio" id="mes_inicio" data-control-name="edit_position_start_date_month" class="ember-view">  
																				<option value="0">Mes</option>
                                                                                <option value="Enero">Enero</option>
                                                                                <option value="Febrero">Febrero</option>
																				<option value="Marzo">Marzo</option>
                                                                                <option value="Abril">Abril</option>
																				<option value="Mayo">Mayo</option>
                                                                                <option value="Junio">Junio</option>
																				<option value="Julio">Julio</option>
                                                                                <option value="Agosto">Agosto</option>
																				<option value="Setiembre">Setiembre</option>
                                                                                <option value="Octubre">Octubre</option>
																				<option value="Noviembre">Noviembre</option>
                                                                                <option value="Diciembre">Diciembre</option>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                    <div class="pe-form-time-period__date-unit">
                                                                        <label for="position-start-year" class="visually-hidden">
                                                                            A�o inicio
                                                                        </label>
                                                                        <span id="ember3858" class="ember-view">
                                                                            <select name="anio_inicio" id="anio_inicio" data-control-name="edit_position_start_date_year" class="ember-view">
																			<option value="0">A�o</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2017">2017</option>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                            <fieldset class="pe-form-time-period__end-date">
                                                                <legend class="pe-form-field__label Sans-15px-black-55 block">
                                                                    a
                                                                </legend>
                                                                <div class="pe-form-time-period__date-field ">
                                                                    <div class="pe-form-time-period__date-unit">
                                                                        <label for="position-end-month" class="visually-hidden">
                                                                            Mes de finalizaci�n
                                                                        </label>
                                                                        <span id="ember3920" class="ember-view">
                                                                            <select name="mes_fin" id="mes_fin" data-control-name="edit_position_end_date_month" class="ember-view">  <option value="0">Mes</option>
                                                                                <option value="Enero">enero</option>
                                                                                <option value="Febrero">febrero</option>
                                                                                <option value="Marzo">marzo</option>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                    <div class="pe-form-time-period__date-unit">
                                                                        <label for="position-end-year" class="visually-hidden">
                                                                            A�o de finalizaci�n
                                                                        </label>
                                                                        <span id="ember3934" class="ember-view">
                                                                            <select name="anio_fin" id="anio_fin" data-control-name="edit_position_end_date_year" class="ember-view">  <option value="0">A�o</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2016">2016</option>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div id="ember3996" class="ember-view">
                                                        </div>
                                                        <div id="ember3999" class="pe-form-time-period__ongoing-activity mt4 ember-view">
                                                            <input data-control-name="edit_position_date_toggle" tabindex="0" type="checkbox" id="position-currently-works-here" class="ember-checkbox state-checkbox visually-hidden ember-view">
                                                            <label for="position-currently-works-here">
                                                                <span class="label-text ml2">
                                                                    Trabajo aqu�� actualmente
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </fieldset>
                                                    <div id="ember4005" class="pe-form-field position-description floating-label ember-view"><!---->
                                                        <label for="position-description" class="pe-form-field__label label-text">
                                                            Descripci�n
                                                        </label>
                                                        <textarea name="descripcion" id="descripcion" value="" class="ember-text-area pe-form-field__textarea ember-view">
                                                        </textarea>
                                                    </div>
                                                </div>

                                                <footer class="pe-s-form__footer pe-form-footer">
                                                    <div class="pe-form-footer__actions">
													<button id="actualizar" name="actualizar" type="submit" onclick="valor('1')" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">Actualizar</button>
													<button id="borrar" name="borrar" type="submit" onclick="valor('2')" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">Borrar</button>
													<input id="metodo" name="metodo" value="" type="hidden"/>
                                                    </div>
                                                </footer>
                                            </div>
                                        </form>
										
                                        <div id="ember4045" class="ember-view">
                                            <div id="ember4046" class="ember-view">
                                            </div>
                                        </div>
                                        <div id="ember4047" class="ember-view">
                                            <div id="ember4048" class="ember-view">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="ember4049" class="carousel-item focused-easeInOut-motion carousel-item--pop visibility-hidden ember-view">
                                </div>
                            </div>
                        </div>
                        <div id="ember4050" class="ember-view">
                            <div id="ember4051" class="ember-view">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-wormhole-overlay overlay-actions-are-disabled" data-ember-action="" data-ember-action-4052="4052">
            </div>
        </div>
        <!-- fin experiencia  -->

		
		<!-- Inicio educacion 2-->
        <div id="ember7" class="modal-wormhole visible pe-s-modal shared-modal-container pe-modal ember-view">
            <div aria-labelledby="ember4627-modal-label" role="dialog" tabindex="-1" class="modal-wormhole-content">
                <div class="modal-content-wrapper">        
                    <h2 id="ember4627-modal-label" class="visually-hidden">Editar formulario</h2>
                    <div id="ember4631" class="carousel-form ember-view">
                        <div id="ember4645" class="breadcrumb-carousel ember-view">
                            <div id="ember4646" class="carousel-header ember-view">
                                <div id="ember4647" class="carousel-header__item Sans-21px-black-85 carousel-header__item--show ember-view">
                                    <div id="carousel-header-item-main" class="carousel-header__item-content">
                                        Educaci�n
                                    </div>
                                </div>
                                <div id="ember4648" class="carousel-header__item Sans-21px-black-85 carousel-header__item--hide ember-view">
                                    <div id="carousel-header-item-edit-treasury" class="carousel-header__item-content">
                                        Editar contenido multimedia
                                    </div>
                                </div>    
                                <div id="ember4649" class="ember-view">
                                    <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-4650="4650">
                                        <span class="svg-icon-wrap" onclick="document.getElementById('ember7').style.display = 'none'">
                                            <span class="visually-hidden">
                                                Cancelar
                                            </span>
                                            <li-icon aria-hidden="true" type="cancel-icon">
                                                <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                <g class="large-icon" style="fill: currentColor">
                                                <path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z">
                                                </path>
                                                </g>
                                                </svg>
                                            </li-icon>
                                        </span>
                                    </button>
                                </div>

                            </div>
                            <div id="ember4651" class="carousel-body ember-view">
                                <div id="ember4652" class="carousel-item focused-easeInOut-motion carousel-item--show ember-view">        
                                    <div id="ember4653" class="pe-education-form form-new ember-view">
                                        <form name="educacion2" method="post" action="EducacionPerfilController/updateEducacion.do" class="pe-form "  data-ember-action="" data-ember-action-4663="4663">
                                            <div class="pe-s-form__container">
                                                <header class="pe-s-form__header pe-form-header">
                                                    <div id="ember4664" class="ember-view">
                                                        <button type="button" data-control-name="cancel" class="form-back-action Sans-15px-black-55" data-ember-action="" data-ember-action-4665="4665">
                                                            <span class="svg-icon-wrap">
                                                                <span class="visually-hidden">
                                                                    Cancelar
                                                                </span>
                                                                <li-icon aria-hidden="true" type="cancel-icon">
                                                                    <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" class="artdeco-icon">
                                                                    <g class="large-icon" style="fill: currentColor">
                                                                    <path d="M20,5.32L13.32,12,20,18.68,18.66,20,12,13.33,5.34,20,4,18.68,10.68,12,4,5.32,5.32,4,12,10.69,18.68,4Z">
                                                                    </path>
                                                                    </g>
                                                                    </svg>
                                                                </li-icon>
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <h2 class="pe-form-header__title">
                                                        Educaci�n
                                                    </h2>
                                                    <button type="submit" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                        Guardar
                                                    </button>
                                                </header>
                                                <div class="pe-s-form__body pe-form-body">
                                                    <div class="pe-form-field edu-school-name has-logo  ">
               											<div class="pe-logo-container">
                                                            <img class="lazy-image pe-logo-container__img ghost-school loaded" alt="" height="24" width="24" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
                                                            <section id="ember4667" class="type-ahead-logo-field pe-form-field__type-ahead ember-view">
                                                                <div id="ember4671" class="ember-view">  
                                                                    <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                        <div role="search" class="type-ahead-input-container">
                                                                            <div class="type-ahead-input-wrapper">
                                                                                <div class="type-ahead-input">
                                                                                    <label for="a11y-ember4667" class="label-text">
                                                                                        Universidad
                                                                                    </label>
                                                                                    <select name="universidad_update" id="universidad_update" onchange="ShowSelectedEducacion();" data-control-name="edit_position_start_date_month" class="ember-view">  
																						<%
																						for (EducacionPerfilBean bb : listaEducacion) {
																							String education = bb.getEdu_id()+"-"+bb.getTitulacion()+"-"+bb.getDisciplina()+"-"+bb.getNota_media()+"-"+bb.getActividades()+"-"+bb.getAnio_inicio()+"-"+bb.getAnio_fin()+"-"+bb.getDescripcion();
																						%>
																							<option value="<%=education%>"><%=bb.getUniversidad()%></option>
																						<%
																						z++;
																						}%>
																					</select>
																					<input id="edu_id" name="edu_id" type="hidden" value=""></input>
                                                                                    <div class="type-ahead-input-icons">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div aria-live="polite" class="live-region visually-hidden">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                        </div>
                                                        <div id="ember4673" class="ember-view">
                                                        </div>
                                                    </div>
                                                    <div class="pe-form-field edu-degree-name  ">
                                                        <section id="ember4674" class="pe-form-field__type-ahead ember-view">
                                                            <div id="ember4678" class="ember-view">  
                                                                <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                   													<div role="search" class="type-ahead-input-container">
                                                                        <div class="type-ahead-input-wrapper">
                                                                            <div class="type-ahead-input">
                                                                                <label for="a11y-ember4674" class="label-text">
                                                                                    Titulaci�n
                                                                                </label>
                                                                                <input aria-autocomplete="list" type="text" value="" name="titulacion" autocomplete="off" spellcheck="false" placeholder="" autocorrect="off" autocapitalize="off" id="titulacion" role="combobox" class="ember-text-field ember-view">
                                                                                <div class="type-ahead-input-icons">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div aria-live="polite" class="live-region visually-hidden">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <div id="ember4680" class="ember-view">
                                                        </div>
                                                    </div>
                                                    <div class="pe-form-field edu-field-of-study  ">
                                                        <section id="ember4681" class="pe-form-field__type-ahead ember-view">
                                                            <div id="ember4685" class="ember-view">  
                                                                <div class="type-ahead-wrapper type-ahead-theme-secondary pe-typeahead">
                                                                    <div role="search" class="type-ahead-input-container">
                                                                        <div class="type-ahead-input-wrapper">
                                                                            <div class="type-ahead-input">
                                                                                <label for="a11y-ember4681" class="label-text">
                                                                                    Disciplina acad�mica
                                                                                </label>
                                                                                <input aria-autocomplete="list" type="text" value="" name="disciplina" autocomplete="off" spellcheck="false" placeholder="" autocorrect="off" autocapitalize="off" id="disciplina" role="combobox" class="ember-text-field ember-view">
                                                                                <div class="type-ahead-input-icons">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div aria-live="polite" class="live-region visually-hidden">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <div id="ember4687" class="ember-view">
                                                        </div>
                                                    </div>
                                                    <div class="pe-form-field floating-label  ">
                                                        <label for="edu-grade" class="pe-form-field__label label-text">
                                                            Nota
                                                        </label>
                                                        <input type="text" maxlength="80" value="" id="nota_media" name="nota_media" class="ember-text-field pe-form-field__text-input ember-view">

                                                    </div>
                                                    <div class="pe-form-field pe-education-form__activities-field floating-label  ">
                                                        <label for="edu-activities-societies" class="pe-form-field__label label-text">
                                                            Actividades y grupos
                                                        </label>
                                                        <textarea id="actividades" name="actividades" value="" class="pe-education-form__activities-text ember-text-area pe-form-field__textarea ember-view"></textarea>
                                                        <div class="pe-form-field__aux">
                                                            <p class="pe-form-field__help-text Sans-13px-black-55-italic">
                                                                Ejemplos: equipo de f�tbol, tuna, coro
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <fieldset class="pe-form-field pe-form-time-period ">
                                                        <legend class="pe-form-field__label Sans-13px-black-55 block">
                                                            Periodo de tiempo
                                                        </legend>
                                                        <div class="pe-form-time-period__container">
                                                            <div class="pe-form-time-period__start-date">
                                                                <label for="pe-education-form__start-year" class="pe-form-field__label">
                                                                    Desde el a�o
                                                                </label>
                                                                <span id="ember4694" class="ember-view">
                                                                    <select id="anio_inicio" name="anio_inicio" data-control-name="edit_education_start_year" class="pe-education-form__start-year ember-view">  
																		<option value="">A�o</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2015">2015</option>
                                                                    </select>
                                                                </span>
                                                            </div>

                                                            <div class="pe-form-time-period__end-date">
                                                                <label for="pe-education-form__end-year" class="pe-form-field__label">
                                                                    Hasta el a�o (o previsto)
                                                                </label>
                                                                <span id="ember4756" class="ember-view">
                                                                    <select id="anio_fin" name="anio_fin" data-control-name="edit_education_end_year" class="pe-education-form__end-year ember-view">  
																		<option value="">A�o</option>
                                                                        <option value="2024">2024</option>
                                                                        <option value="2023">2023</option>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div id="ember4818" class="ember-view">
                                                        </div>
                                                    </fieldset>

                                                    <div class="pe-form-field edu-description floating-label  ">
                                                        <label for="edu-description" class="pe-form-field__label label-text">
                                                            Descripci�n
                                                        </label>
                                                        <textarea name="descripcion" id="descripcion" value="" class="ember-text-area pe-form-field__textarea ember-view">
                                                        </textarea>
                                                    </div>
 
                                                </div>
                                                <footer class="pe-s-form__footer pe-form-footer">
                                                    <div class="pe-form-footer__actions">
                                                        <button type="submit" onclick="valor2('1')" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                            Actualizar
                                                        </button>
														<button type="submit" onclick="valor2('2')" class="pe-form-footer__action--submit form-submit-action Sans-15px-white-100">
                                                            Borrar
                                                        </button>
														<input id="metodo2" name="metodo2" value="" type="hidden"/>
                                                    </div>
                                                </footer>
                                            </div>
                                        </form>
                                        <div id="ember4825" class="ember-view">
                                            <div id="ember4826" class="ember-view">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="ember4827" class="carousel-item focused-easeInOut-motion carousel-item--pop visibility-hidden ember-view"><!---->
                                </div>
                            </div>
                        </div>
                        <div id="ember4828" class="ember-view">
                            <div id="ember4829" class="ember-view">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-wormhole-overlay overlay-actions-are-disabled" data-ember-action="" data-ember-action-4830="4830">
            </div>
        </div>
        <!-- fin educacion -->	
		
		
		<script type="text/javascript">
function valor2(val){
	document.educacion2.metodo2.value = val;
	console.log(val);
	return true;
}
		
function valor(val){
	document.experiencia2.metodo.value = val;
	console.log(val);
	return true;
}
		
function ShowSelectedExperiencia()
{
/* Para obtener el valor */
var cod = document.getElementById("cargo_update").value;
var res = cod.split("-");
var exp_id = res[0];
var empresa = res[2];
var mes_inicio = res[3];
var anio_inicio = res[4];
var mes_fin = res[5];
var anio_fin = res[6];
var lugar = res[7];
var descripcion = res[8];


document.experiencia2.exp_id.value = exp_id;
document.experiencia2.empresa.value = empresa;
document.experiencia2.mes_inicio.value = mes_inicio;
document.experiencia2.anio_inicio.value = anio_inicio;
document.experiencia2.mes_fin.value = mes_fin;
document.experiencia2.anio_fin.value = anio_fin;
document.experiencia2.ubicacion.value = lugar;
document.experiencia2.descripcion.value = descripcion;
console.log(cod)
}
</script>
		
<script type="text/javascript">
function ShowSelectedEducacion()
{
/* Para obtener el valor */
var cod = document.getElementById("universidad_update").value;


var res = cod.split("-");
var edu_id = res[0];
var titulacion = res[1];
var disciplina = res[2];
var nota_media = res[3];
var actividades = res[4];
var anio_inicio = res[5];
var anio_fin = res[6];
var descripcion = res[7];

document.educacion2.edu_id.value = edu_id;
document.educacion2.titulacion.value = titulacion;
document.educacion2.disciplina.value = disciplina;
document.educacion2.nota_media.value = nota_media;
document.educacion2.actividades.value = actividades;
document.educacion2.anio_inicio.value = anio_inicio;
document.educacion2.anio_fin.value = anio_fin;
document.educacion2.descripcion.value = descripcion;
console.log(cod);
}
</script>
		
		
		

        <!-- javascripts -->
        <script src="js/social/jquery.js"></script>
        <script src="js/social/jquery-ui-1.10.4.min.js"></script>
        <script src="js/social/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/social/jquery-ui-1.9.2.custom.min.js"></script>
        <!-- bootstrap -->
        <script src="js/social/bootstrap.min.js"></script>
        <!-- nice scroll -->
        <script src="js/social/jquery.scrollTo.min.js"></script>
        <script src="js/social/jquery.nicescroll.js" type="text/javascript"></script>
        <!-- charts scripts -->
        <script src="assets/jquery-knob/js/jquery.knob.js"></script>
        <script src="js/social/jquery.sparkline.js" type="text/javascript"></script>
        <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
        <script src="js/social/owl.carousel.js" ></script>
        <!-- jQuery full calendar -->
        <<script src="js/social/fullcalendar.min.js"></script>
        <script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
        <!--script for this page only-->
        <script src="js/social/calendar-custom.js"></script>
        <script src="js/social/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script src="js/social/jquery.customSelect.min.js" ></script>
        <script src="assets/chart-master/Chart.js"></script>

        <!--custome script for all page-->
        <script src="js/social/scripts.js"></script>
        <!-- custom script for this page-->
        <script src="js/social/sparkline-chart.js"></script>
        <script src="js/social/easy-pie-chart.js"></script>
        <script src="js/social/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="js/social/jquery-jvectormap-world-mill-en.js"></script>
        <script src="js/social/xcharts.min.js"></script>
        <script src="js/social/jquery.autosize.min.js"></script>
        <script src="js/social/jquery.placeholder.min.js"></script>
        <script src="js/social/gdp-data.js"></script>  
        <script src="js/social/morris.min.js"></script>
        <script src="js/social/sparklines.js"></script>    
        <script src="js/social/charts.js"></script>
        <script src="js/social/jquery.slimscroll.min.js"></script>

        <script>

                                                                    //knob
                                                                    $(function () {
                                                                        $(".knob").knob({
                                                                            'draw': function () {
                                                                                $(this.i).val(this.cv + '%')
                                                                            }
                                                                        })
                                                                    });

                                                                    //carousel
                                                                    $(document).ready(function () {
                                                                        $("#owl-slider").owlCarousel({
                                                                            navigation: true,
                                                                            slideSpeed: 300,
                                                                            paginationSpeed: 400,
                                                                            singleItem: true

                                                                        });
                                                                    });

                                                                    //custom select box

                                                                    $(function () {
                                                                        $('select.styled').customSelect();
                                                                    });

                                                                    /* ---------- Map ---------- */
                                                                    $(function () {
                                                                        $('#map').vectorMap({
                                                                            map: 'world_mill_en',
                                                                            series: {
                                                                                regions: [{
                                                                                        values: gdpData,
                                                                                        scale: ['#000', '#000'],
                                                                                        normalizeFunction: 'polynomial'
                                                                                    }]
                                                                            },
                                                                            backgroundColor: '#eef3f7',
                                                                            onLabelShow: function (e, el, code) {
                                                                                el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                                                                            }
                                                                        });
                                                                    });

        </script>

    </body>


</html>
