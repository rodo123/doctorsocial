<!DOCTYPE html>
	
	<html class="no-js">
	
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RED MEDICA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="EDoctor.com" />
        <meta name="keywords" content="edoctor, medicos, doctores, html5, css3, mobile first, responsive" />
        <meta name="author" content="NyR" />


        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">

        <!-- Animate.css -->
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/hover.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="css/icomoon.css">
        <!-- Simple Line Icons -->
        <link rel="stylesheet" href="css/simple-line-icons.css">
        <link rel="stylesheet" href="css/lineicons.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <!-- Style -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/flexslider.css"/>
        <link rel="stylesheet" href="css/demo.css" type="text/css" media="screen" />
        <!-- Slick -->
        <link rel="stylesheet" type="text/css" href="css/slider.css"/>
        <link rel="stylesheet" type="text/css" href="js/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="js/slick/slick-theme.css"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <!-- Modernizr JS -->
        <script src="js/modernizr-2.6.2.min.js"></script>
        <script src="js/modernizr.js"></script>


    </head>
	
    <body>
        
		<header role="banner" id="fh5co-header">
            <div class="fluid-container">
				<nav class="navbar navbar-default">
					<div id="navbar" class="navbar-collapse collapse" style="background: #591852; height:50px;"> 
						<ul class="nav navbar-nav navbar-right">			
							<li class="call-to-action"><a class="sign-up" onclick="javascript:regirectToServlet('SpecialityControlle')"><span>Registrarse</span></a></li>
							<li class="call-to-action"><a class="log-in" onclick="javascript:regirectToPage('login')"><span>Ingresar</span></a></li>				
						</ul>                           
					</div>
					<div class="navbar-header">
						<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
						<a class="navbar-brand" atastellar-background-ratio="0.5" href="index.jsp"><img src="images/logo.png" style="width: 170px;"/></a> 
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="active"><a href="#" data-nav-section="home"><span>Home</span></a></li>
							<li><a href="#" data-nav-section="services"><span>Nosotros</span></a></li>
							<li><a href="#" data-nav-section="especialidades"><span>Especialidades</span></a></li>
							<li><a href="#" data-nav-section="explore"><span>Miembros</span></a></li>
							<li><a href="#" data-nav-section="testimony"><span>Testimonios</span></a></li>
							<li><a href="#" data-nav-section="pricing"><span>Membres�a</span></a></li>
							<li><a href="#" data-nav-section="team"><span>Equipo M�dico</span></a></li>
							<li><a href="#" data-nav-section="blog"><span>Empleos</span></a></li>
							<li><a href="#" data-nav-section="faq"><span>Contactanos</span></a></li>
						</ul>
					</div>	
				</nav>
            </div>
        </header>

        <script type="text/javascript">

            function regirectToPage(page) {
                window.location.href = page + ".jsp"
            }

            function regirectToServlet(page) {
                window.location.href = page
            }
        </script>
        
		<style>
            .flexslider {
              margin-bottom: 10px;
            }

            .flex-control-nav {
              position: relative;
              bottom: auto;
            }

            .custom-navigation {
              display: table;
			width: 100%;
			/* table-layout: fixed; */
			z-index: 4;
			position: absolute;
			margin-top: -50px;
					}

            .custom-navigation > * {
              display: table-cell;
            }

            .custom-navigation > a {
              width: 50px;
            }

            .custom-navigation .flex-next {
              text-align: right;
            }
          </style>


        <section id="fh5co-home"  data-section="home"  data-stellar-background-ratio="0.5">
            <div class="flexslider">
                <div class="text-inner to-animate">
                    <div class="row">                       
                        <div class="col-md-10 col-md-offset-1 text-center" style="padding-left: 0px;  padding-right: 0px; ">
                            <h1 style="font-family: Montserrat, sans-serif !important, color:white;"><strong>La mayor red profesional de Medicos</strong></h1>
                            <br>
                            <div class="call-to-action">
								<a href="BusquedaDoctorControlle" class="demo  hvr-bob"><img src="images/surgeon.png"><br>Conoce a t� Medico</a>
								<a href="empleos.jsp" class="download  hvr-bob"><img src="images/first_aid_kit.png"><br>Servicios M�dicos</a>
								<a href="BusquedaEmpleosController" class="test  test hvr-bob"><img src="images/insurance_billing.png"><br>Oportunidades M�dicas</a>
							</div>
                        </div>
                    </div>
                </div>
				<ul class="slides">
					<li >
						<img src="images/image1.jpg"  />
					</li>
					<li>
						<img src="images/image1.jpg" />
					</li>
					<li>
						<img src="images/image1.jpg" />
					</li>
				</ul>
				<div class="gradient">
				</div>
			</div>
            <div class="custom-navigation">
				<div class="custom-controls-container">
				</div>
            </div>
		</section>
        

        <section id="fh5co-services" data-section="services">
            <div class="fh5co-services">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 section-heading text-center">
                            <h2 class="to-animate">
								<span>Nosotros</span>
							</h2>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 subtext">
                                    <h3 class="to-animate">
									Somos un grupo de profesionales que deseamos compartir experiencias con oportunidades acad�micas y laborales con la comunidad m�dica peruana e internacional. Deseamos siempre en nuestro coraz�n, brindar cada d�a una medicina de calidad con mejora continua y contribuir en los pr�ximos a�os con la verdadera mejora de los servicios de salud del  Per�.
									</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
					    <div class="col-md-3 text-center">
                            <div class="box-services">
                                <div class="icon to-animate">
                                    <span>
										<img src="images/wheelchair.png" class="icon-speciality">
									</span>
                                </div>
                                <div class="fh5co-post to-animate">
                                    <h3>Tratamiento General</h3>
                                </div>
                            </div>
                            <div class="box-services">
                                <div class="icon to-animate">
									<span>
										<img src="images/occupational_therapy.png" class="icon-speciality">
									</span>
                                </div>
                                <div class="fh5co-post to-animate">
                                    <h3>Dinamismo y Rapides</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="box-services">
                                <div class="icon to-animate">
									<span>
										<img src="images/achievement.png" class="icon-speciality-small">
									</span>
                                </div>
                                <div class="fh5co-post to-animate">
                                    <h3>Reconocimientos</h3>
                                </div>
                            </div>
                            <div class="box-services">
                                <div class="icon to-animate">
									<span>
										<img src="images/quality.png" class="icon-speciality">
									</span>
                                </div>
                                <div class="fh5co-post to-animate">
                                    <h3>Calidad</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="box-services">
                                <div class="icon to-animate">
									<span>
										<img src="images/testimonials.png" class="icon-speciality">
									</span>
                                </div>
                                <div class="fh5co-post to-animate">
                                    <h3>Satisfacci�n de los Clientes</h3>
                                </div>
                            </div>
                            <div class="box-services">
                                <div class="icon to-animate">
									<span>
										<img src="images/allergen.png" class="icon-speciality">
									</span>
                                </div>
                                <div class="fh5co-post to-animate">
                                    <h3>Entorno Integrado</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="box-services">
                                <div class="icon to-animate">
									<span>
										<img src="images/back_up.png" class="icon-speciality">
									</span>
                                </div>
                                <div class="fh5co-post to-animate">
                                    <h3>Ayuda &amp; Soporte</h3>
                                </div>
                            </div>
                            <div class="box-services">
                                <div class="icon to-animate">
									<span>
										<img src="images/privacy.png" class="icon-speciality">
									</span>
                                </div>
                                <div class="fh5co-post to-animate">
                                    <h3>Privacidad de Cuentas</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

		
		 <section id="fh5co-especialidades" data-section="especialidades">
			<div class="especialidades-overlay">
			</div>
			<div class="container">
				<div class="row">
					<div class="row">
						<div class="col-md-12 section-heading text-center">
							<h2 class="to-animate fadeInUp animated"><span>Especialidades</span></h2>
							<div class="row">
								<div class="col-md-8 col-md-offset-2 subtext">
									<h3 class="to-animate fadeInUp animated">Nuestras especialidades: </h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row row-bottom-padded-sm">
					<div class="col-md-2 col-md-offset-1 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-1.jpg" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://sopecard.org/portal/"><h4>Cardiolog�a</h4></a>
							
							<!-- <span>Web</span> -->
							</div>
						</a>
					</div>
					<div class="col-md-2 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-2.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://www.spotrauma.org/"><h4>Traumatolog�a</h4></a>
							<!-- <span>Web</span> -->
							</div>
						</a>
					</div>
					<div class="col-md-2 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-3.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://www.pediatriaperu.org/"><h4>Pediatria</h4></a>
							<!-- <span>UI/UX</span> -->
							</div>
						</a>
					</div>
					<div class="col-md-2 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-4.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="https://sociedadperuanadeoftalmologia.pe/"><h4>Oftamolog�a</h4></a>
							
							<!-- <span>UI/UX</span> -->
							</div>
						</a>
					</div>
					<div class="col-md-2 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-5.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://www.spog.org.pe/web/"><h4>Ginecolog�a</h4></a>
							<!-- <span>UI/UX</span> -->
							</div>
						</a>
					</div>
				</div>
				<div class="row row-bottom-padded-sm">
					<div class="col-md-2 col-md-offset-1 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-6.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://www.cop.org.pe/"><h4>Odontolog�a</h4></a>
							<!-- <span>Web</span> -->
							</div>
						</a>
					</div>
					<div class="col-md-2 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-7.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://www.socgastro.org.pe/"><h4>Gastroenterolog�a</h4></a>
							<!-- <span>Web</span> -->
							</div>
						</a>
					</div>
					<div class="col-md-2 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-8.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://www.sporlcf.org.pe/"><h4>Otorrinolaringolog�a</h4></a>
							
							<!-- <span>UI/UX</span> -->
							</div>
						</a>
					</div>
					<div class="col-md-2 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-9.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://www.dermatologia.pe/"><h4>Dermatolog�a</h4></a>
							
							<!-- <span>UI/UX</span> -->
							</div>
						</a>
					</div>
					<div class="col-md-2 col-sm-6 col-xxs-12 text-center">
						<a>
							<img src="images/project-10.jpg" style="height: 150px" alt="Image" class="img-responsive">
							<div class="fh5co-text">
							<a href="http://cnp.org.pe/"><h4>Nutrici�n</h4></a>
						
							<!-- <span>UI/UX</span> -->
							</div>
						</a>
					</div>
				</div>
            </div>
		</section>
		
		
        <section id="fh5co-explore" data-section="explore">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 section-heading text-center">
                        <h2 class="to-animate"><span>Miembros Destacados</span></h2>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 subtext to-animate">
                                <h3>La mas grande comunidad de doctores y personas relacionadas al campo medico.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fh5co-project">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <div class="project-grid to-animate-2" style="background-image:  url(img/fundadores/david_chuquipoma.jpg);">
                                <div class="desc">
                                    <h3><a href="">David Chuquipoma Pacheco</a></h3>
                                    <span>Doctor - M�dico Forense</span>
                                    <p class="love"><a href="https://www.facebook.com/david.chuquipomapacheco.1"><i class="icon-download"></i></a></p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="project-grid to-animate-2" style="background-image:  url(images/doctor2.png);">
                                <div class="desc">
                                    <h3><a href="#">Arias Avalos Clider Ulises</a></h3>
                                    <span>Doctor - Pediatra</span>

                                    <p class="love"><a href="#"><i class="icon-download"></i></a></p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">

                            <div class="project-grid to-animate-2" style="background-image:  url(images/doctor3.jpg);">
                                <div class="desc">
                                    <h3><a href="#">Barrantes P�rez Alejandro Jos�</a></h3>
                                    <span>Doctor - M�dico Cirujano</span>

                                    <p class="love"><a href="#"><i class="icon-download"></i></a></p>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
            <div id="fh5co-counter-section" class="fh5co-counters"  style="background-color:rgba(89, 24, 84, .85);" data-stellar-background-ratio="0.5">
                <div class="gradient">
				</div>
				<div class="container" >
                    <div class="row">
                        <div class="col-md-12 section-heading text-center">
                            <h2 class="to-animate" style="color: white;">Nuestras Estadisticas</h2>
                        </div>
                    </div>
                    <div class="row to-animate">
                        <div class="col-md-3 text-center">
                            <span class="icon"><i class="icon-th" style="font-size: 38px; color:white;"></i></span>
                            <span class="fh5co-counter js-counter" data-from="0" data-to="3452" data-speed="5000" data-refresh-interval="50" style="color: white;"></span>
                            <span class="fh5co-counter-label" style="color: white;">Miembros</span>
                        </div>
                        <div class="col-md-3 text-center">
                            <span class="icon"><i class="icon-hospital-o" style="font-size: 38px; color:white;"></i></span>
                            <span class="fh5co-counter js-counter" data-from="0" data-to="234" data-speed="5000" data-refresh-interval="50" style="color: white;"></span>
                            <span class="fh5co-counter-label" style="color: white;">Hospitales</span>
                        </div>
                        <div class="col-md-3 text-center">
                            <span class="icon"><i class="icon-ambulance" style="font-size: 38px; color:white;"></i></span>
                            <span class="fh5co-counter js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50" style="color: white;"></span>
                            <span class="fh5co-counter-label" style="color: white;">Clinicas</span>
                        </div>
                        <div class="col-md-3 text-center">
                            <span class="icon"><i class="icon-star" style="font-size: 38px; color:white;"></i></span>
                            <span class="fh5co-counter js-counter" data-from="0" data-to="2" data-speed="5000" data-refresh-interval="50" style="color: white;"></span>
                            <span class="fh5co-counter-label" style="color: white;">Logros</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="fh5co-testimony" class="fh5co-bg-color" data-section="testimony">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 section-heading text-center">
                        <h2 class="to-animate"><span>Nuestros Clientes Felices</span></h2>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 subtext to-animate">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 to-animate">
                        <div class="wrap-testimony">
                            <div class="owl-carousel-fullwidth">
                                <div class="item">
                                    <div class="testimony-slide active text-center">
                                        <figure>
                                            <img src="img/perfiles/rod123123@hotmail.com/rod123123@hotmail.com.jpg" alt="user">
                                        </figure>
                                        <span>Rodolfo Zevallos Salazar</span>
                                        <blockquote>
                                            <p>&ldquo;Es una de las mejores p�ginas para poder encontrar todo lo relacionado a la comunidad de medicos del Per�.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimony-slide active text-center">
                                        <figure>
                                            <img src="img/comentarios/nelsi.jpg" alt="user">
                                        </figure>
                                        <span>Nelsi Melgarejo Vergara</span>
                                        <blockquote>
                                            <p>&ldquo;Me gusta la p�gina web porque puedo encontrar el perfil del doctor y con eso saber si es confiable.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimony-slide active text-center">
                                        <figure>
                                            <img src="img/comentarios/gerson.jpg" alt="user">
                                        </figure>
                                        <span>Gerson Gutierrez Repetto</span>
                                        <blockquote>
                                            <p>&ldquo;Me da mucho gusto que existan p�gina como esta, ya que ayudan a las personas a encontrar a un buen doctor.&rdquo;</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="fh5co-pricing" data-section="pricing">
            <div class="fh5co-pricing">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 section-heading text-center">
                            <h2 class="to-animate"><span>Planes de Membres�a</span></h2>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 subtext">
                                    <h3 class="to-animate">Los distintos tipos de membres�a provee de mejoras al servicio. </h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="price-box to-animate">
                                <h2 class="pricing-plan">B�sico</h2>
                                <div class="price"><sup class="currency">S/</sup>10<small>/mens</small></div>
                                <p>Membresia B�sica</p>
                                <hr>
                                <ul class="pricing-info">
                                    <li>10 Publicaciones</li>
                                    <li>30 Busquedas</li>
                                    <li>20 Emails</li>
                                </ul>
                                <p><a href="#" class="btn btn-primary">Leer m�s</a></p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="price-box to-animate">
                                <h2 class="pricing-plan">Regular</h2>
                                <div class="price"><sup class="currency">S/</sup>15<small>/mens</small></div>
                                <p>Membres�a Regular</p>
                                <hr>
                                <ul class="pricing-info">
                                    <li>20 Publicaciones</li>
                                    <li>50 Busquedas</li>
                                    <li>40 Emails</li>

                                </ul>
                                <p><a href="#" class="btn btn-primary">Leer m�s</a></p>
                            </div>
                        </div>
                        <div class="clearfix visible-sm-block"></div>
                        <div class="col-md-3 col-sm-6 to-animate">
                            <div class="price-box popular">
                                <div class="popular-text">Mejor opci�n</div>
                                <h2 class="pricing-plan">Plus</h2>
                                <div class="price"><sup class="currency">S/</sup>30<small>/mens</small></div>
                                <p>Membres�a Plus</p>
                                <hr>
                                <ul class="pricing-info">
                                    <li>Sin limites de Publicaciones</li>
                                    <li>Sin limites de Busquedas</li>
                                    <li>100 Emails</li>
                                </ul>
                                <p><a href="#" class="btn btn-primary">Leer m�s</a></p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="price-box to-animate">
                                <h2 class="pricing-plan">Empresas</h2>
                                <div class="price"><sup class="currency">S/</sup>60<small>/mens</small></div>
                                <p>Membres�a Empresarial</p>
                                <hr>
                                <ul class="pricing-info">
                                    <li>Sin limites de Publicaciones</li>
                                    <li>Sin limites de Busquedas</li>
                                    <li>Sin limites de Emails</li>
                                </ul>
                                <p><a href="#" class="btn btn-primary">Leer m�s</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 to-animate">
                            <p>Todos los paquetes de membres�a estan bajo condiciones.<a href="#">Leer m�s</a></p>
                        </div>
                    </div>

                </div>
            </div>
        </section>	


        <section id="fh5co-team" class="fh5co-bg-color" data-section="team">
            <div class="fh5co-team">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 section-heading text-center">
                            <h2 class="to-animate"><span>Equipo de Trabajo</span></h2>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 subtext">
                                    <h3 class="to-animate">Doctores peruanos que sue�an con una comunidad m�s unida. </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="team-box text-center to-animate-2">
                                <div class="user"><img class="img-reponsive" src="img/fundadores/david_chuquipoma.jpg" alt="Roger Garfield"></div>
                                <h3>Chuquipoma Pacheco David</h3>
                                <span class="position">Co-Fundador</span>
                                <p>Medico Forense y Legal, egresado de la Universidad Nacional Mayor de San Marcos, con 25 a�os de experiencia en el campo.</p>
                                <ul class="social-media">
                                    <li><a href="https://www.facebook.com/david.chuquipomapacheco.1" class="facebook"><i class="icon-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="icon-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="team-box text-center to-animate-2">
                                <div class="user"><img class="img-reponsive" src="img/fundadores/rodolfo.jpg" alt="Roger Garfield"></div>
                                <h3>Rodolfo Zevallos Salazar</h3>
                                <span class="position">Co-Fundador</span>
                                <p>Investigador dedicado a la investigaci�n cient�fica y al desarrollo de software, fomentado la investigaci�n para el cambio social del Per�.</p>
                                <ul class="social-media">
                                    <li><a href="https://www.facebook.com/rodolfo.zevallos" class="facebook"><i class="icon-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/rjzevallos?lang=es" class="twitter"><i class="icon-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="team-box text-center to-animate-2">
                                <div class="user"><img class="img-reponsive" src="img/fundadores/jessica.jpg" alt="Roger Garfield"></div>
                                <h3>Jessica Meza Rojas</h3>
                                <span class="position">Co-Fundador</span>
                                <p>Bachiller de la carrera de ingenier�a de sistemas altamenta creatia, con iniciativa y puntualidad .</p>
                                <ul class="social-media">
                                    <li><a href="https://www.facebook.com/profile.php?id=100004306068489" class="facebook"><i class="icon-facebook"></i></a></li>
                                    <li><a href="" class="twitter"><i class="icon-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </section>

		
        <section id="fh5co-blog" data-section="blog">
            <div class="fh5co-blog">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 section-heading text-center">
                            <h2 class="to-animate"><span>Encuentra un Empleo</span></h2>
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 subtext">
                                    <h3 class="to-animate">Hacemos que los m�dicos peruanos puedan encontrar de una forma m�s sencilla y r�pida. </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 to-animate blog">
                            <div class="blog-grid" style="background-image: url(images/job1.jpg);">
                                <div class="date">
                                    <span>03</span>
                                    <small>Aug</small>
                                </div>
                            </div>
                            <a href="#" class="desc">
                                <div class="desc-grid">
                                    <h3>Anuncio de Empleo</h3>
                                </div>
                                <div class="reading">
                                    <i class="icon-arrow-right2"></i>
                                </div>
                            </a>
                        </div>

                        <div class="col-md-6 to-animate blog">
                            <div class="blog-grid" style="background-image: url(images/job2.jpg);">
                                <div class="date">
                                    <span>04</span>
                                    <small>Aug</small>
                                </div>
                            </div>
                            <a href="#" class="desc">
                                <div class="desc-grid">
                                    <h3>Anuncio de Empleo</h3>
                                </div>
                                <div class="reading">
                                    <i class="icon-arrow-right2"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="fh5co-faq" class="fh5co-bg-color" style="padding: 0em !important; background-image: url(images/img_bg_2.jpg);" data-section="faq">
            <div class="overlay">
			</div>
            <div id="fh5co-footer" style="padding-bottom: 0em !important" role="contentinfo">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 to-animate">
                            <h3 class="section-title text-center">Red M�dica de Calidad</h3>
                            <p style="text-align:justify;">Deseamos siempre en nuestro coraz�n, brindar cada d�a una medicina de calidad con mejora continua y contribuir en los pr�ximos a�os con la verdadera mejora de los servicios de salud del Per�.</p>
                            <ul class="social-media text-center">
                                <li><a href="#" class="facebook"><i class="icon-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="icon-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 to-animate text-center">
                            <h3 class="section-title ">Conectate con Nosotros</h3>
                            <div style=" ">
                                <ul class="contact-info ">
                                    <li style=" padding-left: 0px;  margin-left: -2em !important;"><i class="icon-map-marker"></i>RED MEDICA DE CALIDAD</li>
                                    <li style=" padding-left: 0px;  margin-left: -2em;"><i class="icon-phone"></i>+ 1235 2355 98</li>
                                    <li style=" padding-left: 0px;  margin-left: -2em;"><i class="icon-envelope"></i><a href="#">red_medica_calidad@gmail.com</a></li>
                                    <li style=" padding-left: 0px;  margin-left: -2em;"><i class="icon-globe2"></i><a href="#">www.redmedicadecalidad.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 to-animate text-center">
                            <h3 class="section-title text-center">Env�anos un Correo</h3>
                            <form name="contacto" method="post" action="ContactoController/insertContacto.do" class="contact-form text-center">
                                <div class="form-group">
                                    <label for="name" class="sr-only">Nombre</label>
                                    <input type="name" class="form-control" id="nombre" name="nombre" value="" placeholder="Nombre">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="sr-only">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="message" class="sr-only">Mensaje</label>
                                    <textarea class="form-control" id="mensaje" name="mensaje" value="" rows="7" placeholder="Mensaje"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" id="btn-submit" class="btn btn-send-message btn-md" value="Enviar Mensaje">
                                </div>
                            </form>
                        </div>
                    </div>
                    <br>
					<br>
                </div>
            </div>
        </section>

                        
        <div class="row copyright" style="background-color:rgba(0, 0, 0, 0.8);" >
			<div class="col-md-12 text-center">
				<p style="color: #fbb001; margin: 0.5em 0; ">
					<strong>
						<small class="block">
							� 2017  All Rights Reserved.
						</small>
					</strong>
				</p>
			</div>
		</div>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script> 
        <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="js/slick/slick.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.slider').slick({
                    dots: true,
                    speed: 1000,
                    autoplay: true
                });
            });
        </script>

        <!-- jQuery Easing -->
        <script src="js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Stellar Parallax -->
        <script src="js/jquery.stellar.min.js"></script>
        <!-- Owl Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- Counters -->
        <script src="js/jquery.countTo.js"></script>
        <!-- Google Map -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
        <script src="js/google_map.js"></script>
        <!-- Main JS (Do not remove) -->
        <script src="js/main.js"></script>
        <script src="js/jquery.style.switcher.js"></script>
		<script defer src="js/jquery.flexslider.js"></script>

		<script defer src="js/demo.js"></script>
		<script>
			$(function(){
				$('#colour-variations ul').styleSwitcher({
					defaultThemeId: 'theme-switch',
					hasPreview: false,
					cookie: {
						expires: 30,
						isManagingLoad: true
					}
				}); 
				$('.option-toggle').click(function() {
					$('#colour-variations').toggleClass('sleep');
				});
			});
		</script>
    
		<script type="text/javascript">
			$(function(){
			  SyntaxHighlighter.all();
			});
			$(window).load(function(){
			  $('.flexslider').flexslider({
				animation: "slide",
				controlsContainer: $(".custom-controls-container"),
				customDirectionNav: $(".custom-navigation a"),
				start: function(slider){
				  $('body').removeClass('loading');
				}
			  });
			});
		</script>

    </body>
</html>

