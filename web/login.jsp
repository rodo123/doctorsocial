<!DOCTYPE html>
<html class="no-js">
   
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>RED MEDICA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="EDoctor.com" />
      <meta name="keywords" content="edoctor, médicos, doctores, html5, css3, mobile first, responsive" />
      <meta name="author" content="NyR" />
      <!-- Facebook and Twitter integration -->
      <meta property="og:title" content=""/>
      <meta property="og:image" content=""/>
      <meta property="og:url" content=""/>
      <meta property="og:site_name" content=""/>
      <meta property="og:description" content=""/>
      <meta name="twitter:title" content="" />
      <meta name="twitter:image" content="" />
      <meta name="twitter:url" content="" />
      <meta name="twitter:card" content="" />
      <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
      <link rel="shortcut icon" href="favicon.ico">
      <!-- <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'> -->
      <!-- Animate.css -->
      <link rel="stylesheet" href="css/animate.css">
      <!-- Icomoon Icon Fonts-->
      <link rel="stylesheet" href="css/icomoon.css">
      <!-- Simple Line Icons -->
      <link rel="stylesheet" href="css/simple-line-icons.css">
      <!-- Bootstrap  -->
      <link rel="stylesheet" href="css/bootstrap.css">
      <!-- Owl Carousel  -->
      <link rel="stylesheet" href="css/owl.carousel.min.css">
      <link rel="stylesheet" href="css/owl.theme.default.min.css">
      <!-- Style -->
      <link rel="stylesheet" href="css/style.css">
      <!-- SweetAlert -->
      <script src="dist/sweetalert.min.js"></script>
      <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
      <!-- Modernizr JS -->
      <script src="js/modernizr-2.6.2.min.js"></script>
      <!-- FOR IE9 below -->
      <!--[if lt IE 9]>
      <script src="js/respond.min.js"></script>
      <![endif]-->
   </head>
    
	<body>
      <header role="banner" id="fh5co-header">
         <div class="fluid-container">
            <nav class="navbar navbar-default">
               <div class="navbar-header">
                  <!-- Mobile Toggle Menu Button -->
                  <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                  <a class="navbar-brand" atastellar-background-ratio="0.5" href="index.jsp"><img src="images/logo.png" style="width: 170px;"/></a> 
               </div>
               <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav navbar-right">
                  </ul>
               </div>
            </nav>
         </div>
      </header>
      <section id="fh5co-faq" class="fh5co-login" data-section="faq">
         <div id="" role="contentinfo">
            <div class="container">
               <div class="row backgroud-transparent-black-login">
                  <div class="">
                     <div class="col-md-5">
                        <div class="">
                           <h3 class="section-title">Bienvenidos a M�dicos Peruanos</h3>
                           <p>M�dicos Per� es el �nico medio especializado para la comunidad m�dica y cient�fica del pa�s. Es un portal de actualidad que agrupa a toda la comunidad de m�dicos con el fin de poder unirlos</p>
                        </div>
                     </div>
                     <div class="col-sm-1 middle-border"></div>
                     <div class="col-md-5">
                        <h3 class="section-title">Acceso al Sistema</h3>
                        <form name="login" method="get" action="UserController/loginAcces.do" class="contact-form" onsubmit="return valida(this)">
                           <div class="form-group">
                              <label for="name" class="sr-only">Correo</label>
                              <input  id ="usuario" class="form-control" type="text" placeholder="Correo" name="usuario" size="20"/>
                              <!-- <input type="name" class="form-control" id="name" placeholder="Usuario"> -->
                           </div>
                           <div class="form-group">
                              <label for="email" class="sr-only">Contrase�a</label>
                              <input id ="contrase�a" class="form-control" type="password" name="contrase�a" placeholder="Contrase�a" size="20"/>
                              <!-- <input type="email" class="form-control" id="email" placeholder="Contrase�a"> -->
                           </div>
                           <div class="form-group">
                              <input type="submit" id="btn-submit" class="btn btn-primary btn-send-message btn-md" value="INICIA SESI�N">
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      
	  <script type="text/javascript">
         function valida(f) {
           var ok = true;
           var msg = "Debes escribir algo en los campos:\n";
           if(f.elements["usuario"].value == "")
           {
             msg += "- Correo 1\n";
             ok = false;
           }
         
           if(f.elements["contrase�a"].value == "")
           {
             msg += "- Contrase�a 2\n";
             ok = false;
           }
         
           if(ok == false)
             alert(msg);
           return ok;
         }
      </script>  
      <!-- jQuery -->
      <script src="js/jquery.min.js"></script>
      <!-- jQuery Easing -->
      <script src="js/jquery.easing.1.3.js"></script>
      <!-- Bootstrap -->
      <script src="js/bootstrap.min.js"></script>
      <!-- Waypoints -->
      <script src="js/jquery.waypoints.min.js"></script>
      <!-- Stellar Parallax -->
      <script src="js/jquery.stellar.min.js"></script>
      <!-- Owl Carousel -->
      <script src="js/owl.carousel.min.js"></script>
      <!-- Counters -->
      <script src="js/jquery.countTo.js"></script>
      <!-- Google Map -->
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
      <script src="js/google_map.js"></script>
      <!-- Main JS (Do not remove) -->
      <script src="js/main.js"></script>
   </body>
</html>