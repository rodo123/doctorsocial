/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author rod12
 */
public class Mail {
    
    public void SendMail(String correoTo,String correoOf, String cargo) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
 
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("rjzevallos.salazar@gmail.com","rodyale123123123");
                    }
                });
 
        try {
 
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("rjzevallos.salazar@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(correoTo));
            message.setSubject("Postulación de trabajo: "+cargo);
            message.setText("Estimados \n Le enviamos el correo: "+correoOf+" de la persona que desea postular para este trabajo");
 
            Transport.send(message);
           
 
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
