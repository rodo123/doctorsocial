package edoctor.com.dao;

import edoctor.com.beans.JobBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Rodolfo Zevallos
 */
public class JobDAO {

    Conexion conexion;

    public JobDAO() {
        conexion = new Conexion();
    }

    public void insertJob(List<JobBean> p) throws SQLException {

        for (int i = 0; i < p.size(); i++) {

            String sql = "insert into edoctor.job (job_doc_id, job_en_id, job_position) VALUES (?,?,?);";

            Connection con = conexion.getConnection();
            PreparedStatement t;
            t = con.prepareStatement(sql);
            t.setInt(1, p.get(i).getJob_doc_id());
            t.setInt(2, p.get(i).getJob_en_id());
            t.setString(3, p.get(i).getJob_position());
            int rowsInserted = t.executeUpdate();
            if (rowsInserted > 0) {
                //System.out.println("Creaci�n de cuenta exitosa!!");
            }
        }
    }

    public int insertSingleJob(JobBean jobBean) throws SQLException {

        String sql = "insert into edoctor.job (job_doc_id, job_en_id, job_position) VALUES (?,?,?);";

        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setInt(1, jobBean.getJob_doc_id());
        t.setInt(2, jobBean.getJob_en_id());
        t.setString(3, jobBean.getJob_position());
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            //System.out.println("Creaci�n de cuenta exitosa!!");
        }
        int last_inserted_id = 0;
        ResultSet rs = t.getGeneratedKeys();
        if (rs.next()) {
            last_inserted_id = rs.getInt(1);
        }
        return last_inserted_id;
    }

}
