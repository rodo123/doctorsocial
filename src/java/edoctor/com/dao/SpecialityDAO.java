package edoctor.com.dao;

import edoctor.com.beans.SpecialityBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author winbugs
 */
public class SpecialityDAO {

    Conexion conexion;

    public SpecialityDAO() {

        conexion = new Conexion();
    }

    public List<SpecialityBean> selectSpeciality() {

        int idnodo = 0;
        Statement st;
        ResultSet rs;
        List<SpecialityBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select sp_id,sp_name from edoctor.speciality;";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            SpecialityBean p;

            while (rs.next()) {
                p = new SpecialityBean();
                p.setSp_name(rs.getString("sp_name"));
                p.setSp_id(rs.getInt("sp_id"));

                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return list;
    }

    public int selectSpecialityOne(String speciality) {

        int idnodo = 0;
        Statement st;
        ResultSet rs;

        Connection con = conexion.getConnection();
        String consultaSQL = "select sp_id from edoctor.speciality where sp_name = '" + speciality + "';";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);

            while (rs.next()) {
                idnodo = rs.getInt("sp_id");
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return idnodo;
    }
    
    public String selectSpecialityNameOne(int specialityID) {

        String name="";
        Statement st;
        ResultSet rs;

        Connection con = conexion.getConnection();
        String consultaSQL = "select sp_name from edoctor.speciality where sp_id = " + specialityID + ";";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);

            while (rs.next()) {
                name = rs.getString("sp_name");
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return name;
    }

    public void insertProgram(String es) throws SQLException {

        String sql = "INSERT INTO edoctor.speciality (sp_name,sp_description) VALUES (?,?);";
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setString(1,es);
        t.setString(2,es);

        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            //System.out.println("Se actualizo el valor de "+idnodo);
        }

    }

}
