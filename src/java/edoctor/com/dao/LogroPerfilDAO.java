package edoctor.com.dao;

import edoctor.com.beans.LogroPerfilBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author winbugs
 */
public class LogroPerfilDAO {

    Conexion conexion;

    public LogroPerfilDAO() {
        conexion = new Conexion();
    }

    private static final Logger log = Logger.getLogger(DoctorDAO.class.getPackage().getName());

    public List<LogroPerfilBean> selectLogro(String doctor_id) {

        int us_id = 0;
        int doc_id= Integer.parseInt(doctor_id);
        Statement st;
        ResultSet rs;
        List<LogroPerfilBean> listLogro = new ArrayList<>();
        
        Connection con = conexion.getConnection();
        String consultaSQL = "select tipo_logro, nombre_logro, fecha_logro from edoctor.logros where doc_id = "+doc_id+";";
        System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            LogroPerfilBean p;
            while (rs.next()) {
                p = new LogroPerfilBean();
                p.setTipo_logro(rs.getInt("tipo_logro")+"");
                p.setNombre_logro(rs.getString("nombre_logro"));
                p.setFecha_logro(rs.getString("fecha_logro"));
                listLogro.add(p);
            }

            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return listLogro;
    }

    public int insertLogro(LogroPerfilBean p) throws SQLException {

        int valor = 0;
        String sql = "insert into edoctor.logros (doc_id,tipo_logro,nombre_logro,fecha_logro) VALUES (?,?,?,?);";
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setInt(1, Integer.parseInt(p.getDoc_id()));
        t.setString(2, p.getTipo_logro());
        t.setString(3, p.getNombre_logro());
        t.setString(4, p.getFecha_logro());
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            System.out.println("creaci�n de perfil!!");
            valor = 1;
        }
        return valor;
    }

    public void deleteLogro() throws SQLException {

        String sql = "delete from edoctor.logros;";
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            //System.out.println("Se actualizo el valor de "+name);
        }
    }
    
}

