package edoctor.com.dao;

import edoctor.com.beans.EducacionPerfilBean;
import edoctor.com.beans.ExperienciaPerfilBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author winbugs
 */
public class EducacionPerfilDAO {

    Conexion conexion;

    public EducacionPerfilDAO() {
        conexion = new Conexion();
    }

    private static final Logger log = Logger.getLogger(DoctorDAO.class.getPackage().getName());

    public List<EducacionPerfilBean> selectPerfil(String doctor_id) {

        int us_id = 0;
        int doc_id= Integer.parseInt(doctor_id);
        Statement st;
        ResultSet rs;
        List<EducacionPerfilBean> listEducacion = new ArrayList<>();
        
        Connection con = conexion.getConnection();
        String consultaSQL = "select edu_id,universidad, titulacion, disciplina, nota_media, actividades, fecha_inicio, fecha_final, descripcion  from edoctor.educacion where doc_id = "+doc_id+";";
        System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            EducacionPerfilBean p;
            while (rs.next()) {
                p = new EducacionPerfilBean();
                p.setUniversidad(rs.getString("universidad"));
                p.setTitulacion(rs.getString("titulacion"));
                p.setDisciplina(rs.getString("disciplina"));
                p.setNota_media(rs.getString("nota_media"));
                p.setActividades(rs.getString("actividades"));
                p.setAnio_inicio(rs.getString("fecha_inicio"));
                p.setAnio_fin(rs.getString("fecha_final"));
                p.setDescripcion(rs.getString("descripcion"));
                p.setEdu_id(rs.getString("edu_id"));
                listEducacion.add(p);
            }

            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return listEducacion;
    }

    public int insertEducacion(EducacionPerfilBean p) throws SQLException {

        int valor = 0;
        String sql = "insert into edoctor.educacion (doc_id,universidad,titulacion,disciplina,nota_media,actividades,fecha_inicio,fecha_final,descripcion) VALUES (?,?,?,?,?,?,?,?,?);";
        System.out.println("hola");
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setInt(1, Integer.parseInt(p.getDoc_id()));
        t.setString(2, p.getUniversidad());
        t.setString(3, p.getTitulacion());
        t.setString(4, p.getDisciplina());
        t.setString(5, p.getNota_media());
        t.setString(6, p.getActividades());
        t.setString(7, p.getAnio_inicio());
        t.setString(8, p.getAnio_fin());
        t.setString(9, p.getDescripcion());
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            System.out.println("creaci�n de perfil!!");
            valor = 1;
        }
        return valor;
    }

    public void deleteEducacion(String edu_id) throws SQLException {

        String sql = "delete from edoctor.educacion where edu_id="+Integer.parseInt(edu_id)+";";
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            //System.out.println("Se actualizo el valor de "+name);
        }
    }
    
    public void updateEducacion(EducacionPerfilBean p) throws SQLException {
        
        String sql = "update edoctor.educacion set";
        String cola = "";
        int contador = 0;
       
        if (p.getTitulacion()!= null){
            sql = sql + " titulacion='"+p.getTitulacion()+"'";
            contador++;
        }
        
        if (p.getDisciplina()!= null){
            
            if (contador>0){
                sql = sql + ", disciplina='"+p.getDisciplina()+"'";
            }else {
                sql = sql + " disciplina='"+p.getDisciplina()+"'";
            }
            contador++;
        }
        
        if (p.getNota_media()!= null){
            
            if (contador>0){
                sql = sql + ", nota_media='"+p.getNota_media()+"'";
            }else {
                sql = sql + " nota_media='"+p.getNota_media()+"'";
            }
            contador++;
        }
        
        if (p.getActividades()!= null){
            
            if (contador>0){
                sql = sql + ", actividades='"+p.getActividades()+"'";
            }else {
                sql = sql + " actividades='"+p.getActividades()+"'";
            }
            contador++;
        }
        
        if (p.getAnio_inicio()!= null){
            
            if (contador>0){
                sql = sql + ", fecha_inicio='"+p.getAnio_inicio()+"'";
            }else {
                sql = sql + " fecha_inicio='"+p.getAnio_inicio()+"'";
            }
            contador++;
        }
        
        if (p.getAnio_fin()!= null){
            
            if (contador>0){
                sql = sql + ", fecha_final='"+p.getAnio_fin()+"'";
            }else {
                sql = sql + " fecha_final='"+p.getAnio_fin()+"'";
            }
            contador++;
        }
        
        if (p.getDescripcion()!= null){
            
            if (contador>0){
                sql = sql + ", descripcion='"+p.getDescripcion()+"'";
            }else {
                sql = sql + " descripcion='"+p.getDescripcion()+"'";
            }
            contador++;
        }
        
        int edu_ip = Integer.parseInt(p.getEdu_id());
        
        sql = sql + " where edu_id ="+edu_ip+";";  
        
        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
    }
}

