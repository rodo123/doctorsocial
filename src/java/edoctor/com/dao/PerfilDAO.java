package edoctor.com.dao;

import edoctor.com.beans.PerfilBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author winbugs
 */
public class PerfilDAO {

    Conexion conexion;

    public PerfilDAO() {
        conexion = new Conexion();
    }

    private static final Logger log = Logger.getLogger(DoctorDAO.class.getPackage().getName());

    public List<PerfilBean> selectPerfil(String doctor_id) {

        List<PerfilBean> listPerfil = new ArrayList<>();
        int doc_id = Integer.parseInt(doctor_id);
        Statement st;
        ResultSet rs;
        Connection con = conexion.getConnection();
        String consultaSQL = "select titular, lema, universidad, reputacion, visitas, busquedas, url_imagen_fondo, url_imagen_personal from edoctor.perfil where doc_id = " + doc_id + ";";
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            PerfilBean p;
            while (rs.next()) {
                p = new PerfilBean();
                p.setTitular(rs.getString("titular"));
                p.setLema(rs.getString("lema"));
                p.setUniversidad(rs.getString("universidad"));
                p.setReputacion(rs.getInt("reputacion"));
                p.setVisitas(rs.getInt("visitas"));
                p.setBusquedas(rs.getInt("busquedas"));
                p.setUrl_imagen_fondo(rs.getString("url_imagen_fondo"));
                p.setUrl_imagen_personal(rs.getString("url_imagen_personal"));
                listPerfil.add(p);
            }

            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return listPerfil;
    }

    public int updateExperiencia(PerfilBean p) throws SQLException {

        String sql = "update edoctor.perfil set";
        int contador = 0;
        
        
        if (p.getTitular().isEmpty() == false) {
            
            sql = sql + " titular='" + p.getTitular() + "'";
            contador++;
        }
        if (p.getLema().isEmpty() == false) {
            if (contador > 0) {
                sql = sql + ", lema='" + p.getLema() + "'";
            } else {
                sql = sql + " lema='" + p.getLema() + "'";
            }
            contador++;
        }
        if (p.getUniversidad().isEmpty() == false) {
            if (contador > 0) {
                sql = sql + ", universidad='" + p.getUniversidad() + "'";
            } else {
                sql = sql + " universidad='" + p.getUniversidad() + "'";
            }

        }
        if (p.getUrl_imagen_fondo().isEmpty() == false) {
            if (contador > 0) {
                sql = sql + ", url_imagen_fondo='" + p.getUrl_imagen_fondo() + "'";
            } else {
                sql = sql + " url_imagen_fondo='" + p.getUrl_imagen_fondo() + "'";
            }

        }
        
        if (p.getUrl_imagen_personal().isEmpty() == false) {
            if (contador > 0) {
                sql = sql + ", url_imagen_personal='" + p.getUrl_imagen_personal() + "'";
            } else {
                sql = sql + " url_imagen_personal='" + p.getUrl_imagen_personal() + "'";
            }

        }

        int doc_id = Integer.parseInt(p.getDoc_id());

        sql = sql + " where doc_id =" + doc_id + ";";

        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
        return 1;
    }
    
    public int updateVisitas(int visitas, int doc_id) throws SQLException {

        String sql = "update edoctor.perfil set visitas="+visitas;
        sql = sql + " where doc_id =" + doc_id + ";";

        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
        return 1;
    }
    
    public int updateRecomendar(int reputacion, int doc_id) throws SQLException {

        String sql = "update edoctor.perfil set reputacion="+reputacion;
        sql = sql + " where doc_id =" + doc_id + ";";

        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
        return 1;
    }
    
    public int selectVisitas(String doctor_id) {
        
        int visitas = 0;
        int doc_id = Integer.parseInt(doctor_id);
        Statement st;
        ResultSet rs;
        Connection con = conexion.getConnection();
        String consultaSQL = "select visitas from edoctor.perfil where doc_id = " + doc_id + ";";
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            PerfilBean p;
            while (rs.next()) {
             
                visitas = rs.getInt("visitas");
            }

            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return visitas;
    }
    
}
