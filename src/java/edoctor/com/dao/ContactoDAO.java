package edoctor.com.dao;

import edoctor.com.beans.ContactoBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author Rodolfo Zevallos
 */
public class ContactoDAO {

    Conexion conexion;

    public ContactoDAO() {
        conexion = new Conexion();
    }

    private static final Logger log = Logger.getLogger(ContactoDAO.class.getPackage().getName());

    public int insertContacto(ContactoBean p) throws SQLException {

        String sql = "insert into edoctor.contacto (nombre, email, mensaje) VALUES (?,?,?);";

        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setString(1, p.getNombre());
        t.setString(2, p.getEmail());
        t.setString(3, p.getMensaje());
        int rowsInserted = t.executeUpdate();
        System.out.println(t.toString());
        if (rowsInserted > 0) {
            log.debug("Se inserto lo siquiente: " + sql);
        }

        int last_inserted_id = 0;
        ResultSet rs = t.getGeneratedKeys();
        if (rs.next()) {
            last_inserted_id = rs.getInt(1);
        }
        return last_inserted_id;
    }

    

}
