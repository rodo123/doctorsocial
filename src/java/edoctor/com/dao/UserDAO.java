
package edoctor.com.dao;

import edoctor.com.beans.DoctorBean;
import edoctor.com.beans.UserBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author winbugs
 */
public class UserDAO {
    
    Conexion conexion;

    public UserDAO() {
        conexion = new Conexion();
    }
    
    private static final Logger log = Logger.getLogger(DoctorDAO.class.getPackage().getName());
    
    public int selectIdUser(String cmp) {

        int us_id = 0;
        Statement st;
        ResultSet rs;
        List<DoctorBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select doc_id from edoctor.doctor where doc_cmp = '"+cmp+"';";
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
        
            while (rs.next()) {
                us_id = rs.getInt("doc_id");
            }
            
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql "+ e.getMessage());
        }   
       return us_id;
    }
    
    public int selectUser(String usuario, String contrase�a) {

        int us_id = 0;
        Statement st;
        ResultSet rs;
        List<DoctorBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select us_id from edoctor.user where us_user = '"+usuario+"' and us_pass = '"+contrase�a+"';";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
        
            while (rs.next()) {
                us_id = rs.getInt("us_id");
            }
            
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql "+ e.getMessage());
        }   
        if (us_id == 0) return 0;
        else return 1;
    }
    
    
    
        public int selectUserByAcount(String usuario) {

        int us_id = 0;
        Statement st;
        ResultSet rs;
        List<DoctorBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select us_id from edoctor.user where us_user = '"+usuario+"'";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
        
            while (rs.next()) {
                us_id = rs.getInt("us_id");
            }
            
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql "+ e.getMessage());
        }   
        if (us_id == 0) return 0;
        else return 1;
    }
    
    
    
     public int SelectDoctorIdUser(String usuario, String contrase�a) {

        int us_id = 0;
        Statement st;
        ResultSet rs;
        List<DoctorBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select doc_id from edoctor.user where us_user = '"+usuario+"' and us_pass = '"+contrase�a+"';";
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
        
            while (rs.next()) {
                us_id = rs.getInt("doc_id");
            }
            
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql "+ e.getMessage());
        }   
       return us_id;
    }
    
    public int selectUserDelete(String usuario, String contrase�a) {

        int us_id = 0;
        Statement st;
        ResultSet rs;
        List<DoctorBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select doc_id from edoctor.user where us_user = '"+usuario+"' and us_pass = '"+contrase�a+"';";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
        
            while (rs.next()) {
                us_id = rs.getInt("doc_id");
            }
            
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql "+ e.getMessage());
        }   
        
        return us_id;
    }
    
    public void insertUser(UserBean p) throws SQLException {

        String sql = "insert into edoctor.user (us_user, us_pass, doc_id) VALUES (?,?,?);";

        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setString(1, p.getUs_user());
        t.setString(2, p.getUs_pass());
        t.setInt(3, p.getDoc_id());
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            System.out.println("Creaci�n de cuenta exitosa!!");
        }
    }
    
        public void deleteUser(int doc_id) throws SQLException {

        String sql = "delete from edoctor.user where doc_id = " + doc_id + ";";
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            //System.out.println("Se actualizo el valor de "+name);
        }
    }
}
