
package edoctor.com.dao;

import edoctor.com.beans.EnterpriseBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author winbugs
 */
public class EnterpriseDAO {
    
    Conexion conexion;

    public EnterpriseDAO() {
        conexion = new Conexion();
    }

    public int insertEnterprise(EnterpriseBean p) throws SQLException {

        String sql = "insert into edoctor.enterprise (en_name, en_region_id, en_province_id, en_distric_id, en_type, en_description) VALUES (?,?,?,?,?,?);";

        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setString(1, p.getEn_name());
        t.setInt(2, p.getEn_region_id());
        t.setInt(3, p.getEn_province_id());
        t.setInt(4, p.getEn_distric_id());
        t.setString(5, p.getEn_type());
        t.setString(6, p.getEn_description());
        int rowsInserted = t.executeUpdate();
        int last_inserted_id=0;
        ResultSet rs = t.getGeneratedKeys();
                if(rs.next())
                {
                   last_inserted_id = rs.getInt(1);
                }
        if (rowsInserted > 0) {
            //System.out.println("Se actualizo el valor de "+i);
        }
        return last_inserted_id;
    }
    
    public List<EnterpriseBean> selectEnterprise() {

        int idnodo = 0;
        Statement st;
        ResultSet rs;
        List<EnterpriseBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select * from edoctor.enterprise;";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            EnterpriseBean p;
            while (rs.next()) {
                p = new EnterpriseBean();
                p.setEn_id(rs.getInt("en_id"));
                p.setEn_name(rs.getString("en_name"));
                p.setEn_region_id(rs.getInt("en_region_id"));
                p.setEn_province_id(rs.getInt("en_province_id"));
                p.setEn_distric_id(rs.getInt("en_distric_id"));
                p.setEn_type(rs.getString("en_type"));
                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return list;
    }
    
    public int selectEnterpriseOne(String speciality) {

        int idnodo = 0;
        Statement st;
        ResultSet rs;

        Connection con = conexion.getConnection();
        String consultaSQL = "select sp_id from edoctor.speciality where sp_name = '" + speciality + "';";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);

            while (rs.next()) {
                idnodo = rs.getInt("sp_id");
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return idnodo;
    }
    
    public String selectEnterpriseMatch(String id, String tipo) {

        String en_name = "";
        Statement st;
        ResultSet rs;
        int en_id = 0;
        String consultaSQL = "";
        if (tipo.contains("1")){
            en_id = Integer.parseInt(id);
            consultaSQL = "select en_name from edoctor.enterprise where en_id = " + en_id + ";";
        } else {
            consultaSQL = "select en_name from edoctor.enterprise where en_name = '" + id + "';";
        }

        Connection con = conexion.getConnection();
       
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);

            while (rs.next()) {
                en_name = rs.getString("en_name");
            }
            System.out.println("test: "+en_name);
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return en_name;
    }
}
