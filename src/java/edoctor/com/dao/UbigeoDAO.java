/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.dao;

import edoctor.com.beans.DistritoBean;
import edoctor.com.beans.ProvinciaBean;
import edoctor.com.beans.RegionBean;
import edoctor.com.beans.SpecialityBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author winbugs
 */
public class UbigeoDAO {
    
    Conexion conexion;

    public UbigeoDAO() {

        conexion = new Conexion();
    }
    
    public List<RegionBean> selectRegion() {

        Statement st;
        ResultSet rs;
        List<RegionBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select idDepa, departamento from edoctor.ubdepartamento;";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            RegionBean p;

            while (rs.next()) {
                p = new RegionBean();
                p.setRe_id(rs.getInt("idDepa"));
                p.setRe_name(rs.getString("departamento"));
                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return list;
    }
    
    public List<ProvinciaBean> selectProvincia() {

        Statement st;
        ResultSet rs;
        List<ProvinciaBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select idProv, provincia, idDepa from edoctor.ubprovincia;";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            ProvinciaBean p;

            while (rs.next()) {
                p = new ProvinciaBean();
                p.setPr_id(rs.getInt("idProv"));
                p.setPr_name(rs.getString("provincia"));
                p.setPr_re_id(rs.getInt("idDepa"));
                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return list;
    }
    
    
    
    
    public List<ProvinciaBean> selectProvinciaFromDepartment(String idDepartamento) {

        Statement st;
        ResultSet rs;
        List<ProvinciaBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select idProv, provincia, idDepa from edoctor.ubprovincia where idDepa=".
                concat(idDepartamento.toString());
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            ProvinciaBean p;

            while (rs.next()) {
                p = new ProvinciaBean();
                p.setPr_id(rs.getInt("idProv"));
                p.setPr_name(rs.getString("provincia"));
                p.setPr_re_id(rs.getInt("idDepa"));
                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return list;
    }
    
    public List<DistritoBean> selectDistrito() {

        Statement st;
        ResultSet rs;
        List<DistritoBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select idDist, distrito, idProv from edoctor.ubdistrito;";
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            DistritoBean p;

            while (rs.next()) {
                p = new DistritoBean();
                p.setDi_id(rs.getInt("idDist"));
                p.setDi_name(rs.getString("distrito"));
                p.setDi_pr_id(rs.getInt("idProv"));
                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return list;
    }
    
    
    public List<DistritoBean> selectDistritoFromProvincia(String idProvincia) {

        Statement st;
        ResultSet rs;
        List<DistritoBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select idDist, distrito, idProv from edoctor.ubdistrito where idProv=".concat(idProvincia);
        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            DistritoBean p;

            while (rs.next()) {
                p = new DistritoBean();
                p.setDi_id(rs.getInt("idDist"));
                p.setDi_name(rs.getString("distrito"));
                p.setDi_pr_id(rs.getInt("idProv"));
                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {

            e.getMessage();
        }
        return list;
    }
}
