package edoctor.com.dao;

import edoctor.com.beans.DoctorBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Rodolfo Zevallos
 */
public class DoctorDAO {

    Conexion conexion;

    public DoctorDAO() {
        conexion = new Conexion();
    }

    private static final Logger log = Logger.getLogger(DoctorDAO.class.getPackage().getName());

    public int insertDoctor(DoctorBean p) throws SQLException {

        String sql = "insert into edoctor.doctor (doc_fname, doc_lname, doc_cmp, doc_phone, doc_email, doc_id_speciality1, doc_id_speciality2, doc_id_job) VALUES (?,?,?,?,?,?,?,?);";

        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setString(1, p.getDoc_fname());
        t.setString(2, p.getDoc_lname());
        t.setString(3, p.getDoc_cmp());
        t.setString(4, p.getDoc_phone());
        t.setString(5, p.getDoc_email());
        t.setInt(6, p.getDoc_id_speciality1());
        t.setInt(7, p.getDoc_id_speciality2());
        t.setInt(8, p.getDoc_id_job());
        int rowsInserted = t.executeUpdate();
        System.out.println(t.toString());
        if (rowsInserted > 0) {
            log.debug("Se inserto lo siquiente: " + sql);
        }

        int last_inserted_id = 0;
        ResultSet rs = t.getGeneratedKeys();
        if (rs.next()) {
            last_inserted_id = rs.getInt(1);
        }
        return last_inserted_id;
    }

    public List<DoctorBean> selectDoctor() {

        Statement st;
        ResultSet rs;
        List<DoctorBean> listDoctorBean = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "select doc_fname, doc_lname, doc_cmp, doc_phone, doc_email, doc_id_speciality1, doc_id_speciality2, doc_id_job from edoctor.doctor;";

        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            DoctorBean p;
            while (rs.next()) {
                p = new DoctorBean();
                p.setDoc_fname(rs.getNString("doc_fname"));
                p.setDoc_lname(rs.getNString("doc_lname"));
                p.setDoc_cmp(rs.getNString("doc_cmp"));
                p.setDoc_phone(rs.getNString("doc_phone"));
                p.setDoc_email(rs.getNString("doc_email"));
                p.setDoc_id_speciality1(rs.getInt("doc_id_speciality1"));
                p.setDoc_id_speciality2(rs.getInt("doc_id_speciality2"));
                p.setDoc_id_job(rs.getInt("doc_id_job"));
                listDoctorBean.add(p);
            }
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return listDoctorBean;
    }

    public List<DoctorBean> SearchDoctor(String nombre, String apellido, String cmp,
            String especialidad, String trabajo) {
        Statement st;
        ResultSet rs;
        List<DoctorBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();

        String consultaSQL = "select d.doc_fname, d.doc_lname, d.doc_cmp, d.doc_phone, d.doc_email, s.sp_name, e.en_name, d.facebook, d.twitter from edoctor.doctor d  "
                + " inner join edoctor.speciality s"
                + " on d.doc_id_speciality1=s.sp_id "
                + " inner join edoctor.job j"
                + " on j.job_id=d.doc_id_job "
                + " inner join edoctor.enterprise e"
                + " on j.job_en_id =e.en_id "
                + " where"
                + "  (doc_fname like ('%"+nombre+"%') and '' <> '"+nombre+"' ) or "
                + "  (doc_lname like ('%"+apellido+"%') and  '' <> '"+apellido+"' ) or "
                + "  doc_cmp = '"+cmp+"' or "
                + "  sp_name = '"+especialidad+"' or"
                + "  en_name= '"+trabajo+"';";
        
        System.out.println(consultaSQL);
        try {
            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            DoctorBean p;
            while (rs.next()) {

                p = new DoctorBean();
                p.setDoc_fname(rs.getString("d.doc_fname"));
                p.setDoc_lname(rs.getString("d.doc_lname"));
                p.setDoc_cmp(rs.getString("d.doc_cmp"));
                p.setDoc_phone(rs.getString("d.doc_phone"));
                p.setDoc_email(rs.getString("d.doc_email"));
                p.setDoc_sp_name1(rs.getString("s.sp_name"));
                p.setDoc_en_name(rs.getString("e.en_name"));
                p.setFacebook(rs.getString("d.facebook"));
                p.setTwitter(rs.getString("d.twitter"));
                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return list;
    }

    public DoctorBean selectDoctorById(String id) {

        Statement st;
        ResultSet rs;
        Connection con = conexion.getConnection();

        String consultaSQL = "select * from edoctor.doctor where doc_id =" + id;
        DoctorBean p = null;

        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            while (rs.next()) {
                p = new DoctorBean();
                p.setDoc_fname(rs.getString("doc_fname"));
                p.setDoc_lname(rs.getString("doc_lname"));
                p.setDoc_cmp(rs.getString("doc_cmp"));
                p.setDoc_phone(rs.getString("doc_phone"));
                p.setDoc_email(rs.getString("doc_email"));
                p.setDoc_id_speciality1(rs.getInt("doc_id_speciality1"));
                p.setDoc_id_speciality2(rs.getInt("doc_id_speciality2"));
                p.setDoc_id_job(rs.getInt("doc_id_job"));
                p.setFacebook(rs.getString("facebook"));
                p.setTwitter(rs.getString("twitter"));
            }
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return p;
    }

    public List<DoctorBean> selectDoctor(String nombre, String apellido) {

        Statement st;
        ResultSet rs;
        List<DoctorBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();
        String consultaSQL = "";
        if (nombre.isEmpty() == true) {
            consultaSQL = "select * from edoctor.doctor where doc_lname = '" + apellido + "';";
        } else if (apellido.isEmpty() == true) {
            consultaSQL = "select * from edoctor.doctor where doc_fname = '" + nombre + "';";
        } else {
            consultaSQL = "select * from edoctor.doctor where doc_fname = '" + nombre + "' and doc_lname = '" + apellido + "';";
        }

        //System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            DoctorBean p;
            while (rs.next()) {
                p = new DoctorBean();
                p.setDoc_fname(rs.getNString("doc_fname"));
                p.setDoc_lname(rs.getNString("doc_lname"));
                p.setDoc_cmp(rs.getNString("doc_cmp"));
                p.setDoc_phone(rs.getNString("doc_phone"));
                p.setDoc_email(rs.getNString("doc_email"));
                p.setDoc_id_speciality1(rs.getInt("doc_id_speciality1"));
                p.setDoc_id_speciality2(rs.getInt("doc_id_speciality2"));
                p.setDoc_id_job(rs.getInt("doc_id_job"));
            }
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return list;
    }

    public void updateDoctor(DoctorBean p) throws SQLException {
        
        String sql = "update edoctor.doctor set";
        String cola = "";
        int contador = 0;
        
        
        if (p.getDoc_fname().isEmpty() == false){
            
            sql = sql + " doc_fname='"+p.getDoc_fname()+"'";
            contador++;
        }
        
        
        if (p.getDoc_lname().isEmpty() == false){
            
            if (contador>0){
                sql = sql + ", doc_lname='"+p.getDoc_lname()+"'";
            }else {
                sql = sql + " doc_lname='"+p.getDoc_lname()+"'";
            }
            contador++;
        }
       System.out.println("3");
        if (p.getDoc_phone() != null){
            
            if (contador>0){
                sql = sql + ", doc_phone='"+p.getDoc_phone()+"'";
            }else {
                sql = sql + " doc_phone='"+p.getDoc_phone()+"'";
            }
            contador++;
        }
        System.out.println("4");
        if (p.getDoc_email()!= null){
            
            if (contador>0){
                sql = sql + ", doc_email='"+p.getDoc_email()+"'";
            }else {
                sql = sql + " doc_email='"+p.getDoc_email()+"'";
            }
            contador++;
        }
        /*
        if (p.getFacebook()!= null){
            
            if (contador>0){
                sql = sql + ", facebook='"+p.getFacebook()+"'";
            }else {
                sql = sql + " facebook='"+p.getFacebook()+"'";
            }
            contador++;
        }
        
        if (p.getTwitter()!= null){
            
            if (contador>0){
                sql = sql + ", twitter='"+p.getTwitter()+"'";
            }else {
                sql = sql + " twitter='"+p.getTwitter()+"'";
            }
            contador++;
        }*/
        
        int doc_id = Integer.parseInt(p.getDoc_id());
        
        sql = sql + " where doc_id ="+doc_id+";";  
        
        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
    }

    public void updateIdJobDoctor(int idDoctor, int idJob) throws SQLException {

        String sql = "update edoctor.doctor set  doc_id_job =" + idJob + " where "
                + "doc_id=" + idDoctor;
        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
    }

    public void deleteDoctor(int doc_id) throws SQLException {

        String sql = "delete from edoctor.doctor where doc_id = " + doc_id + ";";
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se borro lo siquiente: " + sql);
        }
    }
    
    public void updateDoctor2(DoctorBean p) throws SQLException {
        
        String sql = "update edoctor.doctor set";
        String cola = "";
        int contador = 0;
       
        if (p.getDoc_phone() != null){
            sql = sql + " doc_phone='"+p.getDoc_phone()+"'";
            contador++;
        }
        
        if (p.getFacebook()!= null){
            
            if (contador>0){
                sql = sql + ", facebook='"+p.getFacebook()+"'";
            }else {
                sql = sql + " facebook='"+p.getFacebook()+"'";
            }
            contador++;
        }
        
        if (p.getTwitter()!= null){
            
            if (contador>0){
                sql = sql + ", twitter='"+p.getTwitter()+"'";
            }else {
                sql = sql + " twitter='"+p.getTwitter()+"'";
            }
            contador++;
        }
        
        int doc_id = Integer.parseInt(p.getDoc_id());
        
        sql = sql + " where doc_id ="+doc_id+";";  
        
        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
    }

}
