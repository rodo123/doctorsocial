package edoctor.com.dao;

import edoctor.com.beans.DoctorBean;
import edoctor.com.beans.ExperienciaPerfilBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author winbugs
 */
public class ExperienciaPerfilDAO {

    Conexion conexion;

    public ExperienciaPerfilDAO() {
        conexion = new Conexion();
    }

    private static final Logger log = Logger.getLogger(DoctorDAO.class.getPackage().getName());

    public List<ExperienciaPerfilBean> selectPerfil(String doctor_id) {

        int us_id = 0;
        int doc_id= Integer.parseInt(doctor_id);
        Statement st;
        ResultSet rs;
        List<ExperienciaPerfilBean> listExperiencia = new ArrayList<>();
        
        Connection con = conexion.getConnection();
        String consultaSQL = "select exp_id,empresa,ubicacion,descripcion,cargo,fecha_inicio,fecha_final,actual from edoctor.experiencia where doc_id = "+doc_id+";";
        System.out.println(consultaSQL);
        try {

            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            ExperienciaPerfilBean p;
            while (rs.next()) {
                p = new ExperienciaPerfilBean();
                p.setEmpresa(rs.getString("empresa"));
                p.setLugar(rs.getString("ubicacion"));
                p.setDescripcion(rs.getString("Descripcion"));
                p.setCargo(rs.getString("cargo"));
                String fecha_inicio = rs.getString("fecha_inicio");
                String inicio[] = fecha_inicio.split("-");
                p.setAnio_inicio(inicio[1]);
                p.setMes_inicio(inicio[0]);
                String fecha_final = rs.getString("fecha_final");
                String finals[] = fecha_final.split("-");
                p.setAnio_fin(finals[1]);
                p.setMes_fin(finals[0]);
                p.setActual(rs.getString("actual"));
                p.setExp_id(rs.getString("exp_id"));
                listExperiencia.add(p);
            }

            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return listExperiencia;
    }

    public int insertExperiencia(ExperienciaPerfilBean p) throws SQLException {

        int valor = 0;
        String sql = "insert into edoctor.experiencia (doc_id,empresa,cargo,ubicacion,fecha_inicio,fecha_final,actual,descripcion) VALUES (?,?,?,?,?,?,?,?);";

        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setInt(1, Integer.parseInt(p.getDoc_id()));
        t.setString(2, p.getEmpresa());
        t.setString(3, p.getCargo());
        t.setString(4, p.getLugar());
        t.setString(5, p.getMes_inicio() + "-" + p.getAnio_inicio());
        t.setString(6, p.getMes_fin() + "-" + p.getAnio_fin());
        t.setString(7, p.getActual());
        t.setString(8, p.getDescripcion());
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            System.out.println("creaci�n de perfil!!");
            valor = 1;
        }
        return valor;
    }

    public void deleteExperiencia(String exp_id) throws SQLException {

        String sql = "delete from edoctor.experiencia where exp_id="+Integer.parseInt(exp_id)+";";
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            //System.out.println("Se actualizo el valor de "+name);
        }
    }
    
    public void updateExperiencia(ExperienciaPerfilBean p) throws SQLException {
        
        String sql = "update edoctor.experiencia set";
        String cola = "";
        int contador = 0;
       
        if (p.getEmpresa() != null){
            sql = sql + " empresa='"+p.getEmpresa()+"'";
            contador++;
        }
        
        if (p.getLugar()!= null){
            
            if (contador>0){
                sql = sql + ", ubicacion='"+p.getLugar()+"'";
            }else {
                sql = sql + " ubicacion='"+p.getLugar()+"'";
            }
            contador++;
        }
        
        if (p.getMes_inicio()!= null && p.getAnio_inicio()!= null){
            
            if (contador>0){
                sql = sql + ", fecha_inicio='"+p.getMes_inicio() + "-" + p.getAnio_inicio()+"'";
            }else {
                sql = sql + " fecha_inicio='"+p.getMes_inicio() + "-" + p.getAnio_inicio()+"'";
            }
            contador++;
        }
        
        if (p.getMes_fin()!= null && p.getAnio_fin()!= null){
            
            if (contador>0){
                sql = sql + ", fecha_final='"+p.getMes_fin()+ "-" + p.getAnio_fin()+"'";
            }else {
                sql = sql + " fecha_final='"+p.getMes_fin()+ "-" + p.getAnio_fin()+"'";
            }
            contador++;
        }
        
        if (p.getDescripcion()!= null){
            
            if (contador>0){
                sql = sql + ", descripcion='"+p.getDescripcion()+"'";
            }else {
                sql = sql + " descripcion='"+p.getDescripcion()+"'";
            }
            contador++;
        }
        
        int exp_id = Integer.parseInt(p.getExp_id());
        
        sql = sql + " where exp_id ="+exp_id+";";  
        
        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
    }
}

