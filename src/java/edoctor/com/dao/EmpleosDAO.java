package edoctor.com.dao;

import edoctor.com.beans.EmpleoBean;
import edoctor.com.beans.ExperienciaPerfilBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author winbugs
 */
public class EmpleosDAO {

    Conexion conexion;

    public EmpleosDAO() {
        conexion = new Conexion();
    }

    private static final Logger log = Logger.getLogger(DoctorDAO.class.getPackage().getName());

    public List<EmpleoBean> SearchEmpleo(String Cargo, String ubicacion, String empresa, String nivel_antiguedad, String tipo_empleo) {
        Statement st;
        ResultSet rs;
        List<EmpleoBean> list = new ArrayList<>();
        Connection con = conexion.getConnection();

        String consultaSQL = "select * from edoctor.trabajo where (cargo like ('%"+Cargo+"%') and '' <> '"+Cargo+"' ) or "
                + "(ubicacion like ('%"+ubicacion+"%') and  '' <> '"+ubicacion+"' )"
                + "or tipo_empleo='"+tipo_empleo+"' "
                + "or (empresa like ('%"+empresa+"%') and  '' <> '"+empresa+"' );";
        
        System.out.println(consultaSQL);
        try {
            st = con.createStatement();
            rs = st.executeQuery(consultaSQL);
            EmpleoBean p;
            while (rs.next()) {

                p = new EmpleoBean();
                p.setCargo(rs.getString("cargo"));
                p.setTrabajo_id(rs.getString("trabajo_id"));
                p.setDoc_id(rs.getString("doc_id"));
                p.setEmpresa(rs.getString("empresa"));
                p.setUbicacion(rs.getString("ubicacion"));
                p.setFuncion_laboral(rs.getString("funcion_laboral"));
                p.setTipo_empleo(rs.getString("tipo_empleo"));
                p.setSector_empresa(rs.getString("sector_empresa"));
                p.setNivel_antiguedad(rs.getString("nivel_antiguedad"));
                p.setDescripcion(rs.getString("descripcion"));
                p.setDescripcion_empresa(rs.getString("descripcion_empresa"));
                p.setEmail(rs.getString("email"));
                p.setUrl_page(rs.getString("url_page"));
                list.add(p);
            }
            rs.close();
        } catch (SQLException e) {
            log.error("Se produjo un error en el sql " + e.getMessage());
        }
        return list;
    }

    public int insertEmpleo(EmpleoBean p) throws SQLException {

        int valor = 0;
        String sql = "insert into edoctor.trabajo (doc_id,"
                + "cargo,"
                + "empresa,"
                + "ubicacion,"
                + "funcion_laboral,"
                + "tipo_empleo,"
                + "sector_empresa,"
                + "nivel_antiguedad,"
                + "descripcion,"
                + "descripcion_empresa,"
                + "email,"
                + "url_page) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";

        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setInt(1, Integer.parseInt(p.getDoc_id()));
        t.setString(2, p.getCargo());
        t.setString(3, p.getEmpresa());
        t.setString(4, p.getUbicacion());
        t.setString(5, p.getFuncion_laboral());
        t.setString(6, p.getTipo_empleo());
        t.setString(7, p.getSector_empresa());
        t.setString(8, p.getNivel_antiguedad());
        t.setString(9, p.getDescripcion());
        t.setString(10, p.getDescripcion_empresa());
        t.setString(11, p.getEmail());
        t.setString(12, p.getUrl_page());
        
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            System.out.println("creaci�n de perfil!!");
            valor = 1;
        }
        return valor;
    }
    
    public void deleteEmpleo(String trabajo_id) throws SQLException {

        
    }
    
    public void updateEmpleo(ExperienciaPerfilBean p) throws SQLException {
        
        String sql = "update edoctor.experiencia set";
        String cola = "";
        int contador = 0;
       
        if (p.getEmpresa() != null){
            sql = sql + " empresa='"+p.getEmpresa()+"'";
            contador++;
        }
        
        if (p.getLugar()!= null){
            
            if (contador>0){
                sql = sql + ", ubicacion='"+p.getLugar()+"'";
            }else {
                sql = sql + " ubicacion='"+p.getLugar()+"'";
            }
            contador++;
        }
        
        if (p.getMes_inicio()!= null && p.getAnio_inicio()!= null){
            
            if (contador>0){
                sql = sql + ", fecha_inicio='"+p.getMes_inicio() + "-" + p.getAnio_inicio()+"'";
            }else {
                sql = sql + " fecha_inicio='"+p.getMes_inicio() + "-" + p.getAnio_inicio()+"'";
            }
            contador++;
        }
        
        if (p.getMes_fin()!= null && p.getAnio_fin()!= null){
            
            if (contador>0){
                sql = sql + ", fecha_final='"+p.getMes_fin()+ "-" + p.getAnio_fin()+"'";
            }else {
                sql = sql + " fecha_final='"+p.getMes_fin()+ "-" + p.getAnio_fin()+"'";
            }
            contador++;
        }
        
        if (p.getDescripcion()!= null){
            
            if (contador>0){
                sql = sql + ", descripcion='"+p.getDescripcion()+"'";
            }else {
                sql = sql + " descripcion='"+p.getDescripcion()+"'";
            }
            contador++;
        }
        
        int exp_id = Integer.parseInt(p.getExp_id());
        
        sql = sql + " where exp_id ="+exp_id+";";  
        
        System.out.println(sql);
        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        int rowsInserted = t.executeUpdate();
        if (rowsInserted > 0) {
            log.debug("Se actualizo lo siquiente: " + sql);
        }
    }
}

