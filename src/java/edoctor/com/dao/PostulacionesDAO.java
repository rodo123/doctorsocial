package edoctor.com.dao;

import edoctor.com.beans.ContactoBean;
import edoctor.com.beans.PostulacionesBean;
import edoctor.com.conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import utils.Mail;

/**
 *
 * @author Rodolfo Zevallos
 */
public class PostulacionesDAO {

    Conexion conexion;

    public PostulacionesDAO() {
        conexion = new Conexion();
    }

    private static final Logger log = Logger.getLogger(PostulacionesDAO.class.getPackage().getName());

    public int insertPostulacion(PostulacionesBean p, String correoTo, String correoOf , String cargo) throws SQLException {

        String sql = "insert into edoctor.postulaciones (trabajo_id, doc_id) VALUES (?,?);";

        Connection con = conexion.getConnection();
        PreparedStatement t;
        t = con.prepareStatement(sql);
        t.setInt(1, Integer.parseInt(p.getTrabajo_id()));
        t.setInt(2, Integer.parseInt(p.getDoc_id()));
        int rowsInserted = t.executeUpdate();
        System.out.println(t.toString());
        if (rowsInserted > 0) {
            log.debug("Se inserto lo siguiente: " + sql);
        }
        
        Mail m = new Mail();
        m.SendMail(correoTo,correoOf,cargo);
        
        return 1;
    }

   
}
