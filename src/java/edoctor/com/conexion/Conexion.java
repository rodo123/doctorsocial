/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.conexion;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class Conexion {

	   private static Connection conection;
	   private static String servidor ="jdbc:mysql://localhost/edoctor";
	   private static String user = "root";
	   private static String pass = "";
	   private static String driver = "com.mysql.jdbc.Driver";
	   
	   public Conexion(){
	       try{
	           Class.forName(driver);
	           conection=(Connection) DriverManager.getConnection(servidor, user, pass);
	       }catch(ClassNotFoundException | SQLException e){
	           System.out.println("Conexion fallida");   
	       }
	   }
	   public Connection getConnection(){
	           return conection;
	       }
	   
	}


