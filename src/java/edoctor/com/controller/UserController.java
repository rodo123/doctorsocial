package edoctor.com.controller;

import edoctor.com.beans.DoctorBean;
import edoctor.com.beans.EducacionPerfilBean;
import edoctor.com.beans.ExperienciaPerfilBean;
import edoctor.com.beans.LogroPerfilBean;
import edoctor.com.beans.PerfilBean;
import edoctor.com.beans.UserBean;
import edoctor.com.dao.DoctorDAO;
import edoctor.com.dao.EducacionPerfilDAO;
import edoctor.com.dao.ExperienciaPerfilDAO;
import edoctor.com.dao.LogroPerfilDAO;
import edoctor.com.dao.PerfilDAO;
import edoctor.com.dao.SpecialityDAO;
import edoctor.com.dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "UserController", urlPatterns = {"/UserController/loginAcces.do", "/UserController/loginCreate.do",})
public class UserController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            UserDAO userDAO = new UserDAO();
            RequestDispatcher despachador = null;
            HttpSession session = request.getSession(true);
            int valor = 0;

            if (request.getServletPath().contains("/loginAcces.do")) {

                String usuario = request.getParameter("usuario");
                String contraseña = request.getParameter("contraseña");
                
                valor = userDAO.selectUser(usuario, contraseña);
                
            } else if (request.getServletPath().contains("/loginCreate.do")) {
                
                valor = 1;
                UserBean userBean = new UserBean();
                userBean.setUs_user(request.getParameter("usuarioNuevo"));
                userBean.setUs_pass(request.getParameter("contraseñaNueva"));
                userBean.setDoc_id(Integer.parseInt(request.getParameter("documentoNuevo")));
                userDAO.insertUser(userBean);
            }

            if (valor == 1) {
                
                int doc_id = userDAO.SelectDoctorIdUser(request.getParameter("usuario"), request.getParameter("contraseña"));
                
                DoctorBean doctorBean = new DoctorDAO().selectDoctorById(doc_id+"");
                String doctor_id = doc_id+"";
                session.setAttribute("doc_id", doctor_id);
                session.setAttribute("fname", doctorBean.getDoc_fname());
                session.setAttribute("lname", doctorBean.getDoc_lname());
                SpecialityDAO a = new SpecialityDAO();
                String especialidad = a.selectSpecialityNameOne(doctorBean.getDoc_id_speciality1());
                session.setAttribute("speciality1", especialidad);
                session.setAttribute("cmp", doctorBean.getDoc_cmp());
                session.setAttribute("email", doctorBean.getDoc_email());
                session.setAttribute("facebook", doctorBean.getFacebook());
                session.setAttribute("twitter", doctorBean.getTwitter());
                session.setAttribute("telefono", doctorBean.getDoc_phone());
                
                PerfilDAO perfilDAO = new PerfilDAO();
                List<PerfilBean> listPerfil = new ArrayList<>();
                listPerfil = perfilDAO.selectPerfil(doctor_id);
                if (listPerfil.size() > 0){
                    session.setAttribute("lema", listPerfil.get(0).getLema());
                    session.setAttribute("titular", listPerfil.get(0).getTitular());
                    session.setAttribute("universidad", listPerfil.get(0).getUniversidad());
                    session.setAttribute("reputacion", listPerfil.get(0).getReputacion()+"");
                    session.setAttribute("visitas", listPerfil.get(0).getVisitas()+"");
                    session.setAttribute("busquedas", listPerfil.get(0).getBusquedas()+"");
                    session.setAttribute("url_imagen_fondo", listPerfil.get(0).getUrl_imagen_fondo());
                    session.setAttribute("url_imagen_personal", listPerfil.get(0).getUrl_imagen_personal());
                    System.out.println("La ruta de la imagen es: "+listPerfil.get(0).getUrl_imagen_personal());
                } else {
                    session.setAttribute("lema", "");
                    session.setAttribute("titular", "");
                    session.setAttribute("universidad", "");
                    session.setAttribute("reputacion", "0");
                    session.setAttribute("visitas", "0");
                    session.setAttribute("busquedas", "0");
                    session.setAttribute("url_imagen_fondo", "");
                    session.setAttribute("url_imagen_personal", "");
                }
                
                System.out.println("---------------------------------------HOLA A TODOS------------------------------------");
                
                ExperienciaPerfilDAO experienciaDAO = new ExperienciaPerfilDAO();
                List<ExperienciaPerfilBean> listExperiencia = new ArrayList<>();
                listExperiencia = experienciaDAO.selectPerfil(doctor_id);
                session.setAttribute("listExperiencia",listExperiencia);
                
                EducacionPerfilDAO educacionDAO = new EducacionPerfilDAO();
                List<EducacionPerfilBean> listEducacion = new ArrayList<>();
                listEducacion = educacionDAO.selectPerfil(doctor_id);
                session.setAttribute("listEducacion",listEducacion);
                
                LogroPerfilDAO logroDAO = new LogroPerfilDAO();
                List<LogroPerfilBean> listLogro = new ArrayList<>();
                listLogro = logroDAO.selectLogro(doctor_id);
                session.setAttribute("listLogro",listLogro);
                
                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/perfil3.jsp");

            } else {

                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/errorRegistro.jsp");

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
