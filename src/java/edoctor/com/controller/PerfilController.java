package edoctor.com.controller;

import edoctor.com.beans.PerfilBean;
import edoctor.com.beans.DoctorBean;
import edoctor.com.beans.EducacionPerfilBean;
import edoctor.com.beans.ExperienciaPerfilBean;
import edoctor.com.beans.LogroPerfilBean;
import edoctor.com.dao.DoctorDAO;
import edoctor.com.dao.EducacionPerfilDAO;
import edoctor.com.dao.ExperienciaPerfilDAO;
import edoctor.com.dao.LogroPerfilDAO;
import edoctor.com.dao.PerfilDAO;
import edoctor.com.dao.SpecialityDAO;
import edoctor.com.dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "PerfilController", urlPatterns = {"/PerfilController/updatePerfil.do", "/PerfilController/selectPerfil.do"})
public class PerfilController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            PerfilDAO perfilDAO = new PerfilDAO();
            DoctorDAO doctorDAO = new DoctorDAO();
            PerfilBean p = new PerfilBean();
            DoctorBean a = new DoctorBean();
            RequestDispatcher despachador = null;
            HttpSession session = request.getSession(false);
            int valor = 0;

            if (request.getServletPath().contains("/updatePerfil.do")) {

                valor = 1;
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String titular = request.getParameter("titular");
                String lema = request.getParameter("lema");
                String universidad = request.getParameter("universidad");
                String doc_id = (String) session.getAttribute("doc_id");
                String imagen_fondo = request.getParameter("imagen_fondo");
                String imagen_personal = request.getParameter("imagen_personal");
                System.out.println("La ruta es: "+ "img/perfiles/"+doc_id+"/"+imagen_personal);
                if (nombre.isEmpty() == false || apellido.isEmpty() == false) {

                    a.setDoc_id(doc_id);
                    a.setDoc_fname(nombre);
                    a.setDoc_lname(apellido);
                    doctorDAO.updateDoctor(a);

                }

                p.setDoc_id(doc_id);
                p.setTitular(titular);
                p.setLema(lema);
                p.setUniversidad(universidad);
                p.setUrl_imagen_fondo(imagen_fondo);
                p.setUrl_imagen_personal(imagen_personal);

                valor = perfilDAO.updateExperiencia(p);
            } else if (request.getServletPath().contains("/selectPerfil.do")) {

                valor = 2;
                String cmp = request.getParameter("cmp");
                UserDAO userDAO = new UserDAO();
                int doc_id = userDAO.selectIdUser(request.getParameter("cmp"));
                int contador = 1+perfilDAO.selectVisitas(doc_id+"");
                perfilDAO.updateVisitas(contador, doc_id);
                
            }

            switch (valor) {
                case 1: {
                    String doc_id = (String) session.getAttribute("doc_id");
                    List<PerfilBean> listPerfil = new ArrayList<>();
                    listPerfil = perfilDAO.selectPerfil(doc_id);
                    session.setAttribute("listPerfil", listPerfil);
                    session.setAttribute("lema", listPerfil.get(0).getLema());
                    session.setAttribute("titular", listPerfil.get(0).getTitular());
                    session.setAttribute("universidad", listPerfil.get(0).getUniversidad());
                    DoctorBean d = new DoctorBean();
                    d = doctorDAO.selectDoctorById(doc_id);
                    session.setAttribute("fname", d.getDoc_fname());
                    session.setAttribute("lname", d.getDoc_lname());
                    URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                    response.sendRedirect(url.toString() + "/perfil3.jsp");
                    break;
                }
                case 2: {
                    UserDAO userDAO = new UserDAO();
                    int doc_id = userDAO.selectIdUser(request.getParameter("cmp"));
                    DoctorBean doctorBean = new DoctorDAO().selectDoctorById(doc_id + "");
                    String doctor_id = doc_id + "";
                    session.setAttribute("doc_id", doctor_id);
                    session.setAttribute("fname", doctorBean.getDoc_fname());
                    session.setAttribute("lname", doctorBean.getDoc_lname());
                    SpecialityDAO c = new SpecialityDAO();
                    String especialidad = c.selectSpecialityNameOne(doctorBean.getDoc_id_speciality1());
                    session.setAttribute("speciality1", especialidad);
                    session.setAttribute("cmp", doctorBean.getDoc_cmp());
                    session.setAttribute("email", doctorBean.getDoc_email());
                    PerfilDAO d = new PerfilDAO();
                    List<PerfilBean> listPerfil = new ArrayList<>();
                    listPerfil = d.selectPerfil(doctor_id);
                    if (listPerfil.size() > 0) {
                        session.setAttribute("lema", listPerfil.get(0).getLema());
                        session.setAttribute("titular", listPerfil.get(0).getTitular());
                        session.setAttribute("universidad", listPerfil.get(0).getUniversidad());
                        session.setAttribute("reputacion", listPerfil.get(0).getReputacion() + "");
                        session.setAttribute("visitas", listPerfil.get(0).getVisitas() + "");
                        session.setAttribute("busquedas", listPerfil.get(0).getBusquedas() + "");
                        session.setAttribute("url_imagen_fondo", listPerfil.get(0).getUrl_imagen_fondo());
                        session.setAttribute("url_imagen_personal", listPerfil.get(0).getUrl_imagen_personal());
                    } else {
                        session.setAttribute("lema", "");
                        session.setAttribute("titular", "");
                        session.setAttribute("universidad", "");
                        session.setAttribute("reputacion", "0");
                        session.setAttribute("visitas", "0");
                        session.setAttribute("busquedas", "0");
                        session.setAttribute("url_imagen_fondo", "");
                        session.setAttribute("url_imagen_personal", "");
                    }
                    ExperienciaPerfilDAO experienciaDAO = new ExperienciaPerfilDAO();
                    List<ExperienciaPerfilBean> listExperiencia = new ArrayList<>();
                    listExperiencia = experienciaDAO.selectPerfil(doctor_id);
                    session.setAttribute("listExperiencia", listExperiencia);
                    EducacionPerfilDAO educacionDAO = new EducacionPerfilDAO();
                    List<EducacionPerfilBean> listEducacion = new ArrayList<>();
                    listEducacion = educacionDAO.selectPerfil(doctor_id);
                    session.setAttribute("listEducacion", listEducacion);
                    LogroPerfilDAO logroDAO = new LogroPerfilDAO();
                    List<LogroPerfilBean> listLogro = new ArrayList<>();
                    listLogro = logroDAO.selectLogro(doctor_id);
                    session.setAttribute("listLogro",listLogro);
                    System.out.println("cantidad de logros:");
                    URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                    response.sendRedirect(url.toString() + "/perfil5.jsp");
                    break;
                }
                default: {
                    URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                    response.sendRedirect(url.toString() + "/errorRegistro.jsp");
                    break;
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PerfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PerfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
