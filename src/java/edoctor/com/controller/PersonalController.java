package edoctor.com.controller;

import edoctor.com.beans.PerfilBean;
import edoctor.com.beans.DoctorBean;
import edoctor.com.beans.EducacionPerfilBean;
import edoctor.com.beans.ExperienciaPerfilBean;
import edoctor.com.beans.LogroPerfilBean;
import edoctor.com.dao.DoctorDAO;
import edoctor.com.dao.EducacionPerfilDAO;
import edoctor.com.dao.ExperienciaPerfilDAO;
import edoctor.com.dao.LogroPerfilDAO;
import edoctor.com.dao.PerfilDAO;
import edoctor.com.dao.SpecialityDAO;
import edoctor.com.dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "PersonalController", urlPatterns = {"/PersonalController/updatePersonal.do"})
public class PersonalController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            PerfilDAO perfilDAO = new PerfilDAO();
            DoctorDAO doctorDAO = new DoctorDAO();
            PerfilBean p = new PerfilBean();
            DoctorBean a = new DoctorBean();
            RequestDispatcher despachador = null;
            HttpSession session = request.getSession(false);
            int valor = 0;

            if (request.getServletPath().contains("/updatePersonal.do")) {

                valor = 1;
                String facebook = request.getParameter("facebook");
                String twitter = request.getParameter("twitter");
                String telefono = request.getParameter("telefono");
                String doc_id = (String) session.getAttribute("doc_id");
                System.out.println(facebook);
                System.out.println(twitter);
                System.out.println(telefono);
                a.setDoc_id(doc_id);
                a.setFacebook(facebook);
                a.setTwitter(twitter);
                a.setDoc_phone(telefono);

                doctorDAO.updateDoctor2(a);
            } 

            switch (valor) {
                case 1: {
                    String doc_id = (String) session.getAttribute("doc_id");
                    List<PerfilBean> listPerfil = new ArrayList<>();
                    listPerfil = perfilDAO.selectPerfil(doc_id);
                    session.setAttribute("listPerfil", listPerfil);
                    session.setAttribute("lema", listPerfil.get(0).getLema());
                    session.setAttribute("titular", listPerfil.get(0).getTitular());
                    session.setAttribute("universidad", listPerfil.get(0).getUniversidad());
                    DoctorBean d = new DoctorBean();
                    d = doctorDAO.selectDoctorById(doc_id);
                    session.setAttribute("fname", d.getDoc_fname());
                    session.setAttribute("lname", d.getDoc_lname());
                    session.setAttribute("facebook", d.getFacebook());
                    session.setAttribute("twitter", d.getTwitter());
                    session.setAttribute("telefono", d.getDoc_phone());
                    URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                    response.sendRedirect(url.toString() + "/perfil3.jsp");
                    break;
                }
                
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PersonalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PersonalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
