package edoctor.com.controller;

import edoctor.com.beans.DoctorBean;
import edoctor.com.beans.EmpleoBean;
import edoctor.com.beans.EnterpriseBean;
import edoctor.com.beans.SpecialityBean;
import edoctor.com.dao.DoctorDAO;
import edoctor.com.dao.EmpleosDAO;
import edoctor.com.dao.EnterpriseDAO;
import edoctor.com.dao.SpecialityDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
@WebServlet(name = "BusquedaEmpleosController", urlPatterns = {"/BusquedaEmpleosController"})
public class BusquedaEmpleosController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            EmpleosDAO empleoDAO = new EmpleosDAO();
            
            //SpecialityDAO specialityDAO = new SpecialityDAO();
            //EnterpriseDAO enterpriseDAO = new EnterpriseDAO();
            request.setCharacterEncoding("UTF-8");
            List<EmpleoBean> listEmpleo = new ArrayList<>();

            String Cargo = (String) ((request.getParameter("cargo") != null) ? request.getParameter("cargo").trim() : "");
            String ubicacion = (String) ((request.getParameter("ubicacion") != null) ? request.getParameter("ubicacion").trim() : "");
            String empresa = (String) ((request.getParameter("empresa") != null) ? request.getParameter("empresa").trim() : "");
            String funcion_laboral = (String) ((request.getParameter("funcion_laboral") != null) ? request.getParameter("funcion_laboral").trim() : "");
            String nivel_antiguedad = (String) ((request.getParameter("nivel_antiguedad") != null) ? request.getParameter("nivel_antiguedad").trim() : "");
            String tipo_empleo = (String) ((request.getParameter("tipo_empleo") != null) ? request.getParameter("tipo_empleo").trim() : "");
            
            if (!(Cargo.equals("") && ubicacion.equals("") && empresa.equals("") && funcion_laboral.equals("") && nivel_antiguedad.equals("") && tipo_empleo.equals(""))) {
                if (!tipo_empleo.equals("")) {
                    listEmpleo = empleoDAO.SearchEmpleo(Cargo, ubicacion, empresa, nivel_antiguedad, tipo_empleo);
                } else {
                    listEmpleo = empleoDAO.SearchEmpleo(Cargo, ubicacion, empresa, nivel_antiguedad, funcion_laboral);

                }

            }
            System.out.println("Tama�o: "+listEmpleo.size());
            request.setAttribute("listEmpleo", listEmpleo);
            request.getRequestDispatcher("empleo.jsp").forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error en el servlet" + e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
