/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.controller;

import edoctor.com.beans.DoctorBean;
import edoctor.com.beans.EnterpriseBean;
import edoctor.com.beans.JobBean;
import edoctor.com.beans.SpecialityBean;
import edoctor.com.beans.UserBean;
import edoctor.com.dao.DoctorDAO;
import edoctor.com.dao.EnterpriseDAO;
import edoctor.com.dao.JobDAO;
import edoctor.com.dao.SpecialityDAO;
import edoctor.com.dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.Folder;

/**
 *
 * @author winbugs
 */
@WebServlet(name = "DoctorController", urlPatterns = {"/DoctorController/*"})
public class DoctorController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            System.out.println(request.getServletPath());
            DoctorBean doctorBean = new DoctorBean();
            SpecialityBean specialityBean = new SpecialityBean();
            DoctorDAO doctorDAO = new DoctorDAO();
            JobDAO jobDAO = new JobDAO();
            HttpSession misession = (HttpSession) request.getSession();
            EnterpriseDAO enterpriseDAO = new EnterpriseDAO();
            RequestDispatcher despachador = null;
            String accion = request.getPathInfo();

            if (accion.equals("/DoctorInsert.do")) {

                SpecialityDAO specialityDAO = new SpecialityDAO();
                UserDAO userDAO = new UserDAO();
                UserBean userBean = new UserBean();

                if (userDAO.selectUserByAcount(request.getParameter("correo")) == 1) {

                    URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                    response.sendRedirect(url.toString() + "/errorRegistro.jsp");
                } else {

                    doctorBean.setDoc_fname(request.getParameter("nombre").toUpperCase());
                    doctorBean.setDoc_lname(request.getParameter("apellido").toUpperCase());
                    doctorBean.setDoc_cmp(request.getParameter("cmp"));
                    doctorBean.setDoc_phone(request.getParameter("telefono"));
                    doctorBean.setDoc_email(request.getParameter("correo"));
                    String especialidad1 = request.getParameter("especialidad1");
                    String especialidad2 = request.getParameter("especialidad2");
                    doctorBean.setDoc_id_speciality1(Integer.parseInt(especialidad1));
                    if (!especialidad2.equals("")) {
                        doctorBean.setDoc_id_speciality2(Integer.parseInt(especialidad2));
                    }

                    int idDoctor = doctorDAO.insertDoctor(doctorBean);

                    // Registro de la tabla user
                    userBean.setUs_user(request.getParameter("correo"));
                    userBean.setUs_pass(request.getParameter("password"));
                    userBean.setDoc_id(idDoctor);
                    userDAO.insertUser(userBean);

                    // Registro de la tabla enterprise
                    int flag1 = 0;
                    String trabajo1 = request.getParameter("trabajo1").toUpperCase();
                    String trabajo2 = request.getParameter("trabajo2").toUpperCase();
                    String en_name = enterpriseDAO.selectEnterpriseMatch(trabajo1, "1");
                    String idDepartamento = request.getParameter("region");
                    String idDistrito = request.getParameter("distrito");
                    String idProvincia = request.getParameter("provincia");
                    int en_id = Integer.parseInt(trabajo1);
                    System.out.println("El en_name: " + en_name);
                    if (en_name.equals("")) {
                        System.out.println("El en_name: " + en_name);
                        EnterpriseBean enterpriseBean = new EnterpriseBean();
                        enterpriseBean.setEn_name(trabajo2.toUpperCase());
                        enterpriseBean.setEn_region_id(Integer.parseInt(idDepartamento));
                        enterpriseBean.setEn_province_id(Integer.parseInt(idProvincia));
                        enterpriseBean.setEn_distric_id(Integer.parseInt(idDistrito));
                        en_id = enterpriseDAO.insertEnterprise(enterpriseBean);
                    }

                    JobBean jobBean = new JobBean();
                    jobBean.setJob_doc_id(idDoctor);
                    jobBean.setJob_en_id(en_id);

                    int idJob = jobDAO.insertSingleJob(jobBean);
                    doctorDAO.updateIdJobDoctor(idDoctor, idJob);
                    
                    //Folder folder = new Folder();
                    //folder.createFolder(request.getParameter("correo").toString());

                    URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                    response.sendRedirect(url.toString() + "/correctoRegistro.jsp");
                }

            } else if (accion.equals("/DoctorSelect.do")) {

                List<DoctorBean> listDoctor = new ArrayList<>();
                if (request.getParameter("todos").contains("1")) {
                    listDoctor = doctorDAO.selectDoctor();
                } else if (request.getParameter("todos").contains("2")) {
                    listDoctor = doctorDAO.selectDoctor(request.getParameter("nombre"), request.getParameter("apellido"));
                } else if (request.getParameter("todos").contains("3")) {
                    listDoctor = doctorDAO.selectDoctor(request.getParameter("tipo"), request.getParameter("busca"));
                }

                request.setAttribute("listaDeDoctores", listDoctor);
                despachador = request.getRequestDispatcher(""); // falta poner el nombre del html a desplegar
                despachador.forward(request, response);

            } else if (request.getServletPath().equals("/DoctorDelete.do")) {

                UserDAO userDAO = new UserDAO();
                int doc_id = userDAO.selectUserDelete(request.getParameter("usuario"), request.getParameter("contraseņa"));
                doctorDAO.deleteDoctor(doc_id);
                despachador = request.getRequestDispatcher(""); // falta poner el nombre del html a desplegar
                despachador.forward(request, response);

            } else if (request.getServletPath().equals("/DoctorUpdate.do")) {

                SpecialityDAO specialityDAO = new SpecialityDAO();
                doctorBean.setDoc_fname(request.getParameter("nombres"));
                doctorBean.setDoc_lname(request.getParameter("apellidos"));
                doctorBean.setDoc_cmp(request.getParameter("cmp"));
                doctorBean.setDoc_phone(request.getParameter("telefono"));
                doctorBean.setDoc_email(request.getParameter("correo"));
                String especialidad1 = request.getParameter("especialidad1");
                String especialidad2 = request.getParameter("especialidad2");

                doctorBean.setDoc_id_speciality1(specialityDAO.selectSpecialityOne(especialidad1));
                doctorBean.setDoc_id_speciality2(specialityDAO.selectSpecialityOne(especialidad2));
                doctorDAO.updateDoctor(doctorBean);
                despachador = request.getRequestDispatcher(""); // falta poner el nombre del html a desplegar
                despachador.forward(request, response);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DoctorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DoctorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
