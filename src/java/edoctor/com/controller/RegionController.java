/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.controller;

import edoctor.com.beans.DistritoBean;
import edoctor.com.beans.ProvinciaBean;
import edoctor.com.beans.RegionBean;
import edoctor.com.beans.SpecialityBean;
import edoctor.com.dao.SpecialityDAO;
import edoctor.com.dao.UbigeoDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
@WebServlet(name = "RegionControlle", urlPatterns = {"/RegionControlle"})

public class RegionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        	//	System.out.println("get");

        
        try (PrintWriter out = response.getWriter()) {

            SpecialityDAO specialityDAO = new SpecialityDAO();
            List<SpecialityBean> listSpeciality = new ArrayList<>();
            listSpeciality = specialityDAO.selectSpeciality();
            UbigeoDAO a = new UbigeoDAO();
            List<ProvinciaBean> y = new ArrayList<>();
            List<DistritoBean> z = new ArrayList<>();
            if (request.getParameter("region")!=null) {
                y = a.selectProvinciaFromDepartment(request.getParameter("region"));
               out.println("<option value=0>SELECCIONE...</option>");
                for (ProvinciaBean provincia : y) {
                out.println("<option value="+provincia.getPr_id()+" >"+provincia.getPr_name()+"</option>");

            }
            }else if (request.getParameter("provincia")!=null) {
                z = a.selectDistritoFromProvincia(request.getParameter("provincia"));
                out.println("<option value=0>SELECCIONE...</option>");  
                for (DistritoBean distrito : z) {
                out.println("<option value="+distrito.getDi_id()+" >"+distrito.getDi_name()+"</option>");

            }
            
            
         
            }
            
		
            
        }catch (Exception e) {
            System.out.println("Error en el servlet"+e);
	}
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

