package edoctor.com.controller;

import edoctor.com.beans.EducacionPerfilBean;
import edoctor.com.dao.EducacionPerfilDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "EducacionPerfilController", urlPatterns = {"/EducacionPerfilController/insertEducacion.do", "/EducacionPerfilController/updateEducacion.do"})
public class EducacionPerfilController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            EducacionPerfilDAO educacionDAO = new EducacionPerfilDAO();
            EducacionPerfilBean p = new EducacionPerfilBean();
            RequestDispatcher despachador = null;
            HttpSession session = request.getSession(false);
            int valor = 0;

            if (request.getServletPath().contains("/insertEducacion.do")) {

                String universidad = request.getParameter("universidad");
                String titulacion = request.getParameter("titulacion");
                String disciplina = request.getParameter("disciplina");
                String nota_media = request.getParameter("nota_media");
                String actividades = request.getParameter("actividades");
                String descripcion = request.getParameter("descripcion");
                String anio_fin = request.getParameter("anio_fin");
                String anio_inicio = request.getParameter("anio_inicio");
                String doc_id = (String) session.getAttribute("doc_id");

                p.setDoc_id(doc_id);
                p.setUniversidad(universidad);
                p.setTitulacion(titulacion);
                p.setDisciplina(disciplina);
                p.setNota_media(nota_media);
                p.setActividades(actividades);
                p.setDescripcion(descripcion);
                p.setAnio_fin(anio_fin);
                p.setAnio_inicio(anio_inicio);

                valor = educacionDAO.insertEducacion(p);

            } else if (request.getServletPath().contains("/updateEducacion.do")) {

                valor = 1;

                String titulacion = request.getParameter("titulacion");
                String disciplina = request.getParameter("disciplina");
                String nota_media = request.getParameter("nota_media");
                String actividades = request.getParameter("actividades");
                String descripcion = request.getParameter("descripcion");
                String anio_fin = request.getParameter("anio_fin");
                String anio_inicio = request.getParameter("anio_inicio");
                String doc_id = (String) session.getAttribute("doc_id");
                String edu_id = request.getParameter("edu_id");
                String boton = request.getParameter("metodo2");

                if ("1".equals(boton)) {
                    p.setDoc_id(doc_id);
                    p.setTitulacion(titulacion);
                    p.setDisciplina(disciplina);
                    p.setNota_media(nota_media);
                    p.setActividades(actividades);
                    p.setDescripcion(descripcion);
                    p.setAnio_fin(anio_fin);
                    p.setAnio_inicio(anio_inicio);
                    p.setEdu_id(edu_id);

                    educacionDAO.updateEducacion(p);
                } else {
                    
                    educacionDAO.deleteEducacion(edu_id);

                }

            } else if (request.getServletPath().contains("/deleteEducacion.do")) {

                valor = 1;
                /*UserBean userBean = new UserBean();
                userBean.setUs_user(request.getParameter("usuarioNuevo"));
                userBean.setUs_pass(request.getParameter("contraseñaNueva"));
                userBean.setUs_doc_id(Integer.parseInt(request.getParameter("documentoNuevo")));
                userDAO.insertUser(userBean);*/
            }

            if (valor == 1) {

                String doc_id = (String) session.getAttribute("doc_id");
                List<EducacionPerfilBean> listEducacion = new ArrayList<>();
                listEducacion = educacionDAO.selectPerfil(doc_id);
                session.setAttribute("listEducacion", listEducacion);

                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/perfil3.jsp");

            } else {

                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/errorRegistro.jsp");

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(EducacionPerfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(EducacionPerfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
