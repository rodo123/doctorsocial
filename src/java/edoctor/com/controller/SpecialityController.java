
package edoctor.com.controller;

import edoctor.com.beans.DistritoBean;
import edoctor.com.beans.EnterpriseBean;
import edoctor.com.beans.ProvinciaBean;
import edoctor.com.beans.RegionBean;
import edoctor.com.beans.SpecialityBean;
import edoctor.com.dao.EnterpriseDAO;
import edoctor.com.dao.SpecialityDAO;
import edoctor.com.dao.UbigeoDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author winbugs
 */
@WebServlet(name = "SpecialityControlle", urlPatterns = {"/SpecialityControlle"})
public class SpecialityController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        		System.out.println("get");

        
        try (PrintWriter out = response.getWriter()) {

            SpecialityDAO specialityDAO = new SpecialityDAO();
            List<SpecialityBean> listSpeciality = new ArrayList<>();
            listSpeciality = specialityDAO.selectSpeciality();
            UbigeoDAO a = new UbigeoDAO();
            List<RegionBean> x = new ArrayList<>();
            List<ProvinciaBean> y = new ArrayList<>();
            List<DistritoBean> z = new ArrayList<>();
            EnterpriseDAO b = new EnterpriseDAO();
            List<EnterpriseBean> listEnterprise = new ArrayList<>();
            listEnterprise = b.selectEnterprise();
            x = a.selectRegion();
            if (request.getParameter("region")!=null) {
                y = a.selectProvinciaFromDepartment(request.getParameter("region"));
            }
            
            if (request.getParameter("provincia")!=null) {
                z = a.selectDistritoFromProvincia(request.getParameter("provincia"));
            }
            
            
            request.setAttribute("listRegion",x);
            request.setAttribute("listProvincia",y);
            request.setAttribute("listDistrito",z);
            request.setAttribute("listEspecialidades",listSpeciality);
            request.setAttribute("listEnterprise",listEnterprise);
            request.getRequestDispatcher("/registrar.jsp").forward(request, response);
            
        }catch (Exception e) {
            System.out.println("Error en el servlet"+e);
	}
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
