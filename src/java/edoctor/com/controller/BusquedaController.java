package edoctor.com.controller;

import edoctor.com.beans.DoctorBean;
import edoctor.com.beans.EnterpriseBean;
import edoctor.com.beans.SpecialityBean;
import edoctor.com.dao.DoctorDAO;
import edoctor.com.dao.EnterpriseDAO;
import edoctor.com.dao.SpecialityDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
@WebServlet(name = "BusquedaDoctorController", urlPatterns = {"/BusquedaDoctorControlle"})
public class BusquedaController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            DoctorDAO doctorDAO = new DoctorDAO();
            SpecialityDAO specialityDAO = new SpecialityDAO();
            EnterpriseDAO enterpriseDAO = new EnterpriseDAO();
            request.setCharacterEncoding("UTF-8");
            List<DoctorBean> listDoctor = new ArrayList<>();

            String nombre = (String) ((request.getParameter("nombre") != null) ? request.getParameter("nombre").trim() : "");
            String apellido = (String) ((request.getParameter("apellido") != null) ? request.getParameter("apellido").trim() : "");
            String cmp = (String) ((request.getParameter("cmp") != null) ? request.getParameter("cmp").trim() : "");
            String trabajo = (String) ((request.getParameter("trabajo") != null) ? request.getParameter("trabajo").trim() : "");
            String especialidad = (String) ((request.getParameter("especialidad") != null) ? request.getParameter("especialidad").trim() : "");
            String trabajoLista = (String) ((request.getParameter("trabajoLista") != null) ? request.getParameter("trabajoLista").trim() : "");
            if (!(nombre.equals("") && apellido.equals("") && cmp.equals("") && trabajo.equals("") && especialidad.equals("") && trabajoLista.equals(""))) {
                if (!trabajoLista.equals("")) {
                    listDoctor = doctorDAO.SearchDoctor(nombre, apellido, cmp, especialidad, trabajoLista);
                } else {
                    listDoctor = doctorDAO.SearchDoctor(nombre, apellido, cmp, especialidad, trabajo);

                }

            }

            List<SpecialityBean> listSpeciality = new ArrayList<>();
            listSpeciality = specialityDAO.selectSpeciality();

            List<EnterpriseBean> listEnterprise = new ArrayList<>();
            listEnterprise = enterpriseDAO.selectEnterprise();
            request.setAttribute("listEspecialidades", listSpeciality);
            request.setAttribute("listDoctor", listDoctor);
            request.setAttribute("listEnterprise", listEnterprise);
            request.getRequestDispatcher("buscar.jsp").forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error en el servlet" + e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
