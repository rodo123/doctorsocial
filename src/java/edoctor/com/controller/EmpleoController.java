package edoctor.com.controller;

import edoctor.com.beans.EmpleoBean;
import edoctor.com.beans.ExperienciaPerfilBean;
import edoctor.com.beans.PostulacionesBean;
import edoctor.com.dao.EmpleosDAO;
import edoctor.com.dao.ExperienciaPerfilDAO;
import edoctor.com.dao.PostulacionesDAO;
import edoctor.com.dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "EmpleoController", urlPatterns = {"/EmpleoController/insertEmpleo.do", "/EmpleoController/updateEmpleo.do","/EmpleoController/postularEmpleo.do"})
public class EmpleoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            EmpleosDAO empleoDAO = new EmpleosDAO();
            EmpleoBean p = new EmpleoBean();
            
            RequestDispatcher despachador = null;
            HttpSession session = request.getSession(false);
            
            int valor = 0;

            if (request.getServletPath().contains("/insertEmpleo.do")) {

                String cargo = request.getParameter("cargo");
                String empresa = request.getParameter("empresa");
                String ubicacion = request.getParameter("ubicacion");
                String funcion_laboral = request.getParameter("funcion_laboral");
                String tipo_empleo = request.getParameter("tipo_empleo");
                String sector_empresa = request.getParameter("sector_empresa");
                String nivel_antiguedad = request.getParameter("nivel_antiguedad");
                String descripcion = request.getParameter("descripcion");
                String doc_id = (String) session.getAttribute("doc_id");
                String descripcion_empresa = request.getParameter("descripcion_empresa");
                String email = request.getParameter("email");
                String url_page = request.getParameter("url_page");
                
                System.out.println("El cargo es: "+cargo);

                p.setDoc_id(doc_id);
                p.setEmpresa(empresa);
                p.setCargo(cargo);
                p.setUbicacion(ubicacion);
                p.setFuncion_laboral(funcion_laboral);
                p.setTipo_empleo(tipo_empleo);
                p.setNivel_antiguedad(nivel_antiguedad);
                p.setDescripcion(descripcion);
                p.setDescripcion_empresa(descripcion_empresa);
                p.setEmail(email);
                p.setUrl_page(url_page);

                valor = empleoDAO.insertEmpleo(p);

            } else if (request.getServletPath().contains("/updateExperiencia.do")) {

                System.out.println("");

            } else if (request.getServletPath().contains("/deleteExperiencia.do")) {

                /*valor = 1;
                String exp_id = request.getParameter("exp_id_delete");
                System.out.println("borrar el id: " + exp_id);
                */
                /*UserBean userBean = new UserBean();
                userBean.setUs_user(request.getParameter("usuarioNuevo"));
                userBean.setUs_pass(request.getParameter("contraseñaNueva"));
                userBean.setUs_doc_id(Integer.parseInt(request.getParameter("documentoNuevo")));
                userDAO.insertUser(userBean);*/
            } else if (request.getServletPath().contains("/postularEmpleo.do")) {

                valor = 2;
                
                String correoTo = request.getParameter("email_postulacion");
                String trabajo_id = request.getParameter("trabajo_id");
                String doc_id = (String) session.getAttribute("doc_id");
                String correoOf = (String) session.getAttribute("correo");
                String cargo = request.getParameter("cargo_postulacion");
                
                if (doc_id == null) {
                    valor = 3;
                } else {
                    PostulacionesDAO postulacionesDAO = new PostulacionesDAO();
                    PostulacionesBean postulacionesBean = new PostulacionesBean();
                    postulacionesBean.setTrabajo_id(trabajo_id);
                    postulacionesBean.setDoc_id(doc_id);
                    postulacionesDAO.insertPostulacion(postulacionesBean,correoTo,correoOf,cargo);
                }
            }

            switch (valor) {
                case 1:
                    {
                        URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                        response.sendRedirect(url.toString() + "/empleos.jsp");
                        break;
                    }
                case 2: {
                        URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                        response.sendRedirect(url.toString() + "/correctoPostulacion.jsp");
                        break;
                    }
                case 3: {
                        URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                        response.sendRedirect(url.toString() + "/login.jsp");
                        break;
                    }
                default:
                    {
                        URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                        response.sendRedirect(url.toString() + "/errorRegistro.jsp");
                        break;
                    }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(EmpleoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(EmpleoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
