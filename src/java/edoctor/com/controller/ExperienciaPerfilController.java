package edoctor.com.controller;

import edoctor.com.beans.ExperienciaPerfilBean;
import edoctor.com.dao.ExperienciaPerfilDAO;
import edoctor.com.dao.PerfilDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ExperienciaPerfilController", urlPatterns = {"/ExperienciaPerfilController/insertExperiencia.do", "/ExperienciaPerfilController/updateExperiencia.do", "/ExperienciaPerfilController/deleteExperiencia.do"})
public class ExperienciaPerfilController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            ExperienciaPerfilDAO experienciaDAO = new ExperienciaPerfilDAO();
            ExperienciaPerfilBean p = new ExperienciaPerfilBean();
            RequestDispatcher despachador = null;
            HttpSession session = request.getSession(false);
            System.out.println(request.getServletPath());
            int valor = 0;

            if (request.getServletPath().contains("/insertExperiencia.do")) {

                String cargo = request.getParameter("cargo");
                String empresa = request.getParameter("empresa");
                String lugar = request.getParameter("ubicacion");
                String mes_inicio = request.getParameter("mes_inicio");
                String a�o_inicio = request.getParameter("anio_inicio");
                String mes_fin = request.getParameter("mes_fin");
                String a�o_fin = request.getParameter("anio_fin");
                String descripcion = request.getParameter("descripcion");
                String doc_id = (String) session.getAttribute("doc_id");

                p.setDoc_id(doc_id);
                p.setEmpresa(empresa);
                p.setCargo(cargo);
                p.setLugar(lugar);
                p.setMes_inicio(mes_inicio);
                p.setAnio_inicio(a�o_inicio);
                p.setMes_fin(mes_fin);
                p.setAnio_fin(a�o_fin);
                p.setActual("0");
                p.setDescripcion(descripcion);

                valor = experienciaDAO.insertExperiencia(p);

            } else if (request.getServletPath().contains("/updateExperiencia.do")) {

                valor = 1;
                //String cargo = request.getParameter("cargo");
                String empresa = request.getParameter("empresa");
                String lugar = request.getParameter("ubicacion");
                String mes_inicio = request.getParameter("mes_inicio");
                String a�o_inicio = request.getParameter("anio_inicio");
                String mes_fin = request.getParameter("mes_fin");
                String a�o_fin = request.getParameter("anio_fin");
                String descripcion = request.getParameter("descripcion");
                String doc_id = (String) session.getAttribute("doc_id");
                String exp_id = request.getParameter("exp_id");
                String boton = request.getParameter("metodo");

                System.out.println("el boton es: " + boton);

                if ("1".equals(boton)) {
                    p.setExp_id(exp_id);
                    p.setDoc_id(doc_id);
                    p.setEmpresa(empresa);
                    //p.setCargo(cargo);
                    p.setLugar(lugar);
                    p.setMes_inicio(mes_inicio);
                    p.setAnio_inicio(a�o_inicio);
                    p.setMes_fin(mes_fin);
                    p.setAnio_fin(a�o_fin);
                    p.setActual("0");
                    p.setDescripcion(descripcion);

                    experienciaDAO.updateExperiencia(p);
                } else {
                    
                    experienciaDAO.deleteExperiencia(exp_id);
                }

            } else if (request.getServletPath().contains("/deleteExperiencia.do")) {

                valor = 1;
                String exp_id = request.getParameter("exp_id_delete");
                System.out.println("borrar el id: " + exp_id);

                /*UserBean userBean = new UserBean();
                userBean.setUs_user(request.getParameter("usuarioNuevo"));
                userBean.setUs_pass(request.getParameter("contrase�aNueva"));
                userBean.setUs_doc_id(Integer.parseInt(request.getParameter("documentoNuevo")));
                userDAO.insertUser(userBean);*/
            }

            if (valor == 1) {

                String doc_id = (String) session.getAttribute("doc_id");
                List<ExperienciaPerfilBean> listExperiencia = new ArrayList<>();
                listExperiencia = experienciaDAO.selectPerfil(doc_id);
                session.setAttribute("listExperiencia", listExperiencia);

                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/perfil3.jsp");

            } else {

                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/errorRegistro.jsp");

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ExperienciaPerfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ExperienciaPerfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
