package edoctor.com.controller;

import edoctor.com.beans.ContactoBean;
import edoctor.com.dao.ContactoDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "ContactoController", urlPatterns = {"/ContactoController/insertContacto.do"})
public class ContactoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            ContactoDAO contactoDAO = new ContactoDAO();
            ContactoBean p = new ContactoBean();
            RequestDispatcher despachador = null;
            HttpSession session = request.getSession(false);
            int valor = 0;

            if (request.getServletPath().contains("/insertContacto.do")) {
                
                valor = 1;
                String nombre = request.getParameter("nombre");
                String email = request.getParameter("email");
                String mensaje = request.getParameter("mensaje");
                
                p.setNombre(nombre);
                p.setEmail(email);
                p.setMensaje(mensaje);
                
                valor = contactoDAO.insertContacto(p);
            }

            if (valor == 1) {
                      
                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/index.jsp");

            } else {

                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/index.jsp");

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ContactoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ContactoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
