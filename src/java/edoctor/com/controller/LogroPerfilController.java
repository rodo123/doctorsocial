package edoctor.com.controller;

import edoctor.com.beans.LogroPerfilBean;
import edoctor.com.dao.LogroPerfilDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "LogroPerfilController", urlPatterns = {"/LogroPerfilController/insertLogro.do","/LogroPerfilController/updateLogro.do"})
public class LogroPerfilController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            LogroPerfilDAO logroDAO = new LogroPerfilDAO();
            LogroPerfilBean p = new LogroPerfilBean();
            RequestDispatcher despachador = null;
            HttpSession session = request.getSession(false);
            int valor = 0;

            if (request.getServletPath().contains("/insertLogro.do")) {
                
                //String tipo_logro = request.getParameter("tipo_logro");
                String nombre_logro = request.getParameter("nombre_logro");
                System.out.println("logro: "+nombre_logro);
                //String fecha_logro = request.getParameter("fecha_logro");
                String doc_id = (String) session.getAttribute("doc_id");
                
                p.setDoc_id(doc_id);
                //p.setTipo_logro(tipo_logro);
                p.setNombre_logro(nombre_logro);
                //p.setFecha_logro(fecha_logro);
                
                valor = logroDAO.insertLogro(p);
                
            } else if (request.getServletPath().contains("/updateLogro.do")) {
                
                valor = 1;
                String tipo_logro = request.getParameter("tipo_logro");
                String nombre_logro = request.getParameter("nombre_logro");
                String fecha_logro = request.getParameter("fecha_logro");
                String doc_id = (String) session.getAttribute("doc_id");
                
                
            } else if (request.getServletPath().contains("/deleteEducacion.do")) {
                
                valor = 1;
                /*UserBean userBean = new UserBean();
                userBean.setUs_user(request.getParameter("usuarioNuevo"));
                userBean.setUs_pass(request.getParameter("contraseñaNueva"));
                userBean.setUs_doc_id(Integer.parseInt(request.getParameter("documentoNuevo")));
                userDAO.insertUser(userBean);*/
            }

            
            if (valor == 1) {
                
                String doc_id = (String) session.getAttribute("doc_id");
                List<LogroPerfilBean> listLogro = new ArrayList<>();
                listLogro = logroDAO.selectLogro(doc_id);
                session.setAttribute("listLogro",listLogro);
                
                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/perfil3.jsp");

            } else {

                URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
                response.sendRedirect(url.toString() + "/errorRegistro.jsp");

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LogroPerfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LogroPerfilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
