/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.beans;

/**
 *
 * @author telecomunicaciones
 */
public class PerfilBean {
    
    private String doc_id;
    private String titular;
    private String lema;
    private String universidad;
    private int reputacion;
    private int busquedas;
    private int visitas;
    private String url_imagen_fondo;
    private String url_imagen_personal;

    
    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getLema() {
        return lema;
    }

    public void setLema(String lema) {
        this.lema = lema;
    }

    public int getReputacion() {
        return reputacion;
    }

    public void setReputacion(int reputacion) {
        this.reputacion = reputacion;
    }

    public int getBusquedas() {
        return busquedas;
    }

    public void setBusquedas(int busquedas) {
        this.busquedas = busquedas;
    }

    public int getVisitas() {
        return visitas;
    }

    public void setVisitas(int visitas) {
        this.visitas = visitas;
    }

    public String getUrl_imagen_fondo() {
        return url_imagen_fondo;
    }

    public void setUrl_imagen_fondo(String url_imagen_fondo) {
        this.url_imagen_fondo = url_imagen_fondo;
    }

    public String getUrl_imagen_personal() {
        return url_imagen_personal;
    }

    public void setUrl_imagen_personal(String url_imagen_personal) {
        this.url_imagen_personal = url_imagen_personal;
    }
    
    
    
}
