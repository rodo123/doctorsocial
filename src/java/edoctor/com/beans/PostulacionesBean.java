/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.beans;

/**
 *
 * @author rod12
 */
public class PostulacionesBean {
    
    private String post_id;
    private String trabajo_id;
    private String doc_id;

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getTrabajo_id() {
        return trabajo_id;
    }

    public void setTrabajo_id(String trabajo_id) {
        this.trabajo_id = trabajo_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }
    
}
