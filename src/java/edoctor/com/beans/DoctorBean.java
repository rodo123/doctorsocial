
package edoctor.com.beans;

/**
 *
 * @author Rodolfo Zevallos
 */
public class DoctorBean {
    
    private String doc_id;
    private String doc_fname;
    private String doc_lname;
    private String doc_cmp;
    private String doc_phone;
    private String doc_email;
    private int doc_id_speciality1;
    private int doc_id_speciality2;
    private int doc_id_job;
    private String doc_sp_name1;
    private String doc_sp_name2;
    private String doc_en_name;
    private String facebook;
    private String twitter;

    
    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }
    
    public String getDoc_en_name() {
        return doc_en_name;
    }

    public void setDoc_en_name(String doc_en_name) {
        this.doc_en_name = doc_en_name;
    }
    
    public String getDoc_sp_name1() {
        return doc_sp_name1;
    }

    public void setDoc_sp_name1(String doc_sp_name1) {
        this.doc_sp_name1 = doc_sp_name1;
    }

    public String getDoc_sp_name2() {
        return doc_sp_name2;
    }

    public void setDoc_sp_name2(String doc_sp_name2) {
        this.doc_sp_name2 = doc_sp_name2;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getDoc_fname() {
        return doc_fname;
    }

    public void setDoc_fname(String doc_fname) {
        this.doc_fname = doc_fname;
    }

    public String getDoc_lname() {
        return doc_lname;
    }

    public void setDoc_lname(String doc_lname) {
        this.doc_lname = doc_lname;
    }

    public String getDoc_cmp() {
        return doc_cmp;
    }

    public void setDoc_cmp(String doc_cmp) {
        this.doc_cmp = doc_cmp;
    }

    public String getDoc_phone() {
        return doc_phone;
    }

    public void setDoc_phone(String doc_phone) {
        this.doc_phone = doc_phone;
    }

    public String getDoc_email() {
        return doc_email;
    }

    public void setDoc_email(String doc_email) {
        this.doc_email = doc_email;
    }

    public int getDoc_id_speciality1() {
        return doc_id_speciality1;
    }

    public void setDoc_id_speciality1(int doc_id_speciality1) {
        this.doc_id_speciality1 = doc_id_speciality1;
    }

    public int getDoc_id_speciality2() {
        return doc_id_speciality2;
    }

    public void setDoc_id_speciality2(int doc_id_speciality2) {
        this.doc_id_speciality2 = doc_id_speciality2;
    }

    public int getDoc_id_job() {
        return doc_id_job;
    }

    public void setDoc_id_job(int doc_id_job) {
        this.doc_id_job = doc_id_job;
    }
    
    
    
    
}
