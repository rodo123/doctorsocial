
package edoctor.com.beans;

/**
 *
 * @author Rodolfo Zevallos
 */
public class EnterpriseBean {
    
    private int en_id;
    private String en_name;
    private int en_region_id;
    private int en_province_id;
    private int en_distric_id;
    private String en_type;
    private String en_description;

    public int getEn_id() {
        return en_id;
    }

    public void setEn_id(int en_id) {
        this.en_id = en_id;
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }

    public int getEn_region_id() {
        return en_region_id;
    }

    public void setEn_region_id(int en_region_id) {
        this.en_region_id = en_region_id;
    }

    public int getEn_province_id() {
        return en_province_id;
    }

    public void setEn_province_id(int en_province_id) {
        this.en_province_id = en_province_id;
    }

    public int getEn_distric_id() {
        return en_distric_id;
    }

    public void setEn_distric_id(int en_distric_id) {
        this.en_distric_id = en_distric_id;
    }

    public String getEn_type() {
        return en_type;
    }

    public void setEn_type(String en_type) {
        this.en_type = en_type;
    }

    public String getEn_description() {
        return en_description;
    }

    public void setEn_description(String en_description) {
        this.en_description = en_description;
    }
    
    
}
