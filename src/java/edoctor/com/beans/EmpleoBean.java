package edoctor.com.beans;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rod12
 */
public class EmpleoBean {
    
    private String trabajo_id;
    private String doc_id;
    private String cargo;
    private String empresa;
    private String ubicacion;
    private String funcion_laboral;
    private String tipo_empleo;
    private String sector_empresa;
    private String nivel_antiguedad;
    private String descripcion;
    private String descripcion_empresa;
    private String email;
    private String url_page;

    public String getTrabajo_id() {
        return trabajo_id;
    }

    public void setTrabajo_id(String trabajo_id) {
        this.trabajo_id = trabajo_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getFuncion_laboral() {
        return funcion_laboral;
    }

    public void setFuncion_laboral(String funcion_laboral) {
        this.funcion_laboral = funcion_laboral;
    }

    public String getTipo_empleo() {
        return tipo_empleo;
    }

    public void setTipo_empleo(String tipo_empleo) {
        this.tipo_empleo = tipo_empleo;
    }

    public String getSector_empresa() {
        return sector_empresa;
    }

    public void setSector_empresa(String sector_empresa) {
        this.sector_empresa = sector_empresa;
    }

    public String getNivel_antiguedad() {
        return nivel_antiguedad;
    }

    public void setNivel_antiguedad(String nivel_antiguedad) {
        this.nivel_antiguedad = nivel_antiguedad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion_empresa() {
        return descripcion_empresa;
    }

    public void setDescripcion_empresa(String descripcion_empresa) {
        this.descripcion_empresa = descripcion_empresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl_page() {
        return url_page;
    }

    public void setUrl_page(String url_page) {
        this.url_page = url_page;
    }
    
    
    
}
