
package edoctor.com.beans;

/**
 *
 * @author Rodolfo Zevallos
 */
public class SpecialityBean {
    
    private int sp_id;
    private String sp_name;
    private String sp_description;

    public int getSp_id() {
        return sp_id;
    }

    public void setSp_id(int sp_id) {
        this.sp_id = sp_id;
    }

    public String getSp_name() {
        return sp_name;
    }

    public void setSp_name(String sp_name) {
        this.sp_name = sp_name;
    }

    public String getSp_description() {
        return sp_description;
    }

    public void setSp_description(String sp_description) {
        this.sp_description = sp_description;
    }
    
    
}
