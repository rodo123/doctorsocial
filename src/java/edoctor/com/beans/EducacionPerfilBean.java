
package edoctor.com.beans;

/**
 *
 * @author Rodolfo Zevallos
 */
public class EducacionPerfilBean {
   
    private String edu_id;
    private String doc_id;
    private String universidad;
    private String titulacion;
    private String disciplina;
    private String nota_media;
    private String actividades;
    private String descripcion;
    private String anio_fin;
    private String anio_inicio;

    public String getEdu_id() {
        return edu_id;
    }

    public void setEdu_id(String edu_id) {
        this.edu_id = edu_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public String getTitulacion() {
        return titulacion;
    }

    public void setTitulacion(String titulacion) {
        this.titulacion = titulacion;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getNota_media() {
        return nota_media;
    }

    public void setNota_media(String nota_media) {
        this.nota_media = nota_media;
    }

    public String getActividades() {
        return actividades;
    }

    public void setActividades(String actividades) {
        this.actividades = actividades;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAnio_fin() {
        return anio_fin;
    }

    public void setAnio_fin(String anio_fin) {
        this.anio_fin = anio_fin;
    }

    public String getAnio_inicio() {
        return anio_inicio;
    }

    public void setAnio_inicio(String anio_inicio) {
        this.anio_inicio = anio_inicio;
    }
    
    

   
}
