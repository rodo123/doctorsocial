
package edoctor.com.beans;

/**
 *
 * @author Rodolfo Zevallos
 */
public class UserBean {
    
    private int us_id;
    private String us_user;
    private String us_pass;
    private int doc_id;

    public int getUs_id() {
        return us_id;
    }

    public void setUs_id(int us_id) {
        this.us_id = us_id;
    }

    public String getUs_user() {
        return us_user;
    }

    public void setUs_user(String us_user) {
        this.us_user = us_user;
    }

    public String getUs_pass() {
        return us_pass;
    }

    public void setUs_pass(String us_pass) {
        this.us_pass = us_pass;
    }

    public int getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(int doc_id) {
        this.doc_id = doc_id;
    }

    
    
}
