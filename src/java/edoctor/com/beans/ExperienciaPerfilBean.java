
package edoctor.com.beans;

/**
 *
 * @author Rodolfo Zevallos
 */
public class ExperienciaPerfilBean {
   
    private String exp_id;
    private String doc_id;
    private String cargo;
    private String empresa;
    private String lugar;
    private String mes_inicio;
    private String anio_inicio;
    private String mes_fin;
    private String anio_fin;
    private String descripcion;
    private String actual;

    public String getExp_id() {
        return exp_id;
    }

    public void setExp_id(String exp_id) {
        this.exp_id = exp_id;
    }

    
    
    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }
   
    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }
   
    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getMes_inicio() {
        return mes_inicio;
    }

    public void setMes_inicio(String mes_inicio) {
        this.mes_inicio = mes_inicio;
    }

    public String getMes_fin() {
        return mes_fin;
    }

    public void setMes_fin(String mes_fin) {
        this.mes_fin = mes_fin;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAnio_inicio() {
        return anio_inicio;
    }

    public void setAnio_inicio(String anio_inicio) {
        this.anio_inicio = anio_inicio;
    }

    public String getAnio_fin() {
        return anio_fin;
    }

    public void setAnio_fin(String anio_fin) {
        this.anio_fin = anio_fin;
    }
   
}
