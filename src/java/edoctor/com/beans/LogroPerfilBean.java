/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.beans;

/**
 *
 * @author telecomunicaciones
 */
public class LogroPerfilBean {
    
    private String doc_id;
    private String tipo_logro;
    private String nombre_logro;
    private String fecha_logro;

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getTipo_logro() {
        return tipo_logro;
    }

    public void setTipo_logro(String tipo_logro) {
        this.tipo_logro = tipo_logro;
    }

    public String getNombre_logro() {
        return nombre_logro;
    }

    public void setNombre_logro(String nombre_logro) {
        this.nombre_logro = nombre_logro;
    }

    public String getFecha_logro() {
        return fecha_logro;
    }

    public void setFecha_logro(String fecha_logro) {
        this.fecha_logro = fecha_logro;
    }
    
    
    
}
