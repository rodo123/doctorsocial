/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.beans;

/**
 *
 * @author Rodolfo Zevallos
 */
public class JobBean {
    
    private int job_id;
    private int job_doc_id;
    private int job_en_id;
    private String job_position;

    public int getJob_id() {
        return job_id;
    }

    public void setJob_id(int job_id) {
        this.job_id = job_id;
    }

    public int getJob_doc_id() {
        return job_doc_id;
    }

    public void setJob_doc_id(int job_doc_id) {
        this.job_doc_id = job_doc_id;
    }

    public int getJob_en_id() {
        return job_en_id;
    }

    public void setJob_en_id(int job_en_id) {
        this.job_en_id = job_en_id;
    }

    public String getJob_position() {
        return job_position;
    }

    public void setJob_position(String job_position) {
        this.job_position = job_position;
    }
    
    
    
}
