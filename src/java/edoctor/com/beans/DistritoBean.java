/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.beans;

/**
 *
 * @author winbugs
 */
public class DistritoBean {
    
    private int di_id;
    private String di_name;
    private int di_pr_id;

    public int getDi_id() {
        return di_id;
    }

    public void setDi_id(int di_id) {
        this.di_id = di_id;
    }

    public String getDi_name() {
        return di_name;
    }

    public void setDi_name(String di_name) {
        this.di_name = di_name;
    }

    public int getDi_pr_id() {
        return di_pr_id;
    }

    public void setDi_pr_id(int di_pr_id) {
        this.di_pr_id = di_pr_id;
    }
    
    
}
