/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edoctor.com.beans;

/**
 *
 * @author winbugs
 */
public class ProvinciaBean {
    
    private int pr_id;
    private String pr_name;
    private int pr_re_id;

    public int getPr_re_id() {
        return pr_re_id;
    }

    public void setPr_re_id(int pr_re_id) {
        this.pr_re_id = pr_re_id;
    }
    
    public int getPr_id() {
        return pr_id;
    }

    public void setPr_id(int pr_id) {
        this.pr_id = pr_id;
    }

    public String getPr_name() {
        return pr_name;
    }

    public void setPr_name(String pr_name) {
        this.pr_name = pr_name;
    }
    
}
